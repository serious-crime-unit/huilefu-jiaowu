<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::get('/', 'index/read');

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');

Route::get('admin/index', 'admin.blog/index'); // 测试

// 公众号接口路由
Route::group('api', function () {
	Route::post('login', 'Login/login'); // 登录

	Route::post('phoneLogin', 'Login/phoneLogin'); // 验证码登录

	Route::post('wechatAuthLogin', 'Login/wechatAuthLogin'); // 微信授权登录
	
	Route::post('jsSDK', 'Login/jsSDK'); // 获取vxjssdk
	
	Route::post('phoneAuthLogin', 'Login/bindPhone'); // 绑定微信自动登录

	Route::post('unbindVX', 'Login/unbindVX'); // 用户解绑微信

	Route::post('upload', 'User/upload'); // 上传文件

	Route::post('video', 'User/video'); // 上传视频

	Route::post('send-verify-code', 'User/sendVerifyCode'); // 发送短信验证码
	
	Route::post('SMS_verify_code', 'Check/SMS_verify_code'); // 短信验证码验证

    Route::post('parent/list', 'User/getParentList'); // 搜索推荐人

	Route::post('readExcel', 'Excel/import'); // 导入Excel(用户)

	Route::post('exportExcel', 'Excel/exportExcel'); // 导出Excel

	Route::post('exportExcelMOMO', 'Excel/exportExcelMOMO'); // 导出好妈妈excel

    Route::post('exportExcelAPI', 'Excel/exportExcelAPI'); // 导出用户等级时间筛选

    Route::post('exportExcelList', 'Excel/exportExcelList'); // 查询可导出列表

    Route::post('importCourseOrder', 'Excel/importCourseOrder'); // 导入excel(课程)

    Route::post('importMeeting', 'Excel/importMeeting'); // 导入excel(活动)

    Route::post('forgot_pwd', 'User/forgot_pwd'); // 忘记密码

    Route::post('user/cate', 'User/cate'); // 产品列表

    Route::post('user/cateRank', 'User/cateRank'); // 排行分类

    Route::post('export/list', 'User/exportList'); // 查看导出用户

	Route::post('editUserVIP', 'Login/editUserVIP'); // 定时任务 - 检查会员是否到期一个月
	Route::post('editUserVIP2', 'Login/editUserVIP2'); // 定时任务 - 检测会员是否异常冻结 （如果会员没到期自动解冻）
	Route::post('examine', 'Login/examine'); // 定时任务 - 检查活动是否开始结束
	Route::post('saveNotice', 'Login/saveNotice'); // 定时任务 - 定时发布公告

	Route::post('user/info', 'User/getUserInfo')->middleware('InterfaceLoginAuth'); // 获取用户信息
    Route::post('user/grade/list', 'User/userGradeList')->middleware('InterfaceLoginAuth'); // 获取用户级别
    Route::post('user/list', 'User/list')->middleware('InterfaceLoginAuth'); // 总部查看用户
    Route::post('user/child/list', 'User/getChildList')->middleware('InterfaceLoginAuth'); // 用户下级列表
    Route::post('user/registerNumber', 'User/registerNumber')->middleware('InterfaceLoginAuth'); // 首页数据统计(数字)
    Route::post('user/registerList', 'User/registerList')->middleware('InterfaceLoginAuth'); // 首页数据统计(列表)
    Route::post('user/ranking', 'User/ranking')->middleware('InterfaceLoginAuth'); // 中心注册量排行
    Route::post('user/edit', 'User/edit')->middleware('InterfaceLoginAuth'); // 修改用户信息
    Route::post('user/read', 'User/read'); // 获取会员信息
    Route::post('user/uplevel', 'Login/getUserUp')->middleware('InterfaceLoginAuth'); // 查看是否升级
    Route::post('user/updatePhone', 'User/updatePhone')->middleware('InterfaceLoginAuth'); // 用户修改手机号
    Route::post('user/pwd', 'User/updatePwd')->middleware('InterfaceLoginAuth'); // 用户修改密码
    Route::post('user/parent', 'User/getParentFirst')->middleware('InterfaceLoginAuth'); // 购买名额时显示的推荐人
    Route::post('user/checkList', 'User/checkList')->middleware('InterfaceLoginAuth'); // 会员申请列表
    Route::post('user/concern/save', 'User/save_concern')->middleware('InterfaceLoginAuth'); // 会员关注用户
    Route::post('user/concern/list', 'User/list_concern')->middleware('InterfaceLoginAuth'); // 会员关注列表
    Route::post('user/concern/del', 'User/del_concern')->middleware('InterfaceLoginAuth'); // 会员取消关注
    Route::post('user/rank/list', 'User/user_rank')->middleware('InterfaceLoginAuth'); // 中心查看排名
    Route::post('user/boss/edit', 'User/setUserInfo')->middleware('InterfaceLoginAuth'); // 总部修改用户
    Route::post('user/boss/setUserInfoPay', 'User/setUserInfoPay')->middleware('InterfaceLoginAuth'); // 总部修改库存x
    Route::post('boss/grade/edit', 'User/setUserGrade')->middleware('InterfaceLoginAuth'); // 总部修改等级
    Route::post('boss/grade/del', 'User/delUserGrade')->middleware('InterfaceLoginAuth'); // 总部删除等级
    Route::post('user/grade/info', 'User/getGradeInfo')->middleware('InterfaceLoginAuth'); // 等级图片
    Route::post('user/grade/save', 'User/saveUserGrade')->middleware('InterfaceLoginAuth'); // 添加用户身份
    Route::post('user/grade/choice', 'User/getUserGradeInfo')->middleware('InterfaceLoginAuth'); // 查询当前用户的身份用来筛选
    Route::post('user/chart', 'User/getUserQuotaChart')->middleware('InterfaceLoginAuth'); // 折线图数据 每周每月筛选(名额、用户)
    Route::post('user/getGradeDetails', 'User/getGradeDetails')->middleware('InterfaceLoginAuth'); // 身份到期时间
    Route::post('get/new/user/chart', 'User/getUserFunnelChart')->middleware('InterfaceLoginAuth'); // 漏斗图数据
    Route::post('get/city/user/chart', 'User/getUserCityChart')->middleware('InterfaceLoginAuth'); // 用户地区分布
    Route::post('get/course/user/chart', 'User/getUserCourseChart')->middleware('InterfaceLoginAuth'); // 课程报名数量
    Route::post('user/getUserGradeVIPList', 'User/getUserGradeVIPList')->middleware('InterfaceLoginAuth'); // 查询用户证书
    Route::post('user/getUserThreeList', 'User/getUserThreeList'); // 个人主页数据
    Route::post('user/myComments', 'User/myComments')->middleware('InterfaceLoginAuth'); // 查看我发布的评论
    Route::post('user/whoLikedMyComments', 'User/whoLikedMyComments')->middleware('InterfaceLoginAuth'); // 谁点赞了我的评论
    Route::post('user/saveChil', 'User/saveChil')->middleware('InterfaceLoginAuth'); // 用户绑定孩子
    Route::post('user/chilList', 'User/chilList')->middleware('InterfaceLoginAuth'); // 查询孩子列表
    Route::post('user/chilDel', 'User/chilDel')->middleware('InterfaceLoginAuth'); // 用户解绑孩子
    Route::post('user/setVideo', 'User/setVideo')->middleware('InterfaceLoginAuth'); // 添加视频内容
    Route::post('user/getVideo', 'User/getVideo')->middleware('InterfaceLoginAuth'); // 查询回放列表
    Route::post('user/delVideo', 'User/delVideo')->middleware('InterfaceLoginAuth'); // 删除回放视频
    Route::post('user/saveCityBoss', 'User/saveCityBoss')->middleware('InterfaceLoginAuth'); // 上传大区微信
    Route::post('user/getCityBoss', 'User/getCityBoss')->middleware('InterfaceLoginAuth'); // 查看大区列表
    Route::post('user/delCityBoss', 'User/delCityBoss')->middleware('InterfaceLoginAuth'); // 删除大区总裁

    Route::post('user/city/list', 'City/cityList')->middleware('InterfaceLoginAuth'); // 查看自己的地址列表
    Route::post('user/city/save', 'City/citySave')->middleware('InterfaceLoginAuth'); // 添加自己的地址
    Route::post('user/city/edit', 'City/cityEdit')->middleware('InterfaceLoginAuth'); // 修改自己的地址
    Route::post('user/city/del', 'City/cityDel')->middleware('InterfaceLoginAuth'); // 删除自己的地址

    Route::post('check/save', 'Check/save'); // 提交注册审核
    Route::post('check/create', 'Check/create')->middleware('InterfaceLoginAuth'); // 申请续费会员
    Route::post('check/saveQuota', 'Check/saveQuota')->middleware('InterfaceLoginAuth'); // 申请购买名额
    Route::post('check/course/save', 'Check/courseSave')->middleware('InterfaceLoginAuth'); // 课程产品报名
    Route::post('check/product/save', 'Check/save_product')->middleware('InterfaceLoginAuth'); // 申请开通产品
    Route::post('check/list', 'Check/list')->middleware('InterfaceLoginAuth'); // 查看审核列表
    Route::post('check/edit', 'Check/edit')->middleware('InterfaceLoginAuth'); // 修改审核状态
    Route::post('check/check_notice', 'Check/check_notice')->middleware('InterfaceLoginAuth'); // 未处理审核提醒
	Route::post('check/course/list', 'Check/courseList')->middleware('InterfaceLoginAuth'); // 课程产品列表
	Route::post('check/course/log', 'Check/courseLog')->middleware('InterfaceLoginAuth'); // 子级购买课程产品列表
	Route::post('check/auth', 'Check/auth'); // 注册时验证是否开通产品
    Route::post('check/product/list', 'Check/saveProductList')->middleware('InterfaceLoginAuth'); // 续费和开通产品列表
    Route::post('check/findProduct', 'Check/findProduct')->middleware('InterfaceLoginAuth'); // 查看课程详情
    Route::post('check/delCheckFist', 'Check/delCheckFist')->middleware('InterfaceLoginAuth'); // 删除预购订单

    Route::post('meeting/list', 'Meeting/list'); // 会员活动列表
    Route::post('meeting/getCityTeacher', 'Meeting/getCityTeacher'); // 查询所有讲师列表
    Route::post('meeting/desc', 'Meeting/getMeetingDetail'); // 查看活动详情
    Route::post('meeting/event', 'Meeting/reserveEvent'); // 会员预约活动
    Route::post('meeting/getMeetingCityList', 'Meeting/getMeetingCityList'); // 活动地址列表
    Route::post('meeting/getMeetingCityDetail', 'Meeting/getMeetingCityDetail'); // 活动地址详情
    Route::post('meeting/first/teacher', 'Meeting/firstTeacher'); // 查看平台内的讲师
    Route::post('meeting/calendar', 'Meeting/calendar'); // 月份查询每月几号有课
    Route::post('meeting/getCityList', 'Meeting/getCityList'); // 讲师地址筛选
    Route::post('meeting/getCityChild', 'Meeting/getCityChild'); // 获取活动筛选
    Route::post('meeting/getMomentsList', 'Meeting/getMomentsList'); // 精彩时刻列表
    Route::post('meeting/saveEvaluate', 'Meeting/saveEvaluate')->middleware('InterfaceLoginAuth'); // 发布评论内容
    Route::post('meeting/saveEvaluates', 'Meeting/saveEvaluates')->middleware('InterfaceLoginAuth'); // 发布评论内容s
    Route::post('meeting/support', 'Meeting/support')->middleware('InterfaceLoginAuth'); // 评论点赞/取消
    Route::post('meeting/delComment', 'Meeting/delComment')->middleware('InterfaceLoginAuth'); // 删除评论
    Route::post('meeting/getEventFirst', 'Meeting/getEventFirst'); // 会员预约详细
    Route::post('meeting/getMeetingFist', 'Meeting/getMeetingFist')->middleware('InterfaceLoginAuth'); // 评价活动详情
    Route::post('meeting/getUserEvaluate', 'Meeting/getUserEvaluate')->middleware('InterfaceLoginAuth'); // 用户评价列表

    Route::post('meeting/getMeetingEvent', 'Meeting/getMeetingEvent')->middleware('InterfaceLoginAuth'); // 会员取消预约
	Route::post('meeting/read', 'Meeting/read')->middleware('InterfaceLoginAuth'); // 活动提醒
	Route::post('meeting/edit', 'Meeting/edit')->middleware('InterfaceLoginAuth'); // 总部审核活动
    Route::post('meeting/delete', 'Meeting/delete')->middleware('InterfaceLoginAuth'); // 总部删除活动
    Route::post('meeting/update', 'Meeting/update')->middleware('InterfaceLoginAuth'); // 查看活动后修改为已读
    Route::post('meeting/getUserMeetingList', 'Meeting/getUserMeetingList')->middleware('InterfaceLoginAuth'); // 讲师活动列表
    Route::post('meeting/setMeetingCentent', 'Meeting/setMeetingCentent')->middleware('InterfaceLoginAuth'); // 修改活动内容
    Route::post('verify_qr_code', 'Meeting/verify_qr_code')->middleware('InterfaceLoginAuth'); // 活动创办人检测是否参加活动
    Route::post('meeting/getEventList', 'Meeting/getEventList')->middleware('InterfaceLoginAuth'); // 用户查看自己预约过的活动
    Route::post('meeting/getMeetingList', 'Meeting/getMeetingList')->middleware('InterfaceLoginAuth'); // 总部查看活动
    Route::post('meeting/delMeetingCity', 'Meeting/delMeetingCity')->middleware('InterfaceLoginAuth'); // 删除活动地址
    Route::post('meeting/verify_qr_user', 'Meeting/verify_qr_user')->middleware('InterfaceLoginAuth'); // 扫码查看信息

    Route::post('notice/list', 'notice/list')->middleware('InterfaceLoginAuth'); // 查看近期公告
    Route::post('notice/edit', 'notice/edit')->middleware('InterfaceLoginAuth'); // 修改公告状态
    Route::post('notice/index', 'notice/index')->middleware('InterfaceLoginAuth'); // 查看公告记录
    Route::post('notice/read', 'notice/read')->middleware('InterfaceLoginAuth'); // 查看公告详情

    Route::post('remind/list', 'remind/index')->middleware('InterfaceLoginAuth'); // 查看消息通知
    Route::post('remind/edit', 'remind/edit')->middleware('InterfaceLoginAuth'); // 修改消息状态
    Route::post('remind/delete', 'remind/delete')->middleware('InterfaceLoginAuth'); // 删除消息通知
    Route::post('remind/getUserGradeVIPRemind', 'remind/getUserGradeVIPRemind')->middleware('InterfaceLoginAuth'); // 好妈妈提醒
    Route::post('remind/index', 'remind/setParentRemind'); // 审核通知

	Route::post('config/read', 'Config/read'); // 平台配置查询
	
	Route::post('city/list', 'City/list'); // 查询地址列表
	Route::post('city/cityAllList', 'City/cityAllList'); // 查询全部地址列表

})->prefix('api.')->middleware('CrossDomain'); // 跨域中间件

// 后台接口路由
Route::group('admin', function () {
	Route::post('login', 'Login/login'); // 登录

	Route::post('upload', 'User/upload'); // 上傳文件
	
	Route::post('user/info', 'User/getUserInfo')->middleware('BackstageLoginAuth'); // 获取用户信息
    Route::post('user/save', 'User/save')->middleware('BackstageLoginAuth'); // 后台添加用户
    Route::post('user/list', 'User/list')->middleware('BackstageLoginAuth'); // 获取用户列表
    Route::post('user/edit', 'User/edit')->middleware('BackstageLoginAuth'); // 用户信息修改
    Route::post('user/child/list', 'User/getChildList')->middleware('BackstageLoginAuth'); // 用户下级列表
    Route::post('user/read', 'User/read')->middleware('BackstageLoginAuth'); // 查看用户详情
    Route::post('user/grade/list', 'User/userGradeList')->middleware('BackstageLoginAuth'); // 获取用户级别
    Route::post('parent/list', 'User/getParentList')->middleware('BackstageLoginAuth'); // 搜索推荐人
    Route::post('user/lecturer/list', 'User/lecturerList')->middleware('BackstageLoginAuth'); // 查询讲师列表

    Route::post('menu/save', 'Menu/save')->middleware(['BackstageLoginAuth']); // 添加菜单
	Route::post('menu/read', 'Menu/read')->middleware(['BackstageLoginAuth']); // 查看可用菜单
	Route::post('menu/index', 'Menu/index')->middleware(['BackstageLoginAuth']); // 查看菜单列表
	Route::post('menu/update', 'Menu/update')->middleware(['BackstageLoginAuth']); // 修改菜单参数
	Route::post('menu/delete', 'Menu/delete')->middleware(['BackstageLoginAuth']); // 删除菜单数据

	Route::post('remind/list', 'Remind/list')->middleware(['BackstageLoginAuth']); // 未读消息列表
	Route::post('remind/edit', 'Remind/edit')->middleware(['BackstageLoginAuth']); // 修改消息状态
	
	Route::post('role/save', 'Role/save')->middleware(['BackstageLoginAuth']); // 角色数据添加
	Route::post('role/list', 'Role/list')->middleware(['BackstageLoginAuth']); // 查看角色列表
	Route::post('role/edit', 'Role/edit')->middleware(['BackstageLoginAuth']); // 修改角色内容
	Route::post('role/update', 'Role/update')->middleware(['BackstageLoginAuth']); // 修改角色权限
	Route::post('role/delete', 'Role/delete')->middleware(['BackstageLoginAuth']); // 删除角色权限
	
	Route::post('config/save', 'Config/save')->middleware(['BackstageLoginAuth']); // 平台配置添加
	Route::post('config/list', 'Config/list')->middleware(['BackstageLoginAuth']); // 平台配置列表
	Route::post('config/edit', 'Config/edit')->middleware(['BackstageLoginAuth']); // 平台配置修改
	
	Route::post('meeting/save', 'Meeting/save')->middleware(['BackstageLoginAuth']); // 平台发布活动
	Route::post('meeting/list', 'Meeting/list')->middleware(['BackstageLoginAuth']); // 平台活动列表
	Route::post('meeting/edit', 'Meeting/edit')->middleware(['BackstageLoginAuth']); // 修改平台活动
	Route::post('meeting/delete', 'Meeting/delete')->middleware(['BackstageLoginAuth']); // 删除违规活动
	Route::post('meeting/read', 'Meeting/read')->middleware(['BackstageLoginAuth']); // 查询活动详情
	Route::post('meeting/update', 'Meeting/update')->middleware(['BackstageLoginAuth']); // 修改活动状态
	Route::post('meeting/stage', 'Meeting/stage')->middleware(['BackstageLoginAuth']); // 增加课时
	Route::post('meeting/stageList', 'Meeting/stageList')->middleware(['BackstageLoginAuth']); // 查看课时列表
	Route::post('meeting/stageDel', 'Meeting/stageDel')->middleware(['BackstageLoginAuth']); // 课时删除
	Route::post('meeting/first/teacher', 'Meeting/firstTeacher')->middleware(['BackstageLoginAuth']); // 查询平台中的讲师
	Route::post('meeting/saveLecturer', 'Meeting/saveLecturer')->middleware(['BackstageLoginAuth']); // 课时录入讲师
	Route::post('meeting/teacherList', 'Meeting/teacherList')->middleware(['BackstageLoginAuth']); // 课时讲师列表
	Route::post('meeting/teacherDel', 'Meeting/teacherDel')->middleware(['BackstageLoginAuth']); // 删除课时讲师
	Route::post('meeting/openTeacher', 'Meeting/openTeacher')->middleware(['BackstageLoginAuth']); // 开通讲师权限

	Route::post('notice/save', 'notice/save')->middleware(['BackstageLoginAuth']); // 公司发布公告
	Route::post('notice/list', 'notice/list')->middleware(['BackstageLoginAuth']); // 查看公告列表
	Route::post('notice/read', 'notice/read')->middleware(['BackstageLoginAuth']); // 查看公告详情
	Route::post('notice/delete', 'notice/delete')->middleware(['BackstageLoginAuth']); // 后台删除公告
	Route::post('notice/time/save', 'notice/save_notice_time')->middleware(['BackstageLoginAuth']); // 添加定时发布
	Route::post('notice/time/list', 'notice/list_notice_time')->middleware(['BackstageLoginAuth']); // 定时发布列表
	Route::post('notice/time/edit', 'notice/edit_notice_time')->middleware(['BackstageLoginAuth']); // 取消定时发布

	Route::post('log/list', 'Log/list')->middleware(['BackstageLoginAuth']); // 查看后台日志
	
	Route::post('city/list', 'City/list'); // 查询地址列表
	
})->prefix('admin.')->middleware('CrossDomain'); // 跨域中间件

// 答题测试系统
Route::group('exam', function () {
    Route::post('login', 'Exam/login'); // 登录
    Route::post('import', 'Exam/import'); // 导入答题系统
    Route::post('createOrder', 'Exam/createOrder'); // 发起支付请求
    Route::post('getExamList', 'Exam/getExamList'); // 查询全部题目
    Route::post('setExamSave', 'Exam/setExamSave'); // 提交题目答案
    Route::post('wechatAuthLogin', 'Exam/wechatAuthLogin'); // 微信授权登录
    Route::post('getUserList', 'Exam/getUserList'); // 获取用户列表
    Route::post('setReportList', 'Exam/setReportList'); // 生成报告列表
    Route::post('getLogin', 'Exam/getLogin'); // 管理员登录
    Route::post('queryOrder', 'Exam/queryOrder'); // 查询微信订单
    Route::post('getDateList', 'Exam/getDateList'); // 营业额列表

})->prefix('exam.')->middleware('CrossDomain'); // 跨域中间件