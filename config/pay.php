<?php
/**
 * @Description: 微信支付参数
 * @Author: 林怼怼
 * @Date: 2025/1/20
 * @Time: 17:48
 */
return [
    // 微信支付相关配置
    'app_id'       => 'wxe580b7e06fce927b',         // 微信公众平台的应用ID
    'mch_id'       => '1616344636',         // 商户号
    'v2_key'       => 'WBHWuYm2pQ4FuJsT6SbQE289pKvl8ZQH',         // 微信支付v2密钥
    'v3_key'       => 'WBHWuYm2pQ4FuJsT6SbQE289pKvl8ZQH',         // 微信支付v3密钥
    'serial_no'    => '4E95165174E43E4CC1E9FFC2C07C4623DFB8DA49',      // 商户证书序列号
    'cert_path'    => app()->getRootPath() . 'certs/apiclient_cert.pem', // 商户证书路径
    'key_path'     => app()->getRootPath() . 'certs/apiclient_key.pem',  // 商户证书私钥路径
    'platform_cert'=> app()->getRootPath() . 'certs/platform_cert.pem',  // 微信支付平台证书路径
    'notify_url'   => 'https://dev.aimanrenjian.net:8443/php/api/huilefu_member/exam/notify', // 微信支付异步通知回调地址
];


