<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
		// 定时任务命令
		'timer'=>\app\command\Timer::class,
		// GatewayWorker命令
		'worker:gateway_win' => 'app\controller\GatewayWorkerForWin',
    ],
];
