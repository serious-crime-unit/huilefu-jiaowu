<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Workerman设置 仅对 php think worker:gateway 指令有效
// +----------------------------------------------------------------------
return [
	// 扩展自身需要的配置
	'protocol'              => 'websocket', // 协议 支持 tcp udp unix http websocket text
	'host'                  => env('gateway.host','0.0.0.0'), // 监听地址
	'port'                  => env('gateway.port',1236), // 监听端口
	'socket'                => '', // 完整监听地址
	'context'               => [], // socket 上下文选项
	'register_deploy'       => true, // 是否需要部署register
	'businessWorker_deploy' => true, // 是否需要部署businessWorker
	'gateway_deploy'        => true, // 是否需要部署gateway
	
	// Register配置
	'registerAddress'       => env('gateway.register_address','127.0.0.1:1236'),
	
	// Gateway配置
	'name'                  => 'ZsrdCrmGateway',
	'count'                 => 4,
	'lanIp'                 => env('gateway.lan_ip','127.0.0.1'),
	'startPort'             => 2000,
	'daemonize'             => false,
	'pingInterval'          => 30,
	'pingNotResponseLimit'  => 0,
	'pingData'              => '',
	
	// BusinsessWorker配置
	'businessWorker'        => [
		'name'         => 'BusinessWorker',
		'count'        => 1,
		'eventHandler' => 'app\controller\GatewayEvents',
	],

];
