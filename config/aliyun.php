<?php
// config/aliyun.php
	
	return [
		// 阿里云短信API配置信息
		'access_key_id' => env('ALY_OSS.ALY_ACCESSKEY_ID', ''),
		'access_key_secret' => env('ALY_OSS.ALY_ACCESSKEY_SECRET', ''),
		'sign_name' => env('ALY_SMS.ALY_SIGN', ''),
		'template_code' => env('ALY_SMS.ALY_TEMPLATECODE', ''),
	];