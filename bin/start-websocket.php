#!/usr/bin/env php
<?php

use app\websocket\Server;

require_once __DIR__ . '/../vendor/autoload.php';

// 启动 WebSocket 服务
(new Server())->start();
