<?php

namespace app\websocket;

use Workerman\Worker;

require_once __DIR__ . '/../../../vendor/autoload.php';

class Server
{
    // 保存用户与连接的绑定关系
    protected $userConnections = [];

    public function start()
    {
        // 创建一个 WebSocket 服务
        $wsWorker = new Worker("websocket://0.0.0.0:2346");

        // 设置 Worker 进程数量
        $wsWorker->count = 4;

        // 当客户端连接时触发
        $wsWorker->onConnect = function ($connection) {
            echo "新客户端连接：" . $connection->id . "\n";

            // 给每个连接分配一个标识符（临时）
            $connection->onWebSocketConnect = function ($connection) {
                // 通过连接参数绑定用户，例如：ws://127.0.0.1:2346?user_id=123
                if (isset($_GET['user_id'])) {
                    $user_id = $_GET['user_id'];
                    $connection->user_id = $user_id;

                    // 绑定用户与连接
                    $this->userConnections[$user_id] = $connection;
                    echo "用户绑定成功：user_id = $user_id\n";
                }
            };
        };

        // 当收到客户端消息时触发
        $wsWorker->onMessage = function ($connection, $data) {
            echo "收到消息：" . $data . "\n";

            // 示例：发送回执给用户
            $connection->send("服务端已收到消息：" . $data);
        };

        // 当客户端断开连接时触发
        $wsWorker->onClose = function ($connection) {
            echo "客户端断开：" . $connection->id . "\n";

            // 清理绑定的用户
            if (isset($connection->user_id)) {
                unset($this->userConnections[$connection->user_id]);
                echo "用户解绑成功：user_id = " . $connection->user_id . "\n";
            }
        };

        // 运行 Worker
        Worker::runAll();
    }

    // 向指定用户发送消息
    public function sendMessageToUser($user_id, $message)
    {
        if (isset($this->userConnections[$user_id])) {
            $connection = $this->userConnections[$user_id];
            $connection->send($message);
            echo "消息发送成功：user_id = $user_id, message = $message\n";
        } else {
            echo "消息发送失败：用户未连接 user_id = $user_id\n";
        }
    }
}

// 启动 WebSocket 服务
(new Server())->start();