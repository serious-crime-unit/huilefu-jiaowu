<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;

class Remind
{
	
	/**
	 * Created by PhpStorm.
	 * @purpose 消息修改为已读状态
	 * @Author: Linxy
	 * @Time: 2023/10/12 16:50
	 */
	public function edit()
	{
		$param = input();
		$userInfo = getUserInfo();
		$result = Db::name('remind')
            ->where('id', 'in', $param['ids'])
            ->where('c_id', $userInfo['id'])
            ->save(['state' => 2]);
		return json(['code' => 200, 'msg' => '读取消息成功。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 未读消息列表
	 * @Author: Linxy
	 * @Time: 2023/10/12 16:27
	 */
	public function list()
	{
		$param = input();
		$userInfo = getUserInfo();
		$data = Db::name('remind')
			->alias('a')
			->join('member b', 'a.u_id = b.id')
			->where(['a.c_id' => $userInfo['id'], 'a.state' => 1])
			->order('a.time')
			->field('a.id, a.r_id, b.avatar, b.name, a.desc, a.time')
			->select()
			->toArray();
		$count = Db::name('remind')
			->where(['c_id' => $userInfo['id']])
			->order('time')
			->count();
		return json(['code' => 200, 'msg' => '消息列表。', 'data' => $data, 'count' => $count]);
	}
}
