<?php
declare (strict_types = 1);

namespace app\controller\admin;
use think\facade\Db;

class Notice
{
    /**
     * @Description: 取消定时发布
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 16:40
     */
    public function edit_notice_time()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            Db::name('notice_time')->whereIn('id', $id)->delete();
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '失败', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 定时公告列表
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 16:19
     */
    public function list_notice_time()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            $data = Db::name('notice_time')
                ->page((int)$param['page'], (int)$param['limit'])
                ->select()->toArray();
            $count = Db::name('notice_time')
                ->page((int)$param['page'], (int)$param['limit'])
                ->count();
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data, 'count' => $count]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '失败', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 定时发布公告
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 15:43
     */
    public function save_notice_time()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            // 查看是否通知全部人等于2只通知指定的人
            if ($param['type'] == 1) {
                $param['ids'] = implode(',', Db::name('member')->where([
                    ['id', '<>', 1],
                    ['state', '=', 1]
                ])->column('id'));
            }
            // 定时发布公告
            $id = Db::name('notice_time')->insert([
                'title' => $param['title'],
                'u_id' => $userInfo['id'],
                'img' => $param['img'],
                'desc' => $param['desc'],
                'ids' => $param['ids'],
                'message_time' => strtotime($param['message_time']),
                'time' => time()
            ]);
            $desc = $userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'添加了定时发布了公告'. $param['title'] .'。';
            BackstageLog($userInfo['id'], '平台公告', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '失败', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 后台删除公告
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 14:31
     */
    public function delete()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            Db::name('notice')->whereIn('id', $id)->delete();
            Db::name('notice_user')->whereIn('id', $id)->delete();
            $desc = $userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'删除了公告'. $id .'。';
            BackstageLog($userInfo['id'], '平台公告', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查看公告详情
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 14:19
     */
    public function read()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            $data = Db::name('notice')
                ->where(['id' => $param['id']])
                ->find();
            $data['userInfo'] = '';
            if ($data['state'] === 2) {
                $data['userInfo'] = Db::name('notice_user')
                    ->alias('t1')
                    ->join('member t2', 't1.u_id = t2.id')
                    ->where('t1.id', $param['id'])
                    ->field('t2.id, t2.name, t2.avatar')
                    ->select()
                    ->toArray();
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查看公告列表
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 14:07
     */
    public function list()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            $data = Db::name('notice')
                ->where(['u_id' => $userInfo['id']])
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->select()->toArray();
            $count = Db::name('notice')
                ->where(['u_id' => $userInfo['id']])
                ->page((int)$param['page'], (int)$param['limit'])
                ->count();
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data, 'count' => $count]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 发布公告
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 13:33
     */
    public function save()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            // 查看是否通知全部人等于2只通知指定的人
            if ($param['type'] == 1) {
                $param['ids'] = implode(',', Db::name('member')->where([
                    ['id', '<>', 1],
                    ['state', '=', 1]
                ])->column('id'));
            }
            $ids = explode(',', $param['ids']);
            $count = count($ids);
            // 添加公告
            $id = Db::name('notice')->insertGetId([
                'title' => $param['title'],
                'u_id' => $userInfo['id'],
                'img' => $param['img'],
                'desc' => $param['desc'],
                'state' => $param['type'],
                'time' => time()
            ]);
            $data = [];
            for ($i = 0; $i < $count; $i++) {
                $data[] = ['id' => $id, 'u_id' => $ids[$i], 'state' => 2, 'time' => time()];
            }
            Db::name('notice_user')->insertAll($data);
            $desc = $userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'发布了公告'. $param['title'] .'。';
            BackstageLog($userInfo['id'], '平台公告', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '失败', 'error' => $e->getMessage()]);
        }
    }
}
