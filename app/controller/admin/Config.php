<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;

class Config
{
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改配置文件
	 * @Author: Linxy
	 * @Time: 2023/10/18 11:08
	 */
	public function edit()
	{
		$param = input();
		$result = Db::name('config')->save($param);
		return json(['code' => 200, 'msg' => '修改配置成功。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 配置列表
	 * @Author: Linxy
	 * @Time: 2023/10/18 10:59
	 */
	public function list()
	{
		$param = input();
		$data = Db::name('config')
			->page((int) $param['page'], (int) $param['limit'])
			->select()
			->toArray();
		$count = Db::name('config')->count();
		return json(['code' => 200, 'msg' => '查询配置列表成功。', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 协议添加
	 * @Author: Linxy
	 * @Time: 2023/10/18 10:48
	 */
	public function save()
	{
		$param = input();
		$param['time'] = time();
		$result = Db::name('config')->save($param);
		return json(['code' => 200, 'msg' => '添加配置成功。']);
	}
}
