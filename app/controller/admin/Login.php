<?php
declare (strict_types = 1);

namespace app\controller\admin;
use think\facade\Db;

class Login
{
	/**
	 * Created by PhpStorm.
	 * @purpose 后台登录
	 * @Author: Linxy
	 * @Time: 2023/10/8 17:39
	 */
	public function login()
	{
		$param = input();
		$account = Db::table('jw_member')->where('phone', $param['account'])->find();
		if (!$account) {
			$account = Db::name('member')->where('number', $param['account'])->find();
			if (!$account) return json(['code' => 201, 'msg' => '账号不存在，请确认账号是否正确！']);
		}
		if ($account['pwd'] != encrypt_password($param['pwd'])) return json(['code' => 201, 'msg' => '密码不正确，请确认后重试！']);
		if ($account['is_admin'] === 2) return json(['code' => 201, 'msg' => '该用户无权限访问后台！']);
		if ($account['state'] === 2) return json(['code' => 201, 'msg' => '该用户已冻结！']);
		if ($account['state'] === 3) return json(['code' => 201, 'msg' => '该用户已删除！']);
		if ($account['state'] === 0) return json(['code' => 201, 'msg' => '该用户审核中！']);
		$desc = '会员'.$account['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'登录教务管理系统。';
		BackstageLog($account['id'], '登录教务管理系统', $desc);
		return json(['code' => 200, 'msg' => '欢迎'. $account['name'] .'登录。', 'token' => encryptToken($account['id'])]);
	}
}
