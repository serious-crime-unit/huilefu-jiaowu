<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;
use think\Request;

class User
{
    /**
     * @Description: 查询讲师列表
     * @Author: 林怼怼
     * @Date: 2024/12/25
     * @Time: 16:47
     */
    public function lecturerList()
    {
        $param = input();
    }

    /**
     * @Description: 总部录入讲师
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 9:48
     */
    public function saveLecturer()
    {
        $param = input();

    }

    /**
     * @Description: 搜索推荐人
     * @Author: 林怼怼
     * @Date: 2024/3/12/012
     * @Time: 17:25
     */
    public function getParentList()
    {
        $param = input();
        $data = Db::name('member')
            ->where([
                ['state', '=', 1],
                ['name|phone', 'like', '%'.$param['search']]
            ])
            ->field('id, name, phone')
            ->page((int) $param['page'], (int) $param['limit'])
            ->select()->toArray();
        $count = Db::name('member')
            ->where([
                ['state', '=', 1],
                ['name|phone', 'like', '%'.$param['search']]
            ])
            ->field('id, name, phone')
            ->count();
        return json(['code' => 200, 'msg' => '操作成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description:
     * @Author: 林怼怼
     * @Date: 2024/3/7/007
     * @Time: 16:37
     */
    public function userGradeList()
    {
        $data = Db::name('grade')->select()->toArray();
        return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
    }

    /**
     * @Description: 查询用户详情
     * @Author: 林怼怼
     * @Date: 2023/12/21/021
     * @Time: 10:05
     */
    public function read()
    {
        $id = input('id');
        $data = Db::name('member')
            ->alias('t1')
            ->leftJoin('member_address t2', 't1.id = t2.id AND t2.is_master = 1 AND t2.state = 1')
            ->leftJoin('city t3', 't2.p_id = t3.id')
            ->leftJoin('city t4', 't2.c_id = t4.id')
            ->leftJoin('city t8', 't2.c_id = t8.id')
            ->leftJoin('city t5', 't2.d_id = t5.id')
            ->leftJoin('user_role t6', 't1.id = t6.id')
            ->leftJoin('role t7', 't6.r_id = t7.id')
            ->leftJoin('member_parent t10', 't1.id = t10.id')
            ->leftJoin('member t9', 't10.p_id = t9.id')
            ->fieldRaw('t1.id, t1.name, t1.avatar, t1.phone, t1.number, t9.id as p_id, t9.name as userParentTop, t7.id as r_id, IFNULL(t7.code, "") as roles, IFNULL(t3.cityName, "") province_name, IFNULL(t4.cityName, "") city_name, IFNULL(t5.cityName, "") area_name, t2.address, t1.state, t1.is_admin, t1.time')
            ->find($id);
        // 获取等级
        $data['level'] = array_level($data['id']);
        $data['pay1'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 1])->value('num');
        $data['pay2'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 2])->value('num');
        $data['pay3'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 3])->value('num');
        $data['pay4'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 4])->value('num');
        $data['pay5'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 5])->value('num');
        $data['pay6'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 6])->value('num');
        $data['pay7'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 7])->value('num');
        if (in_array(3, $data['level']) || in_array(6, $data['level']) || in_array(9, $data['level'])){
            $data['userParentMaster'] = Db::name('member')->where('id', 1)->value('name');
        }else{
            $data['userParentMaster'] = getParent_365_521($data['id'])['name'];
        }
        return json(['code' => 200, 'msg' => '获取会员信息。', 'data' => $data]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 上傳文件
	 * @Author: Linxy
	 * @Time: 2023/10/17 15:52
	 */
	public function upload(Request $request)
	{
		// 启动事务
		Db::startTrans();
		try {
			$files = $_FILES['file'];
			$userInfo = getUserInfo();
			// 判断文件类型
			$ext = strtolower(pathinfo(@$files['name'], PATHINFO_EXTENSION));
			if (!in_array($ext, ['jpg', 'png', 'jpeg', 'gif'])) {
				return json(['code'=>0,'msg'=>'文件类型错误！']);
			}
			// 临时存放路径
			$path = $_SERVER['DOCUMENT_ROOT']."/uploads/";
			// 判断是否存在上传到的目录
			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}
			// 生成唯一的文件名
			$fileName = md5(date('Y-m-d H:i:s:u')) . '.' . $ext;
			// 将文件名拼接到指定的目录下
			$destName = $path . $fileName;
			// 进行文件移动
			if (!move_uploaded_file($files['tmp_name'], $destName)) {
				return json(['code'=>0,'msg'=>'文件上传失败！']);
			}
			// 上传到oss
			$result = uploadFile($fileName, $destName);
			// 删除临时文件
			unlink($destName);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'上传文件'. $result['info']['url'] .'。';
			BackstageLog($userInfo['id'], '上传文件', $desc);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '上传文件成功。', 'img' => $result['info']['url']]);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 用户下级列表
	 * @Author: Linxy
	 * @Time: 2023/10/17 9:28
	 */
	public function getChildList()
	{
        $param = input();
        $userInfo = Db::name('member')
            ->alias('t1')
            ->leftJoin('member_address t2', 't1.id = t2.id AND t2.is_master = 1 AND t2.state = 1')
            ->leftJoin('city t3', 't2.p_id = t3.id')
            ->leftJoin('city t4', 't2.c_id = t4.id')
            ->leftJoin('city t8', 't2.c_id = t8.id')
            ->leftJoin('city t5', 't2.d_id = t5.id')
            ->leftJoin('user_role t6', 't1.id = t6.id')
            ->leftJoin('role t7', 't6.r_id = t7.id')
            ->fieldRaw('t1.id, t1.name, t1.avatar, t1.phone, t1.number, IFNULL(t7.code, "") as roles, IFNULL(t3.cityName, "") province_name, IFNULL(t4.cityName, "") city_name, t8.id city_id, IFNULL(t5.cityName, "") area_name, t2.address, t1.state, t1.is_admin, t1.time')
            ->find($param['id']);
        $where = [];
        // 搜索
        $where[] = ['t1.name|t1.phone|t1.number', 'like', '%'.$param['search'].'%'];
        // 级别筛选
        if (!empty($param['level'])) $where[] = ['t5.grade', 'in', $param['level']];
        // 状态筛选
        if (!empty($param['state'])) $where[] = ['t1.state', '=', $param['state']];
        if ($param['type'] == 1) {
            // 查询直接下级
            $ids = Db::name('member_parent')
                ->alias('t1')
                ->leftJoin('member t2', 't1.id = t2.id')
                ->leftJoin('member_grade t3', 't2.id = t3.id')
                ->where([
                    ['t1.p_id', 'in', $userInfo['id']]
                ])
                ->column('t1.id');
        }elseif($param['type'] == 2) {
            // 查询间接下级
            $ids = Db::name('member_parent')
                ->alias('t1')
                ->where([
                    ['t1.p_id', 'in', $userInfo['id']]
                ])
                ->column('t1.id');
            $ids = getChildUserPhoneS($ids);
        }else{
            // 全部
            $ids = getChildUserPhoneS($userInfo['id']);
        }
        $where[] = ['t1.id', 'in', $ids];
        // 子查询条件
        $subQueryWhere = [];
        // 子查询
        $subQuery = Db::name('member_parent')
            ->alias('t1')
            ->leftJoin('member_grade t2', 't1.id = t2.id')
            ->where($subQueryWhere)
            ->field('t1.p_id, count(1) num')
            ->group('t1.p_id')
            ->buildSql();
        $data = Db::name('member')
            ->alias('t1')
            ->leftJoin("$subQuery t2", 't1.id = t2.p_id')
            ->leftJoin("member_parent t4", 't1.id = t4.p_id')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, IFNULL(t2.num, 0) num, t1.state')
            ->where($where)
            ->group('t1.id')
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('num', 'desc')
            ->select()
            ->toArray();
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("$subQuery t2", 't1.id = t2.p_id')
            ->leftJoin("member_parent t4", 't1.id = t4.p_id')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, IFNULL(t2.num, 0) num, t1.state')
            ->where($where)
            ->group('t1.id')
            ->count();
        foreach ($data as &$datum) {
            $datum['level'] = array_level($datum['id']);
        }
        return json(['code' => 200, 'msg' => '查询用户列表成功', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改用户信息
	 * @Author: Linxy
	 * @Time: 2023/10/11 16:59
	 */
	public function edit()
	{
        $param = input();
        $userInfo = getUserInfo();
        // 查询当前登录的用户
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_parent t2', 't1.id = t2.id')
            ->where(['t1.id' => $param['id']])
            ->field('t1.id, t1.phone, t1.name, t2.p_id')
            ->find();

        // 启动事务
        Db::startTrans();
        try {
            // 添加会员
            Db::name('member')->save([
                'id' => $param['id'],
                'name' => $param['name'],
                'avatar' => $param['avatar'],
                'desc' => $param['desc'],
                'phone' => $param['phone'],
                'is_admin' => $param['is_admin'],
                'is_teacher' => $param['is_teacher'],
                'state' => $param['state'],
                'time' => time(),
            ]);
            // 记录地址
            Db::name('member_address')
                ->where('id', $param['id'])
                ->update([
                    'p_id' => $param['province'],
                    'c_id' => $param['city'],
                    'd_id' => $param['area'],
                    'address' => $param['address'],
                    'is_master' => 1,
                ]);
            // 删除用户等级
            Db::name('member_grade')->where('id', $param['id'])->delete();
            // 记录用户等级
            foreach ($param['level'] as $value) {
                if ($value == 1 || $value == 4 || $value == 7) {
                    // 记录用户等级
                    Db::name('member_grade')->save([
                        'id' => $param['id'],
                        'grade' => $value,
                        'vip_start_time' => time(),
                        'vip_end_time' => strtotime("+1 year", time())
                    ]);
                }else{
                    // 记录用户等级
                    Db::name('member_grade')->save([
                        'id' => $param['id'],
                        'grade' => $value,
                        'vip_start_time' => null,
                        'vip_end_time' => null
                    ]);
                }
            }
            // 记录推荐人
            Db::name('member_parent')->where('id', $param['id'])->update(['p_id' => $param['p_id']]);
            // 是否需要添加权限
            if ($param['r_id'] != "") {
                // 查询当前角色权限
                $role = Db::name('user_role')->find($param['id']);
                // 查看是否设置角色权限
                if (!$role) {
                    // 没设置权限添加角色权限
                    Db::name('user_role')->where('id', $param['id'])->update([
                        'r_id' => $param['r_id'],
                        'time' => time()
                    ]);
                }
            }
            // 记录日志
            $desc = '后台'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'添加了账号'. $param['name'] .'。';
            BackstageLog($userInfo['id'], '添加用户', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '修改用户成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 冻结用户账号
	 * @Author: Linxy
	 * @Time: 2023/10/11 16:43
	 */
	public function delete()
	{
		$param = input();
		// 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			$data = Db::name('member')->find($param['id']);
			$result = Db::name('member')->save(['id' => $param['id'], 'state' => $param['del']]);
			if ($param['del'] == 1) {
				$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员'. $data['name'] .'的状态为正常。';
				BackstageLog($userInfo['id'], '修改状态', $desc);
			}else if ($param['del'] == 2) {
				$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员'. $data['name'] .'的状态为冻结。';
				BackstageLog($userInfo['id'], '修改状态', $desc);
			}else if ($param['del'] == 3) {
				$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员'. $data['name'] .'的状态为删除。';
				BackstageLog($userInfo['id'], '修改状态', $desc);
			}else{
				$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员'. $data['name'] .'的状态为异常。';
				BackstageLog($userInfo['id'], '修改状态', $desc);
			}
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '修改用户状态成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 用户列表
	 * @Author: Linxy
	 * @Time: 2023/10/11 15:32
	 */
	public function list()
	{
        $param = input();
        $where = [];
        // 搜索
        $where[] = ['t1.name|t1.phone|t1.number', 'like', '%'.$param['search'].'%'];

        if (!empty($param['state']))  $where[] = ['t2.grade', 'in', $param['level']];
        // 状态筛选
        if (!empty($param['state'])) $where[] = ['t1.state', '=', $param['state']];
        // 讲师筛选
        if (!empty($param['is_teacher'])) $where[] = ['t1.is_teacher', '=', $param['is_teacher']];
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->field('t1.id, t1.avatar, t1.pinyin, t1.name, t1.desc, t1.phone, t1.is_teacher, t1.is_admin, t1.state, t1.time')
            ->where($where)
            ->group('t1.id')
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.time', 'desc')
            ->select()
            ->toArray();
        $count = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->where($where)
            ->group('t1.id')
            ->order('t1.time', 'desc')
            ->count();
        foreach ($data as &$item) {
            $item['level'] = Db::name('member_grade')->where(['id' => $item['id']])->column('grade');
            $item['p_id'] = Db::name('member_parent')->where(['id' => $item['id']])->value('p_id');
            $item['parent_name'] = Db::name('member')->where(['id' => $item['p_id']])->value('name');
            $item['r_id'] = Db::name('user_role')->where(['id' => $item['id']])->value('r_id');
            $city = Db::name('member_address')->where(['id' => $item['id']])->find();
            $item['province'] = $city['p_id'];
            $item['city'] = $city['c_id'];
            $item['area'] = $city['d_id'];
            $item['address'] = $city['address'];
        }
        return json(['code' => 200, 'msg' => '查询用户列表成功', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 后台添加用户
	 * @Author: Linxy
	 * @Time: 2023/10/11 10:37
	 */
	public function save()
	{
		$param = input();
        $userInfo = getUserInfo();

        // 查看是否注册
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_parent t2', 't1.id = t2.id')
            ->where('phone', $param['phone'])
            ->field('t1.id, t2.p_id')
            ->find();
        if ($data) return json(['code' => 201, 'msg' => '该手机号已经注册过了，请更换手机号。']);

        // 生成编号
        $number = substr(sprintf('%07s', Db::name('member')->max('number')+1), -6);

        // 启动事务
        Db::startTrans();
        try {
            // 添加会员
            $id = Db::name('member')->insertGetId([
                'name' => $param['name'],
                'avatar' => $param['avatar'],
                'desc' => $param['desc'],
                'phone' => $param['phone'],
                'number' => $number,
                'is_admin' => $param['is_admin'],
                'is_teacher' => $param['is_teacher'],
                'state' => $param['state'],
                'time' => time(),
            ]);
            // 记录地址
            Db::name('member_address')
                ->save([
                    'id' => $id,
                    'p_id' => $param['province'],
                    'c_id' => $param['city'],
                    'd_id' => $param['area'],
                    'address' => $param['address'],
                    'is_master' => 1,
                ]);
            // 记录用户等级
            foreach ($param['level'] as $value) {
                if ($value == 1 || $value == 4 || $value == 7) {
                    // 记录用户等级
                    Db::name('member_grade')->save([
                        'id' => $id,
                        'grade' => $value,
                        'vip_start_time' => time(),
                        'vip_end_time' => strtotime("+1 year", time())
                    ]);
                }else{
                    // 记录用户等级
                    Db::name('member_grade')->save([
                        'id' => $id,
                        'grade' => $value,
                        'vip_start_time' => null,
                        'vip_end_time' => null
                    ]);
                }
            }
            // 记录推荐人
            Db::name('member_parent')->save(['id' => $id, 'p_id' => $param['p_id']]);
            // 增加余额
            Db::name('member_pay')->insertAll([
                ['id' => $id, 'type' => 1],
                ['id' => $id, 'type' => 2],
                ['id' => $id, 'type' => 3],
                ['id' => $id, 'type' => 4],
                ['id' => $id, 'type' => 6],
            ]);
//            $type = 1;
//            if ($param['level'] == 1) {
//                $type = 1;
//            }
//            // 审核记录增加
//            $cid = Db::name('order')->insertGetId([
//                'u_id' => $id,
//                'p_id' => 1,
//                'c_id' => $type,
//                'year' => 1,
//                'type' => 1,
//                'state' => 1,
//                'vip_start_time' => time(),
//                'vip_end_time' => strtotime("+1 year", time()),
//                'time' => time()
//            ]);
            // 是否需要添加权限
            if ($param['r_id'] != "") {
                // 查询当前角色权限
                $role = Db::name('user_role')->find($id);
                // 查看是否设置角色权限
                if (!$role) {
                    // 没设置权限添加角色权限
                    Db::name('user_role')->insert([
                        'id' => $id,
                        'r_id' => $param['r_id'],
                        'time' => time()
                    ]);
                }
            }
            // 记录日志
            $desc = '后台'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'添加了账号'. $param['name'] .'。';
            BackstageLog($userInfo['id'], '添加用户', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '添加用户成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改用户权限
	 * @Author: Linxy
	 * @Time: 2023/10/10 17:25
	 */
	public function menuEdit()
	{
		$param = input();
		// 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			$param['time'] = time();
			$data = Db::name('user_role')->find($param['id']);
			if ($data) {
				$result = Db::name('user_role')->save($param);
			}else{
				$result = Db::name('user_role')->insert($param);
			}
			$data = Db::name('member')->field('name')->find($param['id']);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改了会员'. $data['name'] .'的身份，修改为'. $data['name'] .'。';
			BackstageLog($userInfo['id'], '修改权限', $desc);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '用户权限已修改。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 获取家庭中心审核人邀请人信息
	 * @Author: Linxy
	 * @Time: 2023/10/9 17:13
	 */
	public function reviewer()
	{
		$param = input();
		$data = Db::name('member')->where('review_phone', $param['phone'])->field('id, name, avatar, phone, level, number, state')->find();
		if ($data) return json(['code' => 200, 'msg' => '查询审核人信息成功。', 'data' => $data]);
		$data = Db::name('member')->where('invite_phone', $param['phone'])->field('id, name, avatar, phone, level, number, state')->find();
		if ($data) return json(['code' => 200, 'msg' => '查询邀请人信息成功。', 'data' => $data]);
		return json(['code' => 200, 'msg' => '查询的推荐人不存在，或者您已经是最高级别。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 获取用户信息
	 * @Author: Linxy
	 * @Time: 2023/10/9 15:14
	 */
	public function getUserInfo()
	{
        $userInfo = getUserInfo();
		return json(['code' => 200, 'msg' => '查询会员信息成功。', 'data' => $userInfo, 'token' => encryptToken($userInfo['id'])]);
	}
}
