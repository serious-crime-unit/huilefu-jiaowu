<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Cache;
use think\facade\Db;

class Check
{
	
	/**
	 * Created by PhpStorm.
	 * @purpose 审核用户/名额
	 * @Author: Linxy
	 * @Time: 2023/10/13 10:01
	 */
	public function edit()
	{
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo();
            if ($param['type'] == 1) {
                // 会员审核
                if ($param['state'] == 2) {
                    // 通过
                    // 获取审核记录
                    $data = Db::name('check')
                        ->where([
                            'id' => $param['id'],
                            'u_id' => $param['u_id'],
                            'c_id' => $param['c_id']
                        ])
                        ->find();
                    if ($param['c_id'] != $userInfo['id']) return json(['code' => 201, 'msg' => '审核人错误']);
                    // 修改审核状态
                    $res = Db::name('check')->save(['id' => $param['id'], 'state' => $param['state'], 'time' => time()]);
                    // 获取用户数据
                    $u_id = Db::name('member')->find($param['u_id']);
                    // 获取审核人数据
                    $c_id = Db::name('member')->find($param['c_id']);
                    if ($u_id['level'] == 3 && $userInfo['level'] < 4) return json(['code' => 201, 'msg' => '权限不足，主任级别必须由总部审核。']);
                    // 审核通过减少一个库存
                    if ($data['type'] === 1) {
                        if ($c_id['quota'] === 0) return json(['code' => 204, 'msg' => '365会员余额不足，先去购买一点吧。']);
                        $type = 2;
                        Db::name('member')
                            ->where('id', $param['c_id'])
                            ->dec('quota', 1)
                            ->update();
                    }else if ($data['type'] === 3) {
                        if ($c_id['lele_balance'] === 0) return json(['code' => 204, 'msg' => '了了派余额不足，先去购买一点吧。']);
                        $type = 1;
                        Db::name('member')
                            ->where('id', $param['c_id'])
                            ->dec('lele_balance', 1)
                            ->update();
                    }elseif ($data['type'] === 5) {
                        if ($c_id['lele_balance'] === 0) return json(['code' => 204, 'msg' => '了了派余额不足，先去购买一点吧。']);
                        if ($c_id['quota'] === 0) return json(['code' => 204, 'msg' => '365会员余额不足，先去购买一点吧。']);
                        $type = 0;
                        Db::name('member')
                            ->where('id', $param['c_id'])
                            ->dec('quota', 1)
                            ->update();
                        Db::name('member')
                            ->where('id', $param['c_id'])
                            ->dec('lele_balance', 1)
                            ->update();
                    }else{
                        return json(['code' => 201, 'msg' => '订单异常请联系工作人员。']);
                    }
                    if ($u_id['type'] === 1 && $type === 2) {
                        $type = 0;
                    }elseif ($u_id['type'] === 2 && $type === 1) {
                        $type = 0;
                    }
                    // 账号状态改为正常
                    Db::name('member')->save(['id' => $param['u_id'], 'state' => 1, 'type' => $type, 'time' => time()]);
                    // 注册成功后判定是否需要升级
                    $data = Db::name('member')->where(['phone' => $u_id['invite_phone'], 'level' => 1])->find();
                    if ($data) upgrade($data);
                    if ($type == 1) {
                        // 如果学员是优秀学员加一百积分
                        integralInc($param['u_id'], 1, 200);
                    }elseif ($type == 2) {
                        integralInc($param['u_id'], 1);
                    }else{
                        integralInc($param['u_id'], 1, 300);
                    }
                    // 提交事务
                    Db::commit();
                    return json(['code' => 200, 'msg' => '审核完成通过。']);
                }elseif($param['state'] == 3) {
                    // 驳回
                    $res = Db::name('member')->save(['id' => $param['u_id'], 'state' => 3, 'time' => time()]); // 账号状态改为删除
                    // 修改审核状态
                    $result = Db::name('check')->save(['id' => $param['id'], 'desc' => $param['desc'], 'state' => $param['state'], 'time' => time()]);
                    // 获取用户数据
                    $u_id = Db::name('member')->field('id,name,phone,level,review_phone,invite_phone,quota')->find($param['u_id']);
                    // 提交事务
                    Db::commit();
                    return json(['code' => 200, 'msg' => '审核完成驳回。']);
                }
            }else{
                // 名额审核
                if ($param['state'] == 2) {
                    // 通过
                    // 获取审核记录
                    $data = Db::name('check')->find($param['id']);
                    // 查询申请人
                    $u_id = Db::name('member')->find($param['u_id']);
                    // 查询审核人
                    $c_id = Db::name('member')->find($param['c_id']);
                    // 审核通过减少一个库存
                    if ($data['type'] === 2) {
                        if ($u_id['type'] === 1) return json(['code' => 204, 'msg' => '申请人未开通365会员。']);
                        if ($c_id['quota'] < $data['number']) return json(['code' => 204, 'msg' => '365会员余额不足，先去购买一点吧。']);
                        $type = 1;
                        Db::name('member')
                            ->where('id', $data['u_id'])
                            ->inc('quota', $data['number'])
                            ->update();
                        Db::name('member')
                            ->where('id', $data['c_id'])
                            ->dec('quota', $data['number'])
                            ->update();
                    }else if ($data['type'] === 4) {
                        if ($u_id['type'] === 2) return json(['code' => 204, 'msg' => '申请人未开通了了派。']);
                        if ($c_id['lele_balance'] < $data['number']) return json(['code' => 204, 'msg' => '了了派余额不足，先去购买一点吧。']);
                        $type = 2;
                        Db::name('member')
                            ->where('id', $param['u_id'])
                            ->inc('lele_balance', $data['number'])
                            ->update();
                        Db::name('member')
                            ->where('id', $data['c_id'])
                            ->dec('lele_balance', $data['number'])
                            ->update();
                    }else{
                        return json(['code' => 201, 'msg' => '订单异常请联系工作人员。']);
                    }
                    // 修改审核状态
                    Db::name('check')->save(['id' => $param['id'], 'state' => $param['state'], 'time' => time()]);
                    $desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'用户'. $u_id['name'] .'申请购买名额'. $data['number'] .'结果为 “ 通过 ”。';
                    BackstageLog($userInfo['id'], '购买名额', $desc);
                    // 提交事务
                    Db::commit();
                    return json(['code' => 200, 'msg' => '审核完成通过。']);
                }elseif($param['state'] == 3) {
                    // 驳回
                    // 修改审核状态
                    Db::name('check')->save(['id' => $param['id'], 'desc' => $param['desc'], 'state' => $param['state'], 'time' => time()]);
                    // 获取用户数据
                    $u_id = Db::name('member')->field('id,name,phone,level,review_phone,invite_phone,quota')->find($param['u_id']);
                    $desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'用户'. $u_id['name'] .'申请购买名额结果为 “ 驳回 ”。';
                    BackstageLog($userInfo['id'], '购买名额', $desc);
                    // 提交事务
                    Db::commit();
                    return json(['code' => 200, 'msg' => '审核完成驳回。']);
                }
            }
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 提交审核
	 * @Author: Linxy
	 * @Time: 2023/10/12 11:29
	 */
	public function save()
	{
        $param = input();
        // 0代表本地不需要发送短信验证码
        if (env('ALY_SMS.ALY_STATE', '') !== '0') {
            // 查看验证码是否正确
            if ($param['code'] !== Cache::get($param['phone'])) return json(['code' => 201, 'msg' => '验证码不正确。']);
        }
        unset($param['code']);
        // 启动事务
        Db::startTrans();
        try {
            // 查询手机号是否注册过
            $data = Db::name('member')->where('phone', $param['phone'])->find();
            if ($data) {
                if ($data['state'] === 1) return json(['code' => 203, 'msg' => '该手机号已经注册过了请确认手机号是否正确。']);
                if ($data['state'] === 2) return json(['code' => 203, 'msg' => '该手机号已经注册过冻结了，快去联系客服解冻吧。']);
                if ($data['state'] === 3) return json(['code' => 203, 'msg' => '该手机号已经删除了，快联系客服重新启用吧。']);
                if ($data['state'] === 4) return json(['code' => 203, 'msg' => '该手机号已经申请了，快去通过吧。']);
            }

            // 查询推荐人
            $quota = Db::name('member')->where('phone', $param['invite_phone'])->find();
            if (!$quota)  return json(['code' => 201, 'msg' => '推荐人未注册。']);

            if ($param['state'] == 1) {
                // state等于1 代表直接通过直接扣除库存
                if ($quota['quota'] < 1) return json(['code' => 204, 'msg' => '推荐人余额不足。']);
                // 减少库存
                $result = Db::name('member')->where('phone', $param['invite_phone'])->dec('quota')->update();
            }

            // 注册时间
            $param['time'] = time();

            // 密码加密
            $param['pwd'] = encrypt_password($param['pwd']);

            // 获取审核人
            $level = false;

            // 等于3 代表提交的是中心账号
            if ($param['level'] === 3) $level = true;
            $review = review($param['invite_phone'], $level);
            $param['review_phone'] = $review['phone'];

            // 生成编号
            $id = Db::name('member')->max('number')+1;
            $param['number'] = substr(sprintf('%07s', $id), -6);

            // 添加到用户表
            $id = Db::name('member')->insertGetId($param);

            // 审核记录增加
            $cid = Db::name('check')->insertGetId([
                'u_id' => $id,
                'c_id' => $data['id'],
                'state' => $param['state'] === 1 ? 2 : 1,
                'type' => 1,
                'time' => time()
            ]);

            // 后台消息提醒
            $desc = '用户'. $param['name'] .'提交了注册信息，需要审核快去看看吧！';
            remind(['u_id' => $id, 'c_id' => $data['id'], 'r_id' => $cid, 'desc' => $desc, 'type' => 1]);

            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '申请注册会员成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 审核/发放名额列表
	 * @Author: Linxy
	 * @Time: 2023/10/12 10:39
	 */
    public function list()
	{
		$param = input();
		$where = [];
		// 搜索
		if (!empty($param['search'])) $where[] = ['b.name|b.phone', 'like', '%'.$param['search'].'%'];
		// 级别筛选
		if (!empty($param['state'])) $where[] = ['a.state', '=', $param['state']];
		$userInfo = getUserInfo();
		// 查询当前登录的用户需要审核的信息
		$where[] = ['a.c_id', '=', $userInfo['id']];
		$data = Db::name('check')
			->alias('a')
			->join('member b', 'a.u_id = b.id')
			->join('member c', 'a.c_id = c.id')
			->where($where)
			->field('a.id, a.u_id, a.c_id , b.name as user_name, c.name, a.number, b.vip_start_time, b.vip_end_time, a.state, a.type, a.time')
			->page((int)$param['page'], (int)$param['limit'])
			->order('a.time', 'desc')
			->select()->toArray();
		$count = Db::name('check')
			->alias('a')
			->join('member b', 'a.u_id = b.id')
			->join('member c', 'a.c_id = c.id')
			->where($where)
			->field('a.id, a.u_id, a.c_id , b.name as user_name, c.name, a.number, b.vip_start_time, b.vip_end_time, a.state, a.type, a.time')
			->order('a.time', 'desc')
			->count();
		return json(['code' => 200, 'msg' => '获取审核列表成功。', 'data' => $data, 'count' => $count]);
	}
}
