<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;

class Role
{
	/**
	 * Created by PhpStorm.
	 * @purpose 删除角色权限
	 * @Author: Linxy
	 * @Time: 2023/10/16 14:57
	 */
	public function delete()
	{
		$param = input();
		# 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			$data = Db::name('role')->find($param['id']);
			$result = Db::name('role')->delete($param['id']);
			$result = Db::name('menu_role')->delete($param['id']);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'删除了角色权限：'. $data['name'] .'。';
			BackstageLog($userInfo['id'], '角色权限', $desc);
			# 启动事务
			Db::commit();
			return json(['code' => 200, 'msg' => '角色信息已删除。']);
		} catch (\Throwable $e){
			# 回滚事务
			Db::rollback();
			# 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改角色权限
	 * @Author: Linxy
	 * @Time: 2023/10/16 14:45
	 */
	public function update()
	{
		$param = input();
		# 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			Db::name('role')->save($param);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改了角色权限ID：'. $param['id'].'。';
			BackstageLog($userInfo['id'], '角色权限', $desc);
			# 启动事务
			Db::commit();
			return json(['code' => 200, 'msg' => '角色信息修改成功。']);
		} catch (\Throwable $e){
			# 回滚事务
			Db::rollback();
			# 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改角色信息
	 * @Author: Linxy
	 * @Time: 2023/10/16 14:31
	 */
	public function edit()
	{
		$param = input();
		# 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
            // 修改角色信息
			Db::name('role')->save([
                'id' => $param['id'],
                'name' => $param['name'],
                'code' => $param['code'],
                'sort' => $param['sort'],
                'state' => $param['state'],
                'desc' => $param['desc']
            ]);
            // 修改角色权限
            Db::name('menu_role')->save(['id' => $param['id'], 'p_id' => $param['p_id'], 'time' => time()]);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改了角色信息ID：'. $param['id'] .'。';
			BackstageLog($userInfo['id'], '角色权限', $desc);
			# 启动事务
			Db::commit();
			return json(['code' => 200, 'msg' => '角色信息修改成功。']);
		} catch (\Throwable $e){
			# 回滚事务
			Db::rollback();
			# 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 角色列表
	 * @Author: Linxy
	 * @Time: 2023/10/16 10:41
	 */
	public function list()
	{
		$param = input();
		$data = Db::name('role')
			->alias('a')
			->leftJoin('menu_role b', 'a.id = b.id')
			->field('a.id, a.name, a.code, a.sort, a.state, a.desc, IFNULL(b.p_id, "") as p_id, a.time')
			->order('sort', 'asc')
			->page((int)$param['page'], (int)$param['limit'])
			->select()
			->toArray();
		$count = Db::name('role')
			->alias('a')
			->leftJoin('menu_role b', 'a.id = b.id')
			->field('a.id, a.name, a.code, a.sort, a.state, a.desc, IFNULL(b.p_id, "") as p_id, a.time')
			->order('sort', 'asc')
			->count();
		return json(['code' => 200, 'msg' => '查询角色列表成功。', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 添加角色
	 * @Author: Linxy
	 * @Time: 2023/10/16 9:45
	 */
    public function save()
	{
		$param = input();
		# 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			# 添加角色后获取ID
			$id = Db::name('role')->insertGetId([
				'name' => $param['name'],
				'code' => $param['code'],
				'sort' => $param['sort'],
				'desc' => $param['desc'],
				'time' => time()
			]);
			$result = Db::name('menu_role')->insert([
				'id' => $id,
				'p_id' => $param['p_id'],
				'time' => time()
			]);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'创建了角色身份'. $param['name'] .'。';
			BackstageLog($userInfo['id'], '角色权限', $desc);
			# 启动事务
			Db::commit();
			return json(['code' => 200, 'msg' => '角色创建成功。']);
		} catch (\Throwable $e){
			# 回滚事务
			Db::rollback();
			# 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
}
