<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\Request;

class Blog
{

    /**
     *
     * @Description: 测试
     * @Author: Linxy
     * @Date: 2023/12/4/004
     * @Time: 16:00
     *
     */
    public function index()
    {
        $param = input();
        p($param);
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function onConnect($connection)
    {
		$connection->send('hello~');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
