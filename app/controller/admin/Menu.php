<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;
use think\Request;

class Menu
{
	
	/**
	 * Created by PhpStorm.
	 * @purpose 删除菜单
	 * @Author: Linxy
	 * @Time: 2023/10/13 9:51
	 */
	public function delete()
	{
		$param = input();
		$result = Db::name('menu')->whereIn('id', $param['id'])->delete();
		return json(['code' => 200, 'msg' => '删除菜单成功。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 查询菜单列表
	 * @Author: Linxy
	 * @Time: 2023/10/10 17:05
	 */
    public function index()
    {
        $data = Db::name('menu')
			->alias('a')
			->order('sort')
			->select()
			->toArray();
		return json(['code' => 200, 'msg' => '查询菜单成功', 'data' => tree($data)]);
    }
	
	/**
	 * Created by PhpStorm.
	 * @purpose 创建菜单
	 * @Author: Linxy
	 * @Time: 2023/10/10 15:40
	 */
    public function save(Request $request)
    {
		$param = input();
		// 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			$param['pid'] = $param['pid'] ?? 0;
			$param['time'] = time();
			// 添加菜单获取到菜单ID
			$id = Db::name('menu')->insertGetId($param);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'创建了'. $param['name'] .'菜单。';
			BackstageLog($userInfo['id'], '创建菜单', $desc);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '菜单创建成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
    }
	
	/**
	 * Created by PhpStorm.
	 * @purpose 查询可用菜单
	 * @Author: Linxy
	 * @Time: 2023/10/10 15:41
	 */
    public function read()
    {
		$userInfo = getUserInfo();
		$id = Db::name('menu_role')->where('id', $userInfo['id'])->value('p_id');
		$data = Db::name('menu')->where('id','in',$id)->where('state', 1)->order('sort')->select()->toArray();
		return json(['code' => 200, 'msg' => '菜单查询成功。', 'data' => tree($data)]);
    }
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改菜单
	 * @Author: Linxy
	 * @Time: 2023/10/10 17:06
	 */
    public function update()
    {
        $param = input();
		// 启动事务
		Db::startTrans();
		try {
			$userInfo = getUserInfo();
			$param['time'] = time();
			$data = Db::name('menu')->find($param['id']);
			$result = Db::name('menu')->save($param);
			$desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改了菜单'. $data['name'] .'ID为'.$param['id'];
			BackstageLog($userInfo['id'], '修改了菜单', $desc);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '菜单修改成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
    }
}
