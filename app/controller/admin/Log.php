<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;

class Log
{
	/**
	 * Created by PhpStorm.
	 * @purpose 查看后台日志
	 * @Author: Linxy
	 * @Time: 2023/10/18 15:44
	 */
    public function list()
	{
		$param = input();
		$where = [];
		// 按照类型搜索
		if (!empty($param['search'])) $where[] = ['type', 'like', '%'.$param['search'].'%'];
		// 按照操作人搜索
		if (!empty($param['id'])) $where[] = ['a.u_id', '=', $param['id']];
		$data = Db::name('admin_log')
			->alias('a')
			->leftJoin('member b', 'a.u_id = b.id')
			->field('a.id, a.u_id, b.name, a.type, a.content, a.time, a.ip')
			->where($where)
			->page((int)$param['page'], (int)$param['limit'])
			->order('a.time', 'desc')
			->select()
			->toArray();
		$count = Db::name('admin_log')
			->alias('a')
			->leftJoin('member b', 'a.u_id = b.id')
			->field('a.id, a.u_id, b.name, a.type, a.content, a.time, a.ip')
			->where($where)
			->count();
		return json(['code' => 200, 'msg' => '查询日志记录成功。', 'data' => $data, 'count' => $count]);
	}
}
