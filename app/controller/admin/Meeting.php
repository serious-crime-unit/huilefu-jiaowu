<?php
declare (strict_types = 1);

namespace app\controller\admin;

//use GatewayWorker\Gateway;
use Overtrue\Pinyin\Pinyin;
use think\facade\Db;

class Meeting
{
    /**
     * @Description: 开通讲师权限
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 11:22
     */
    public function openTeacher()
    {
        $param = input();
        Db::name('member')->save([
            'id' => $param['id'],
            'is_teacher' => $param['is_teacher']
        ]);
        return json(['code' => 200, 'msg' => '成功。']);
    }

    /**
     * @Description: 删除录入讲师
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 11:11
     */
    public function teacherDel()
    {
        $param = input();
        Db::name('meeting_city')->delete($param['id']); // 删除地址讲师
        Db::name('member_meeting')->where('m_id', $param['id'])->delete(); // 删除报名用户
        return json(['code' => 200, 'msg' => '删除成功。']);
    }

    /**
     * @Description: 课时讲师列表
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 10:40
     */
    public function teacherList()
    {
        $param = input();
        $where = [];
        if (!empty($param['id'])) $where[] = ['t1.p_id', '=', $param['id']];
        $data = Db::name('meeting_city')
            ->alias('t1')
            ->leftJoin('meeting t2', 't1.p_id = t2.id')
            ->where($where)
            ->field('t1.id, t2.name, t1.p_id, t1.city, t1.userId, t1.userName, t1.start_time, t1.end_time')
            ->page((int)$param['page'], (int)$param['limit'])
            ->select()->toArray();
        $count = Db::name('meeting_city')
            ->alias('t1')
            ->leftJoin('meeting t2', 't1.p_id = t2.id')
            ->where($where)
            ->field('t1.id, t2.name, t1.p_id, t1.city, t1.userId, t1.userName, t1.start_time, t1.end_time')
            ->count();
        return json(['code' => 200, 'msg' => '查询讲师成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 课时录入讲师
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 9:52
     */
    public function saveLecturer()
    {
        $param = input();
        $data = Db::name('member')->find($param['userId']);
        $name = $param['userName'];
        if (empty($param['userName'])) $name = $data['name'];
        $phone = $param['userPhone'];
        if (empty($param['userPhone'])) $phone = $data['phone'];
        // 启动事务
        Db::startTrans();
        try {
            Db::name('meeting_city')->save([
                'p_id' => $param['p_id'],
                'city' => $param['city'],
                'address' => $param['address'],
                'location' => $param['location'],
                'userId' => $param['userId'],
                'userName' => $name,
                'userPhone' => $phone,
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
                'time' => time()
            ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '课时讲师录入成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查询平台中的讲师
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 9:59
     */
    public function firstTeacher()
    {
        $data = Db::name('member')
            ->where('is_teacher', 1)
            ->field('id, name, phone')
            ->select()->toArray();
        return json(['code' => 200, 'msg' => '查询讲师成功。', 'data' => $data]);
    }

    /**
     * @Description: 删除课时
     * @Author: 林怼怼
     * @Date: 2024/10/26
     * @Time: 12:07
     */
    public function stageDel()
    {
        $param = input();
        $res = Db::name('meeting')->delete($param['id']); // 删除课时
        $ids = Db::name('meeting_city')->whereIn('p_id', $param['id'])->column('id'); // 查询地址讲师id
        Db::name('meeting_city')->whereIn('p_id', $param['id'])->delete(); // 删除地址讲师
        Db::name('meeting_user')->whereIn('id', $param['id'])->delete(); // 删除订阅用户
        Db::name('member_meeting')->whereIn('m_id', $ids)->delete(); // 删除报名记录
        return json(['code' => 200, 'msg' => '删除课时成功。']);
    }

    /**
     * @Description: 课时列表
     * @Author: 林怼怼
     * @Date: 2024/10/26
     * @Time: 11:23
     */
    public function stageList()
    {
        $param = input();
        $where = [];
        if (!empty($param['id'])) {
            $where[] = ['m1.p_id', '=', $param['id']];
        }
        $data = Db::name('meeting')
            ->alias('m1')
            ->leftJoin('meeting m2', 'm1.p_id = m2.id')
            ->whereNotIn('m1.p_id', 0)
            ->where($where)
            ->field('m1.id, m1.p_id, m2.name as title, m1.userName, m1.userPhone, m1.name')
            ->page((int)$param['page'], (int)$param['limit'])
            ->select()->toArray();
        $count = Db::name('meeting')
            ->alias('m1')
            ->leftJoin('meeting m2', 'm1.p_id = m2.id')
            ->whereNotIn('m1.p_id', 0)
            ->where($where)
            ->count();
        return json(['code' => 200, 'msg' => '查询课时成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 增加课时
     * @Author: 林怼怼
     * @Date: 2024/10/26
     * @Time: 11:02
     */
    public function stage()
    {
        $param = input();
        $userInfo = getUserInfo();
        $data = Db::name('meeting')->where('id', $param['p_id'])->find();
        $count = Db::name('meeting')->where(['p_id'=>$param['p_id']])->count();
        if ($count > $data['num']) return json(['code' => 201, 'msg' => '阶段已满。']);
        $res = Db::name('meeting')->save([
            'u_id' => $userInfo['id'],
            'p_id' => $param['p_id'],
            'userName' => $param['userName'],
            'userPhone' => $param['userPhone'],
            'name' => $param['name'],
            'img' => $param['img'],
            'content' => $param['content'],
            'desc' => $param['desc'],
            'state' => 2,
            'grade' => 0,
            'sort' => $param['sort'],
            'time' => time()
        ]);
        return json(['code' => 200, 'msg' => '增加课时成功。']);
    }

    /**
     * @Description: 修改活动状态
     * @Author: 林怼怼
     * @Date: 2023/12/22/022
     * @Time: 10:16
     */
    public function update()
    {
        $id = input('id');
        $state = input('state', 1);
        $desc = input('desc', '');
        // 启动事务
        Db::startTrans();
        try {
            Db::name('meeting')->save(['id' => $id, 'state' => $state, 'error_desc' => $desc]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '修改活动状态成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 查询活动详情
	 * @Author: Linxy
	 * @Time: 2023/10/31 16:02
	 */
	public function read()
	{
		$param = input();
		$data = Db::name('meeting')
			->alias('a')
			->where(['a.id' => $param['id']])
			->find();
		return json(['code' => 200, 'msg' => '查询活动成功。', 'data' => $data]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 违规活动删除
	 * @Author: Linxy
	 * @Time: 2023/10/18 17:36
	 */
	public function delete()
	{
		// 启动事务
		Db::startTrans();
		try {
            $ids = input('id');
            $id = Db::name('meeting')->whereIn('p_id', $ids)->column('id'); // 课时id
            $idd = Db::name('meeting_city')->whereIn('p_id', $id)->column('id'); // 地址讲师id
            Db::name('meeting')->whereIn('id', $ids)->delete(); // 删除活动
            Db::name('meeting')->whereIn('p_id', $ids)->delete(); // 删除课时
            Db::name('meeting_city')->whereIn('p_id', $id)->delete(); // 删除地址讲师
			Db::name('meeting_user')->whereIn('id', $ids)->delete(); // 删除通知的用户
			Db::name('member_meeting')->whereIn('m_id', $idd)->delete(); // 删除预约的用户
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '删除成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改活动内容
	 * @Author: Linxy
	 * @Time: 2023/10/18 17:15
	 */
	public function edit()
	{
        $param = input();
        $userInfo = getUserInfo();
        // 启动事务
        Db::startTrans();
        try {
            $id = $param['id'];
            $res = Db::name('meeting')->update([
                'id' => $param['id'],
                'p_id' => 0,
                'num' => $param['num'],
                'userName' => $param['userName'],
                'userPhone' => $param['userPhone'],
                'name' => $param['name'],
                'img' => $param['img'],
                'number' => $param['number'],
                'content' => $param['content'],
                'desc' => $param['desc'],
                'grade' => $param['grade'],
                'sort' => $param['sort'],
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
                'time' => time()
            ]);
            // 删除这个活动的全部通知用户
            Db::name('meeting_user')->where(['id' => $param['id']])->delete();
            $message = [
                'id' => $id,
                'userName' => $userInfo['name'],
                'avatar' => $userInfo['avatar'],
                'title' => $param['name'],
                'img' => $param['img'],
                'desc' => $param['desc'],
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
            ];
            // 等于空通知全部
            if (empty($param['ids'])) {
                $ids = getChildUserPhoneS($userInfo['id']);
                $param['ids'] = implode(',', $ids);
            }

            $ids = explode(',', $userInfo['id'].','.$param['ids']);
            $count = count($ids);
            $arr = [];
            for ($i = 0; $i < $count; $i++)
            {
                $arr[] = ['id' => $id, 'u_id' => $ids[$i], 'message_time' => $param['message_time'], 'time' => time()];
            }
            $result = Db::name('meeting_user')->insertAll($arr);
            $desc = $userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'发布了活动'. $param['name'] .'。';
            BackstageLog($userInfo['id'], '线下活动', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '更新活动成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 活动列表
	 * @Author: Linxy
	 * @Time: 2023/10/18 16:51
	 */
	public function list()
	{
		$param = input();
		$where = [];
        $userInfo = getUserInfo();
        if ($userInfo['id'] !== 1) {
            $where[] = ['a.u_id', '=', $userInfo['id']];
        }
        $where[] = ['a.p_id', '=', 0];
		// 搜索活动名字或者发起人名字
		if (!empty($param['search'])) $where[] = ['a.name|b.name', 'like', '%'.$param['search'].'%'];
		// 状态筛选
		if (!empty($param['state'])) $where[] = ['a.state', '=', $param['state']];
		$data = Db::name('meeting')
			->alias('a')
			->leftJoin('member b', 'a.u_id = b.id')
			->field('a.id, a.userName, a.userPhone, a.name, a.img, a.state, a.start_time, a.end_time')
			->where($where)
			->page((int)$param['page'], (int)$param['limit'])
			->order('a.id', 'desc')
			->select()
			->toArray();
		$count = Db::name('meeting')
			->alias('a')
			->leftJoin('member b', 'a.u_id = b.id')
			->field('a.id, a.userName, a.userPhone, a.name, a.img, a.state, a.start_time, a.end_time')
			->where($where)
			->order('a.id', 'desc')
			->count();
		return json(['code' => 200, 'msg' => '查询活动列表成功。', 'data' => $data, 'count' => $count]);
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 发布活动
	 * @Author: Linxy
	 * @Time: 2023/10/18 11:38
	 */
    public function save()
	{
        $param = input();
        $userInfo = getUserInfo();
        $pinyin = new Pinyin();
        // 获取中文字符的拼音并转为大写
        // 启动事务
		Db::startTrans();
		try {
			$id = Db::name('meeting')->insertGetId([
                'u_id' => $userInfo['id'],
                'p_id' => 0,
                'num' => $param['num'],
                'userName' => $param['userName'],
                'userPhone' => $param['userPhone'],
                'name' => $param['name'],
                'pinyin' => str_replace('-', ' ', strtoupper($pinyin->permalink($param['name']))),
                'video' => $param['video'],
                'img' => $param['img'],
                'number' => $param['number'],
                'text' => $param['text'],
                'content' => $param['content'],
                'desc' => $param['desc'],
                'grade' => $param['grade'],
                'sort' => $param['sort'],
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
                'time' => time()
            ]);
			$message = [
				'id' => $id,
				'userName' => $userInfo['name'],
				'avatar' => $userInfo['avatar'],
				'title' => $param['name'],
				'img' => $param['img'],
				'desc' => $param['desc'],
				'start_time' => $param['start_time'],
				'end_time' => $param['end_time'],
			];
            // 等于空通知全部
            if (empty($param['ids'])) {
                $ids = getChildUserPhoneS($userInfo['id']);
                $param['ids'] = implode(',', $ids);
            }
            $ids = explode(',', $userInfo['id'].','.$param['ids']);
            $count = count($ids);
            $arr = [];
            for ($i = 0; $i < $count; $i++)
            {
                $arr[] = ['id' => $id, 'u_id' => $ids[$i], 'message_time' => $param['message_time'], 'time' => time()];
            }
            $result = Db::name('meeting_user')->insertAll($arr);
			$desc = $userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'发布了活动'. $param['name'] .'。';
			BackstageLog($userInfo['id'], '线下活动', $desc);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '发布活动成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
}
