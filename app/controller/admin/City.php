<?php
declare (strict_types = 1);

namespace app\controller\admin;

use think\facade\Db;

class City
{
	
	/**
	 * Created by PhpStorm.
	 * @purpose 地址列表
	 * @Author: Linxy
	 * @Time: 2023/10/11 11:21
	 */
    public function list()
	{
        $id = input('id') === '' ? 100000 : input('id', 0);
        $data = Db::name('city')
            ->where('parentId', $id)
            ->order('id', 'asc')
            ->field('id, cityName as name')
            ->select()
            ->toArray();
        return json(['code' => 200, 'msg' => '地址列表。', 'data' => $data]);
	}
}
