<?php
declare (strict_types = 1);

namespace app\controller\exam;

use PhpOffice\PhpSpreadsheet\IOFactory;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Db;
use think\facade\Filesystem;
use think\facade\Log;
use think\Request;


class Exam
{

    /**
     * @Description: 营业额列表
     * @Author: 林怼怼
     * @Date: 2025/1/23
     * @Time: 12:57
     */
    public function getDateList()
    {
        try {
            // 今天的开始和结束时间
            $todayStart = strtotime(date('Y-m-d 00:00:00'));
            $todayEnd = strtotime(date('Y-m-d 23:59:59'));

            // 昨天的开始和结束时间
            $yesterdayStart = strtotime('-1 day', $todayStart);
            $yesterdayEnd = strtotime('-1 day', $todayEnd);

            // 上周的开始和结束时间
            $lastWeekStart = strtotime('-1 week', $todayStart);
            $lastWeekEnd = strtotime('-1 week', $todayEnd);

            // 上月的开始和结束时间
            $lastMonthStart = strtotime('first day of last month');
            $lastMonthEnd = strtotime('last day of last month 23:59:59');

            // 计算今日数据
            $num1 = Db::name('orders')
                ->where('state', 1)
                ->whereTime('pay_time', 'today')
                ->sum('amount') ?: 0.00;

            // 计算昨日数据
            $yesterdayNum1 = Db::name('orders')
                ->where('state', 1)
                ->whereTime('pay_time', 'between', [$yesterdayStart, $yesterdayEnd])
                ->sum('amount') ?: 0.00;

            // 计算总金额
            $num2 = Db::name('orders')
                ->where('state', 1)
                ->sum('amount') ?: 0.00;

            // 计算上周总金额
            $lastWeekNum2 = Db::name('orders')
                ->where('state', 1)
                ->whereTime('pay_time', 'between', [$lastWeekStart, $lastWeekEnd])
                ->sum('amount') ?: 0.00;

            // 计算总人数
            $num3 = Db::name('user')->count() ?: 0;

            // 计算上月总人数
            $lastMonthNum3 = Db::name('user')
                ->whereTime('time', 'between', [$lastMonthStart, $lastMonthEnd])
                ->count() ?: 0;

            // 计算今日报名人数
            $num4 = Db::name('exam_log')
                ->whereTime('time', 'today')
                ->group('phone')
                ->count() ?: 0;

            // 计算昨日报名人数
            $yesterdayNum4 = Db::name('exam_log')
                ->whereTime('time', 'between', [$yesterdayStart, $yesterdayEnd])
                ->group('phone')
                ->count() ?: 0;

            // 计算增长率
            $rate1 = $this->calculateGrowthRate($num1, $yesterdayNum1);
            $rate2 = $this->calculateGrowthRate($num2, $lastWeekNum2);
            $rate3 = $this->calculateGrowthRate($num3, $lastMonthNum3);
            $rate4 = $this->calculateGrowthRate($num4, $yesterdayNum4);

            return json([
                'code' => 200,
                'msg' => '操作成功',
                'data' => [
                    'num1' => [
                        'value' => number_format($num1, 2),
                        'yesterday_value' => number_format($yesterdayNum1, 2),
                        'rate' => $rate1,
                        'trend' => $rate1 >= 0 ? 'up' : 'down'
                    ],
                    'num2' => [
                        'value' => number_format($num2, 2),
                        'last_week_value' => number_format($lastWeekNum2, 2),
                        'rate' => $rate2,
                        'trend' => $rate2 >= 0 ? 'up' : 'down'
                    ],
                    'num3' => [
                        'value' => $num3,
                        'last_month_value' => $lastMonthNum3,
                        'rate' => $rate3,
                        'trend' => $rate3 >= 0 ? 'up' : 'down'
                    ],
                    'num4' => [
                        'value' => $num4,
                        'yesterday_value' => $yesterdayNum4,
                        'rate' => $rate4,
                        'trend' => $rate4 >= 0 ? 'up' : 'down'
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            Log::error('获取数据统计失败：' . $e->getMessage());
            return json([
                'code' => 201,
                'msg' => '获取数据失败'
            ]);
        }
    }

    /**
     * 计算增长率
     * @param float|int $current 当前值
     * @param float|int $previous 前期值
     * @return string 增长率（百分比）
     */
    private function calculateGrowthRate($current, $previous)
    {
        if ($previous == 0) {
            return $current > 0 ? '100%' : '0%';
        }

        $rate = (($current - $previous) / $previous) * 100;
        return sprintf('%+.2f%%', $rate); // 带正负号的百分比
    }

    /**
     * 格式化数字
     * @param float|int $number
     * @return string
     */
    private function formatNumber($number)
    {
        if ($number >= 10000) {
            return sprintf('%.2fw', $number / 10000);
        }
        return number_format($number, 2);
    }

    /**
     * 查询微信订单
     * @param string $transactionId 微信订单号
     * @return array 查询结果
     */
    public function queryOrder()
    {
        $param = input();
        // 构造请求 URL
        $url = "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{$param['transactionId']}?mchid=". Config::get('wechat.mch_id') ."";
        // 发起请求并返回结果
        $response = $this->getRequest($url);

        if (isset($response['error'])) {
            return json(['code' => 201, 'msg' => '查询失败。', 'data' => $response['error']]);
        }

        // 成功后修改订单
        $this->handlePayResult($response);
        return json(['code' => 200, 'msg' => '操作成功。', 'data' => $response]);


    }

    /**
     * 发起 GET 请求
     * @param string $url 请求 URL
     * @return array|mixed 响应数据
     */
    private function getRequest($url)
    {
        // 获取时间戳和随机字符串
        $timestamp = time();
        $nonceStr = uniqid();

        // 构造签名字符串
        $signatureString = "GET\n"
            . parse_url($url, PHP_URL_PATH) . "?" . parse_url($url, PHP_URL_QUERY) . "\n"
            . $timestamp . "\n"
            . $nonceStr . "\n\n";

        // 使用商户私钥签名
        $privateKey = file_get_contents(Config::get('wechat.certificate'));
        openssl_sign($signatureString, $rawSign, $privateKey, 'sha256WithRSAEncryption');
        $signature = base64_encode($rawSign);

        // 构造 Authorization 头
        $authHeader = sprintf(
            'WECHATPAY2-SHA256-RSA2048 mchid="%s",nonce_str="%s",signature="%s",timestamp="%s",serial_no="%s"',
            Config::get('wechat.mch_id'),
            $nonceStr,
            $signature,
            $timestamp,
            Config::get('wechat.serial_no')
        );

        // 使用 CURL 发起 GET 请求
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . $authHeader,
                'Accept: application/json',                // 添加 Accept 头
                'User-Agent: WeChatPay-PHP/1.0.0',         // 添加 User-Agent 头
            ],
            CURLOPT_SSL_VERIFYPEER => false, // 不验证证书
            CURLOPT_SSL_VERIFYHOST => false, // 不验证主机名
        ]);

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        if ($error) {
            return ['error' => $error];
        }

        return json_decode($response, true);
    }

    /**
     * @Description: 生成报告列表
     * @Author: 林怼怼
     * @Date: 2025/1/22
     * @Time: 14:16
     */
    public function setReportList()
    {
        $param = input();
        $count = Db::name('scoring_analysis')->where('phone', $param['phone'])->count();
        if ($count != 0) {
            $num = 0;
            $desc = '';
            $list1 = $list2 = $list3 = $list4 = $list5 = 0;
            // 第三个data
            $data = Db::name('scoring_analysis')
                ->alias('t1')
                ->join('jw_exam_topic t2', 't1.p_id = t2.id')
                ->where(['t1.phone' => $param['phone']])
                ->field('t1.*, t2.title')
                ->select();

            foreach ($data as &$datum) {
                // 确保 range 是数值类型
                $range = isset($datum['range']) ? intval($datum['range']) : 0;

                // 累加总分
                $num += $range;

                // 处理描述
                if (empty($datum['c_id']) && !empty($datum['result'])) {
                    $desc .= $datum['result'].';';
                }

                // 处理不同类型的得分
                switch ($datum['p_id']) {
                    case 1:
                        $list1 = 3;
                        break;
                    case 2:
                        $list2 = $range;
                        break;
                    case 3:
                        $list3 = $range;
                        break;
                    case 4:
                        $list4 += $range; // 注意这里是累加
                        break;
                    case 5:
                        $list5 = $range;
                        break;
                }
                // 处理 c_id 显示文本
                $cidMap = [
                    1 => '1-5题',
                    2 => '6-10题',
                    3 => '11-15题',
                    4 => '16-20题'
                ];
                if (isset($datum['c_id']) && isset($cidMap[$datum['c_id']])) {
                    $datum['c_id'] = $cidMap[$datum['c_id']];
                }
            }
            // 第二个data
            $data2 = Db::name('exam_topic')
                ->alias('t')
                ->leftJoin('jw_scoring_analysis s', 't.id = s.p_id')
                ->where(['s.phone' => $param['phone']])
                ->where('t.id', '<>', 1)
                ->field([
                    't.id',
                    't.title',
                    't.score as scoreMax',
                    'SUM(s.`range`) as score',  // 使用反引号包裹 range
                    'MAX(s.result) as result'    // 对 result 使用聚合函数
                ])
                ->group('t.id, t.title')        // 包含所有非聚合字段
                ->select();
            // 第一个data保持不变...
            $data1 = [
                'desc' => "<p>你的评估结果为<span style=\"color: rgb(0, 138, 0);\">". $num ."</span>分，总体结果状态良好，". $desc ."改善不良问题，祝你早日成就更好的自己。</p >",
                'list' => [
                    '专注力水平' => $list1,
                    '学习习惯' => $list2,
                    '学习方法' => $list3,
                    '学习动机' => $list4,
                    '学习心态' => $list5
                ]
            ];
            return json(['code' => 200, 'msg' => '操作成功。', 'data' => ['data1' => $data1, 'data2' => $data2, 'data3' => $data]]);
        }


        $cate = Db::name('exam_topic')->select();
        $result = '';
        // 启动事务
        Db::startTrans();
        try {
            // 统计用户测评
            foreach ($cate as $value) {
                $range = 0;
                $range1 = 0;
                $data = Db::name('exam_log')
                    ->where(['phone' => $param['phone'], 't_id' => $value['id']])
                    ->select();
                foreach ($data as $datum) {
                    if ($datum['t_id'] == 1) {
                        // 第一题
                        if (in_array(1, explode(',', $datum['answer']))) {
                            $result = '专注力差';
                            $range1 = 1;
                        }elseif (in_array(2, explode(',', $datum['answer']))) {
                            $result = '专注力较弱';
                            $range1 = 2;
                        }elseif (in_array(3, explode(',', $datum['answer']))) {
                            $result = '专注力佳';
                            $range1 = 3;
                        }else{
                            throw new \Exception('第一题异常。');
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                    }elseif($datum['t_id'] == 2){
                        // 第二题
                        $id = Db::name('exam_list')->where('id', $datum['p_id'])->value('sort');
                        if ($id % 2 == 0) {
                            if (in_array(4, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }elseif (in_array(5, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }elseif (in_array(6, explode(',', $datum['answer']))) {
                                $range += 2;
                                $range1 = 2;
                            }else{
                                throw new \Exception('第二题异常。');
                            }
                        } else {
                            if (in_array(4, explode(',', $datum['answer']))) {
                                $range += 2;
                                $range1 = 2;
                            }elseif (in_array(5, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }elseif (in_array(6, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }else{
                                throw new \Exception('第二题异常。');
                            }
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                        if ($range >= 32) {
                            $result = '学习习惯非常好';
                        }elseif ($range >= 24 && $range < 32) {
                            $result = '学习习惯较';
                        }elseif ($range >= 16 && $range < 24) {
                            $result = '学习习惯一般';
                        }elseif ($range <= 16) {
                            $result = '学习习惯很差，需要改正';
                        }else{
                            throw new \Exception('第二题异常。');
                        }
                    }elseif ($datum['t_id'] == 3) {
                        // 第三题
                        $id = Db::name('exam_list')->where('id', $datum['p_id'])->value('sort');
                        if ($id % 2 == 0) {
                            if (in_array(7, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }elseif (in_array(8, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }else{
                                throw new \Exception('第三题异常。');
                            }
                        } else {
                            if (in_array(7, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }elseif (in_array(8, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }else{
                                throw new \Exception('第三题异常。');
                            }
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                        if ($range >= 24) {
                            $result = '学习方法优良';
                        }elseif ($range >= 22 && $range < 24) {
                            $result = '学习方法较好';
                        }elseif ($range >= 18 && $range < 22) {
                            $result = '学习方法一般';
                        }elseif ($range <= 18) {
                            $result = '学习方法较差，需要改正';
                        }else{
                            throw new \Exception('第三题异常。');
                        }
                    }elseif ($datum['t_id'] == 4) {
                        // 第四题
                        if (in_array(9, explode(',', $datum['answer']))) {
                            $range += 1;
                            $range1 = 1;
                        }elseif (in_array(10, explode(',', $datum['answer']))) {
                            $range += 0;
                            $range1 = 0;
                        }else{
                            throw new \Exception('第四题异常。');
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                        $result = '暂无评价';
                        if ($datum['p_id'] == 56) {
                            // 第一组
                            if ($range > 3) $result = '学习动机较弱，学习阻力大。';
                            $res = Db::name('scoring_analysis')->save([
                                'phone' => $param['phone'],
                                'p_id' => $value['id'],
                                'c_id' => 1,
                                'range' => $range,
                                'result' => $result,
                                'time' => time()
                            ]);
                            $range = 0;
                        }elseif ($datum['p_id'] == 61) {
                            // 第二组
                            if ($range > 3) $result = '学习动机较强，学习压力大。';
                            $res = Db::name('scoring_analysis')->save([
                                'phone' => $param['phone'],
                                'p_id' => $value['id'],
                                'c_id' => 2,
                                'range' => $range,
                                'result' => $result,
                                'time' => time()
                            ]);
                            $range = 0;
                        }elseif ($datum['p_id'] == 66) {
                            // 第三组
                            if ($range > 3) $result = '学习兴趣有困扰。';
                            $res = Db::name('scoring_analysis')->save([
                                'phone' => $param['phone'],
                                'p_id' => $value['id'],
                                'c_id' => 3,
                                'range' => $range,
                                'result' => $result,
                                'time' => time()
                            ]);
                            $range = 0;
                        }elseif ($datum['p_id'] == 71) {
                            // 第四组
                            if ($range > 3) $result = '学习目标有困扰。';
                            $res = Db::name('scoring_analysis')->save([
                                'phone' => $param['phone'],
                                'p_id' => $value['id'],
                                'c_id' => 4,
                                'range' => $range,
                                'result' => $result,
                                'time' => time()
                            ]);
                            $range = 0;
                        }
                    }elseif ($datum['t_id'] == 5) {
                        // 第五题
                        $id = Db::name('exam_list')->where('id', $datum['p_id'])->value('sort');
                        if ($id % 2 == 0) {
                            if (in_array(11, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }elseif (in_array(12, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }elseif (in_array(13, explode(',', $datum['answer']))) {
                                $range += 2;
                                $range1 = 2;
                            }else{
                                throw new \Exception('第五题异常。');
                            }
                        } else {
                            if (in_array(11, explode(',', $datum['answer']))) {
                                $range += 2;
                                $range1 = 2;
                            }elseif (in_array(12, explode(',', $datum['answer']))) {
                                $range += 1;
                                $range1 = 1;
                            }elseif (in_array(13, explode(',', $datum['answer']))) {
                                $range += 0;
                                $range1 = 0;
                            }else{
                                throw new \Exception('第五题异常。');
                            }
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                        if ($range >= 50) {
                            $result = '进取心非常强';
                        }elseif ($range >= 40 && $range < 50) {
                            $result = '进取心较强';
                        }elseif ($range >= 28 && $range < 40) {
                            $result = '进取心一般';
                        }elseif ($range <= 28) {
                            $result = '你的进取心较差';
                        }else{
                            throw new \Exception('第五题异常。');
                        }
                    }elseif ($datum['t_id'] == 6) {
                        // 第六题
                        $id = Db::name('exam_list')->where('id', $datum['p_id'])->value('sort');
                        if ($id > 2) {
                            $yes1 = 2;
                            $yes2 = 2;
                            if ($id == 19) {
                                // 19题
                                if (in_array(14, explode(',', $datum['answer']))) {
                                    $yes1 = 1;
                                }
                            } elseif ($id == 20) {
                                // 20题
                                if (in_array(14, explode(',', $datum['answer']))) {
                                    $yes2 = 1;
                                }
                            }
                            if ($id == 19 || $id == 20) {
                                if ($yes1 == 1 && $yes2 == 1) {
                                    $result = '视听觉较平衡。';
                                } elseif ($yes1 == 1 && $yes2 == 2) {
                                    $result = '偏视觉型学习者。';
                                } elseif ($yes1 == 2 && $yes2 == 1) {
                                    $result = '偏听觉型学习者。';
                                }
                            }
                            // 第三题以后
                            if ($id % 2 == 0) {
                                if (in_array(14, explode(',', $datum['answer']))) {
                                    $range += 0;
                                    $range1 = 0;
                                } elseif (in_array(15, explode(',', $datum['answer']))) {
                                    $range += 1;
                                    $range1 = 1;
                                } else {
                                    throw new \Exception('第六题异常。');
                                }
                            } else {
                                if (in_array(14, explode(',', $datum['answer']))) {
                                    $range += 1;
                                    $range1 = 1;
                                } elseif (in_array(15, explode(',', $datum['answer']))) {
                                    $range += 0;
                                    $range1 = 0;
                                } else {
                                    throw new \Exception('第六题异常。');
                                }
                            }
                        } else{
                            // 第1题
                            if ($id == 1) {
                                if (in_array(16, explode(',', $datum['answer']))) {
                                    $range += 4;
                                    $range1 = 4;
                                } elseif (in_array(17, explode(',', $datum['answer']))) {
                                    $range += 3;
                                    $range1 = 3;
                                } elseif (in_array(18, explode(',', $datum['answer']))) {
                                    $range += 2;
                                    $range1 = 2;
                                } elseif (in_array(19, explode(',', $datum['answer']))) {
                                    $range += 1;
                                    $range1 = 1;
                                } else {
                                    throw new \Exception('第六题异常。');
                                }
                            } else {
                                if (in_array(20, explode(',', $datum['answer']))) {
                                    $range += 2;
                                    $range1 = 2;
                                } elseif (in_array(21, explode(',', $datum['answer']))) {
                                    $range += 1;
                                    $range1 = 1;
                                } else {
                                    throw new \Exception('第六题异常。');
                                }
                            }
                        }
                        Db::name('exam_log')->save(['id' => $datum['id'], 'range' => $range1]);
                    }
                }
                // 第一、二、三、四、五题完成
                if ($value['id'] != 4) {
                    $res = Db::name('scoring_analysis')->save([
                        'phone' => $param['phone'],
                        'p_id' => $value['id'],
                        'range' => $range,
                        'result' => $result,
                        'time' => time()
                    ]);
                }
            }
            // 提交事务
            Db::commit();
            $num = 0;
            $desc = '';
            $list1 = $list2 = $list3 = $list4 = $list5 = 0;
            // 第三个data
            $data = Db::name('scoring_analysis')
                ->alias('t1')
                ->join('jw_exam_topic t2', 't1.p_id = t2.id')
                ->where(['t1.phone' => $param['phone']])
                ->field('t1.*, t2.title')
                ->select();

            foreach ($data as &$datum) {
                // 确保 range 是数值类型
                $range = isset($datum['range']) ? intval($datum['range']) : 0;

                // 累加总分
                $num += $range;

                // 处理描述
                if (empty($datum['c_id']) && !empty($datum['result'])) {
                    $desc .= $datum['result'].';';
                }

                // 处理不同类型的得分
                switch ($datum['p_id']) {
                    case 1:
                        $list1 = $range;
                        break;
                    case 2:
                        $list2 = $range;
                        break;
                    case 3:
                        $list3 = $range;
                        break;
                    case 4:
                        $list4 += $range; // 注意这里是累加
                        break;
                    case 5:
                        $list5 = $range;
                        break;
                }
                // 处理 c_id 显示文本
                $cidMap = [
                    1 => '1-5题',
                    2 => '6-10题',
                    3 => '11-15题',
                    4 => '16-20题'
                ];
                if (isset($datum['c_id']) && isset($cidMap[$datum['c_id']])) {
                    $datum['c_id'] = $cidMap[$datum['c_id']];
                }
            }

            // 第二个data
            $data2 = Db::name('exam_topic')
                ->alias('t')
                ->leftJoin('jw_scoring_analysis s', 't.id = s.p_id')
                ->where(['s.phone' => $param['phone']])
                ->where('t.id', '<>', 1)
                ->field([
                    't.id',
                    't.title',
                    't.score as scoreMax',
                    'SUM(s.`range`) as score',  // 使用反引号包裹 range
                    'MAX(s.result) as result'    // 对 result 使用聚合函数
                ])
                ->group('t.id, t.title')        // 包含所有非聚合字段
                ->select();

            // 第一个data保持不变...
            $data1 = [
                'desc' => "<p>你的评估结果为<span style=\"color: rgb(0, 138, 0);\">". $num ."</span>分，总体结果状态良好，". $desc ."改善不良问题，祝你早日成就更好的自己。</p >",
                'list' => [
                    '专注力水平' => $list1,
                    '学习习惯' => $list2,
                    '学习方法' => $list3,
                    '学习动机' => $list4,
                    '学习心态' => $list5
                ]
            ];
            return json(['code' => 200, 'msg' => '操作成功。', 'data' => ['data1' => $data1, 'data2' => $data2, 'data3' => $data]]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 获取用户列表
     * @Author: 林怼怼
     * @Date: 2025/1/22
     * @Time: 10:04
     */
    public function getUserList()
    {
        $param = input();
        $where = [];
        if (!empty($param['type'])) $where[] = ['type', '=', $param['type']];
        $where[] = ['phone|childName|parentName', 'like', '%'. $param['search'] .'%'];
        $data = Db::name('user')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('time', 'asc')
            ->select()->toArray();
        $count = Db::name('user')
            ->where($where)
            ->count();
        foreach ($data as &$item) {
            $qu = Db::name('city')->where('id', $item['city'])->find();
            $item['d'] = $qu['cityName'];
            $shi = Db::name('city')->where('id', $qu['parentId'])->find();
            $item['c'] = $shi['cityName'];
            $sheng = Db::name('city')->where('id', $shi['parentId'])->find();
            $item['p'] = $sheng['cityName'];
        }
        return json(['code' => 200, 'msg' => '登录成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 管理员登录
     * @Author: 林怼怼
     * @Date: 2025/1/22
     * @Time: 14:27
     */
    public function getLogin()
    {
        $param = input();
        if ($param['pwd'] != 'xnsj6666666') return json(['code' => 201, 'msg' => '密码不正确。']);
        if ($param['phone'] != '19032001802' && $param['phone'] != '18058708881') return json(['code' => 201, 'msg' => '无权限。']);
        $userInfo = Db::table('jw_user')->where(['phone' => $param['phone']])->find();
        return json(['code' => 200, 'msg' => '登录成功。', 'data' => $userInfo]);
    }

    /**
     * Created by PhpStorm.
     * @purpose 微信授权登录
     * @Author: Linxy
     * @Time: 2023/11/30 8:57
     */
    public function wechatAuthLogin()
    {
        $code = input('code', '');
        $appid = env('WECHAT.appId');
        $secret = env('WECHAT.appSecret');
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token";
        $data = [
            'appid' => $appid,
            'secret' => $secret,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ];
        $result = json_decode(http($url, $data, 'GET'), true);
        if (array_key_exists('errcode', $result)) return json(['code' => 302, 'msg' => '错误。']);
        // 查询用户信息
        $url = "https://api.weixin.qq.com/sns/userinfo";
        $data = [
            'access_token' => $result['access_token'],
            'openid' => $result['openid'],
            'lang' => "zh_CN"
        ];
        $userInfo = json_decode(http($url, $data, 'GET'), true);
        $data = Db::name('user')->where(['openid' => $result['openid']])->find();
        if (!$data) {
            return json(['code' => 301, 'msg' => '该微信未绑定账号。', 'data' => $userInfo]);
        }
        // 是否答题 0没答题过 其他全是答题过
        $data['is_over'] = Db::name('exam_log')->where('phone', $data['phone'])->count();
        return json(['code' => 200, 'msg' => '登录成功。', 'data' => $data]);
    }

    /**
     * @Description: 提交题目答案
     * @Author: 林怼怼
     * @Date: 2025/1/21
     * @Time: 11:00
     */
    public function setExamSave()
    {
        $param = input();
        try {
            foreach ($param['data'] as $item) {
                foreach ($item['questions'] as $v) {
                    // 记录答案
                    Db::name('exam_log')->save([
                        't_id' => $item['categoryId'],
                        'p_id' => $v['questionId'],
                        'answer' => implode(',', $v['answerId']),
                        'phone' => $param['phone'],
                        'time' => time()
                    ]);
                }
            }
            Db::name('user')->where('phone', $param['phone'])->save(['type' => 3, 'totalTime' => $param['totalTime']]);
            return json([
                'code' => 200,
                'msg' => '成功等3-5天',
            ]);
        } catch (\Exception $e) {
            return json(['code' => 500, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查询全部题目
     * @Author: 林怼怼
     * @Date: 2025/1/21
     * @Time: 09:43
     */
    public function getExamList()
    {
        try {
            // 获取所有题目分类
            $topics = $this->getTopics();
            if (empty($topics)) {
                return json(['code' => 200, 'msg' => '成功', 'data' => [], 'num' => 0]);
            }

            // 获取题目和答案数据
            $result = $this->processTopicsData($topics);

            return json([
                'code' => 200,
                'msg' => '成功',
                'data' => $result['data'],
                'num' => $result['totalQuestions']
            ]);
        } catch (\Exception $e) {
            return json(['code' => 500, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * 获取题目分类
     */
    private function getTopics()
    {
        return Db::table('jw_exam_topic')
            ->order('sort', 'asc')
//            ->cache(300) // 缓存5分钟
            ->select()
            ->toArray();
    }

    /**
     * 处理题目数据
     */
    private function processTopicsData($topics)
    {
        // 获取所有题目ID
        $topicIds = array_column($topics, 'id');

        // 批量获取题目列表
        $examList = $this->getExamListByTopicIds($topicIds);

        // 获取所有题目ID和父ID
        $examIds = [];
        $parentIds = [];
        foreach ($examList as $exam) {
            $examIds[] = $exam['id'];
            $parentIds[] = $exam['p_id'];
        }

        // 批量获取答案
        $answers = $this->getAnswersByIds($examIds, $parentIds);

        // 组织数据结构
        $totalQuestions = 0;
        foreach ($topics as &$topic) {
            $topic['title_arr'] = [];
            foreach ($examList as $exam) {
                if ($exam['p_id'] == $topic['id']) {
                    // 添加答案数据
                    $exam['answer'] = $this->getExamAnswers($topic['id'], $exam, $answers);
                    $topic['title_arr'][] = $exam;
                    $totalQuestions++;
                }
            }
        }

        return [
            'data' => $topics,
            'totalQuestions' => $totalQuestions
        ];
    }

    /**
     * 批量获取题目列表
     */
    private function getExamListByTopicIds($topicIds)
    {
        return Db::table('jw_exam_list')
            ->whereIn('p_id', $topicIds)
            ->where('state', 1)
            ->order('sort', 'asc')
//            ->cache(300) // 缓存5分钟
            ->select()
            ->toArray();
    }

    /**
     * 批量获取答案
     */
    private function getAnswersByIds($examIds, $parentIds)
    {
        $answers = [];

        // 获取题目ID关联的答案
        $pIdAnswers = Db::table('jw_exam_answer')
            ->whereIn('p_id', $examIds)
//            ->cache(300) // 缓存5分钟
            ->select()
            ->toArray();
        foreach ($pIdAnswers as $answer) {
            $answers['p_' . $answer['p_id']][] = $answer;
        }

        // 获取父ID关联的答案
        $tIdAnswers = Db::table('jw_exam_answer')
            ->whereIn('t_id', $parentIds)
//            ->cache(300) // 缓存5分钟
            ->select()
            ->toArray();
        foreach ($tIdAnswers as $answer) {
            $answers['t_' . $answer['t_id']][] = $answer;
        }

        return $answers;
    }

    /**
     * 获取题目答案
     */
    private function getExamAnswers($topicId, $exam, $answers)
    {
        if ($topicId == 6 && ($exam['sort'] == 1 || $exam['sort'] == 2)) {
            return $answers['p_' . $exam['id']] ?? [];
        }
        return $answers['t_' . $exam['p_id']] ?? [];
    }

    /**
     * @Description: 发起支付请求
     * @Author: 林怼怼
     * @Date: 2025/1/21
     * @Time: 09:04
     */
    /**
     * 发送微信支付请求
     */
    private function sendRequest($url, $data)
    {
        try {
            $config = Config::get('wechat');
            $mchid = $config['mch_id'];
            $serialNo = $config['serial_no'];
            $privateKey = $this->getPrivateKey($config['certificate']);

            // 准备请求数据
            $method = 'POST';
            $timestamp = time();
            $nonce = $this->generateNonce();
            $body = json_encode($data);

            // 构造签名字符串
            $message = $this->buildMessage($method, $url, $timestamp, $nonce, $body);

            // 计算签名值
            $signature = $this->generateSignature($message, $privateKey);

            // 构造认证头
            $token = sprintf('WECHATPAY2-SHA256-RSA2048 mchid="%s",nonce_str="%s",timestamp="%d",serial_no="%s",signature="%s"',
                $mchid, $nonce, $timestamp, $serialNo, $signature);

            // 发送请求
            $ch = curl_init($url);
            if (!$ch) {
                throw new \Exception("初始化 CURL 失败");
            }

            curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $body,
                // 开发环境临时关闭 SSL 验证
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_HTTPHEADER => [
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3',
                    'Authorization: ' . $token,
                    'Content-Length: ' . strlen($body)
                ]
            ]);

            // 执行请求
            $response = curl_exec($ch);
            if ($response === false) {
                throw new \Exception('CURL Error: ' . curl_error($ch));
            }

            // 获取响应信息
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

            // 分离响应头和响应体
            $responseBody = substr($response, $headerSize);

            // 关闭CURL句柄
            curl_close($ch);

            // 验证响应状态码
            if ($httpCode < 200 || $httpCode >= 300) {
                throw new \Exception("HTTP Error: " . $httpCode . "\nResponse: " . $responseBody);
            }

            // 记录日志
            Log::info('微信支付请求成功', [
                'url' => $url,
                'httpCode' => $httpCode,
                'response' => $responseBody
            ]);

            return $responseBody;

        } catch (\Exception $e) {
            Log::error("微信支付请求失败：" . $e->getMessage());
            throw $e;
        }
    }

    /**
     * 获取商户私钥
     */
    private function getPrivateKey($privateKeyPath)
    {
        try {
            $privateKey = file_get_contents($privateKeyPath);
            if (!$privateKey) {
                throw new \Exception('商户私钥文件不存在');
            }
            return $privateKey;
        } catch (\Exception $e) {
            Log::error("读取商户私钥失败：" . $e->getMessage());
            throw $e;
        }
    }

    /**
     * 生成随机字符串
     */
    private function generateNonce()
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * 构造签名信息
     */
    private function buildMessage($method, $url, $timestamp, $nonce, $body)
    {
        $urlParts = parse_url($url);
        $canonicalUrl = ($urlParts['path'] . (!empty($urlParts['query']) ? "?${urlParts['query']}" : ""));

        return sprintf("%s\n%s\n%d\n%s\n%s\n",
            $method,
            $canonicalUrl,
            $timestamp,
            $nonce,
            $body
        );
    }

    /**
     * 生成签名
     */
    private function generateSignature($message, $privateKey)
    {
        $signature = '';
        if (openssl_sign($message, $signature, $privateKey, OPENSSL_ALGO_SHA256)) {
            return base64_encode($signature);
        }
        throw new \Exception("签名生成失败");
    }

    /**
     * 创建支付订单
     */
    public function createOrder()
    {
        try {
            $param = input();

            // 参数验证
            $this->validateOrderParams($param);

            // 获取配置
            $config = Config::get('wechat');
            if (empty($config['notify_url'])) {
                throw new \Exception('未配置支付回调地址');
            }

            // 生成订单数据
            $orderData = [
                'order_no' => 'ORDER' . date('YmdHis') . mt_rand(1000, 9999),
                'title' => '学习能力测评',
                'amount' => $param['amount'],
                'total_fee' => intval($param['amount'] * 100), // 转换为分
                'openid' => $param['openid'],
                'phone' => $param['phone'],
                'state' => 0,  // 0:未支付 1:已支付 2:已取消 3:已退款
                'transaction_id' => '', // 微信支付订单号
                'pay_time' => 0,  // 支付时间
                'create_time' => time(),
                'update_time' => time()
            ];

            // 构造支付请求数据
            $requestData = [
                'mchid' => $config['mch_id'],
                'appid' => $config['app_id'],
                'description' => $orderData['title'],
                'out_trade_no' => $orderData['order_no'],
                'notify_url' => $config['notify_url'],
                'amount' => [
                    'total' => $orderData['total_fee'],
                    'currency' => 'CNY'
                ],
                'payer' => [
                    'openid' => $orderData['openid']
                ]
            ];

            // 记录请求日志
            Log::info('创建支付订单', [
                'order_data' => $orderData,
                'request_data' => $requestData
            ]);

            // 保存订单信息（先保存，避免回调比请求还快）
            $orderId = $this->saveOrder($orderData);
            if (!$orderId) {
                throw new \Exception('订单保存失败');
            }

            // 发送支付请求
            $response = $this->sendRequest(
                'https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi',
                $requestData
            );
            $responseData = json_decode($response, true);
            if (empty($responseData['prepay_id'])) {
                // 更新订单状态为失败
                $this->updateOrderState($orderData['order_no'], 2); // 设置为已取消
                throw new \Exception('支付请求失败：' . ($responseData['message'] ?? '未知错误'));
            }

            // 生成支付参数
            $payParams = $this->generatePayParams($config['app_id'], $responseData['prepay_id']);

            return json([
                'code' => 200,
                'msg' => '成功',
                'data' => [
                    'order_no' => $orderData['order_no'],
                    'pay_params' => $payParams
                ]
            ]);

        } catch (\Exception $e) {
            Log::error("创建支付订单失败", [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return json(['code' => 500, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * 验证订单参数
     */
    private function validateOrderParams($param)
    {
        if (empty($param['amount']) || $param['amount'] <= 0) {
            throw new \Exception('无效的支付金额');
        }
        if (empty($param['openid'])) {
            throw new \Exception('缺少openid参数');
        }
        if (empty($param['phone'])) {
            throw new \Exception('缺少手机号参数');
        }
    }

    /**
     * 保存订单
     */
    private function saveOrder($orderData)
    {
        try {
            return Db::name('orders')->insertGetId($orderData);
        } catch (\Exception $e) {
            Log::error("保存订单失败", [
                'order_data' => $orderData,
                'error' => $e->getMessage()
            ]);
            throw new \Exception('订单保存失败');
        }
    }

    /**
     * 更新订单状态
     */
    private function updateOrderState($orderNo, $state)
    {
        try {
            return Db::name('orders')
                ->where('order_no', $orderNo)
                ->update([
                    'state' => $state,
                    'update_time' => time()
                ]);
        } catch (\Exception $e) {
            Log::error("更新订单状态失败", [
                'order_no' => $orderNo,
                'state' => $state,
                'error' => $e->getMessage()
            ]);
            throw new \Exception('更新订单状态失败');
        }
    }

    /**
     * 生成支付参数
     */
    private function generatePayParams($appId, $prepayId)
    {
        $timeStamp = (string) time(); // 正确时间戳
        $nonceStr = $this->generateNonce();

        // 构造签名字符串
        $message = "{$appId}\n{$timeStamp}\n{$nonceStr}\nprepay_id={$prepayId}\n";

        // 计算签名
        $signature = $this->generateSignature($message, $this->getPrivateKey(Config::get('wechat.certificate')));

        return [
            'appId' => $appId,
            'timeStamp' => $timeStamp,
            'nonceStr' => $nonceStr,
            'package' => 'prepay_id=' . $prepayId,
            'signType' => 'RSA',
            'paySign' => $signature
        ];
    }

    /**
     * 处理支付结果
     */
    private function handlePayResult($result)
    {
        Db::startTrans();
        try {
            // 查询订单
            $order = Db::name('orders')
                ->where('order_no', $result['out_trade_no'])
                ->find();

            if (!$order) {
                throw new \Exception('订单不存在');
            }

            // 判断是否已处理
            if ($order['state'] == 1) {
                Db::rollback();
                return; // 已支付，直接返回
            }

            // 判断支付状态
            if ($result['trade_state'] === 'SUCCESS') {
                // 更新订单状态
                Db::name('orders')
                    ->where('order_no', $order['order_no'])
                    ->update([
                        'state' => 1, // 已支付
                        'transaction_id' => $result['transaction_id'],
                        'pay_time' => strtotime($result['success_time']),
                        'update_time' => time()
                    ]);

                // 处理业务逻辑
                $this->processBusinessLogic($order);
            } else {
                // 支付失败，更新订单状态
                Db::name('orders')
                    ->where('order_no', $order['order_no'])
                    ->update([
                        'state' => 2, // 已取消
                        'update_time' => time()
                    ]);
            }

            Db::commit();
            return json(['code' => 200, 'msg' => '回调请求结束']);
        } catch (\Exception $e) {
            Db::rollback();
            Log::error('处理支付结果失败', [
                'error' => $e->getMessage(),
                'result' => $result
            ]);
            throw $e;
        }
    }

    /**
     * 处理业务逻辑
     */
    private function processBusinessLogic($order)
    {
        try {
            // 例如：激活用户的测评权限
            Db::name('user')->where(['phone' => $order['phone']])->save([
                'type' => 1
            ]);

            // 可以添加其他业务逻辑

        } catch (\Exception $e) {
            Log::error('处理业务逻辑失败：' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * @Description: 微信支付回调
     * @Author: 林怼怼
     * @Date: 2025/1/21
     * @Time: 09:04
     */

    /**
     * 验证签名
     */
    private function verifySign($headers, $body)
    {
        try {
            $config = Config::get('wechat');

            // 获取微信平台证书
            $platformCert = $this->getPlatformCert();

            // 构造验签名串
            $timestamp = $headers['Wechatpay-Timestamp'][0];
            $nonce = $headers['Wechatpay-Nonce'][0];
            $signature = $headers['Wechatpay-Signature'][0];

            $message = $timestamp . "\n" .
                $nonce . "\n" .
                $body . "\n";

            // 使用微信平台证书验证签名
            $publicKey = openssl_pkey_get_public($platformCert);
            $result = openssl_verify(
                $message,
                base64_decode($signature),
                $publicKey,
                OPENSSL_ALGO_SHA256
            );

            return $result === 1;

        } catch (\Exception $e) {
            Log::error('验证签名失败：' . $e->getMessage());
            return false;
        }
    }

    /**
     * 解密数据
     */
    private function decryptData($body)
    {
        try {
            $config = Config::get('wechat');
            $data = json_decode($body, true);

            // 解密回调数据
            $ciphertext = base64_decode($data['resource']['ciphertext']);
            $associatedData = $data['resource']['associated_data'];
            $nonceStr = $data['resource']['nonce'];

            // 解密
            $result = openssl_decrypt(
                $ciphertext,
                'aes-256-gcm',
                $config['api_v3_key'],
                OPENSSL_RAW_DATA,
                $nonceStr,
                $tag,
                $associatedData
            );

            return json_decode($result, true);

        } catch (\Exception $e) {
            Log::error('解密数据失败：' . $e->getMessage());
            return null;
        }
    }

    /**
     * 获取微信平台证书
     */
    private function getPlatformCert()
    {
        // 建议将证书缓存起来，定期更新
        $cacheKey = 'wechat_platform_cert';
        return Cache::remember($cacheKey, function() {
            // 调用获取证书接口
            // 实现证书下载和缓存逻辑
            return '微信平台证书内容';
        }, 3600 * 24); // 缓存24小时
    }

    /**
     * @Description: 导入答题系统
     * @Author: 林怼怼
     * @Date: 2025/1/20
     * @Time: 16:34
     */
    public function import(Request $request)
    {
        // 启动事务
        Db::startTrans();
        try {
            // 上传 Excel 文件
            $file = request()->file('file'); // 获取上传的文件
            if (!$file) {
                return json(['code' => 400, 'message' => '请上传文件']);
            }

            // 保存上传的文件
            $filePath = Filesystem::putFile('excel', $file);
            if (!$filePath) {
                return json(['code' => 500, 'message' => '文件上传失败']);
            }

            // 获取完整文件路径
            $fullPath = Filesystem::path($filePath);

            // 读取 Excel 文件
            $spreadsheet = IOFactory::load($fullPath);
            $sheet = $spreadsheet->getActiveSheet();
            $rows = $sheet->toArray(null, true, true, true);

            $num = 0;

            // 遍历 Excel 数据并插入到数据库
            foreach ($rows as $index => $row) {
                if ($index === 1) {
                    // 跳过第一行（标题行）
                    continue;
                }

                // 获取数据并插入到数据库
                Db::name('exam_list')->save([
                    'serial'       => $row['A'], // 对应 Excel 第一列
                    'title'        => $row['B'], // 对应 Excel 第二列
                    'desc'         => $row['C'], // 对应 Excel 第三列
                    'countdown'    => $row['D'] ?? 0, // 倒计时（默认 0）
                    'p_id'         => Db::table('jw_exam_topic')->where('title', 'like', '%'.$row['E'])->value('id'), // 题目分类 ID
                    'precautions'  => $row['F'] ?? null, // 注意事项
                    'sort'         => $row['G'] ?? 0, // 排序
                    'type'         => $row['H'], // 题目类型
                    'is_type'      => $row['I'], // 题目类别
                ]);
                $num++;
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'message' => '数据导入成功', 'num' => $num]);
        } catch (\Exception $e) {
            return json(['code' => 500, 'message' => '导入失败: ' . $e->getMessage()]);
        }
    }

    /**
     * Created by PhpStorm.
     * @purpose 读取文件
     * @Author: Linxy
     * @Time: 2023/11/30 11:26
     */
    private function readExcel($filePath)
    {
        // 读取 Excel 文件的逻辑
        $spreadsheet = IOFactory::load($filePath);
        $worksheet = $spreadsheet->getActiveSheet();

        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $rowData = [];
            foreach ($row->getCellIterator() as $cell) {
                $rowData[] = $cell->getValue();
            }
            $data[] = $rowData;
        }
        return $data;
    }

    /**
     * @Description: 1、提交用户信息
     * @Author: 林怼怼
     * @Date: 2025/1/20
     * @Time: 15:19
     */
    public function login()
    {
        $param = input();
        $code = Cache::get($param['phone']);
        if ($code !== $param['verifyCode']) return json(['code' => 201, 'msg' => '验证码不正确。']);
        $userInfo = Db::table('jw_user')->where(['phone' => $param['phone']])->find();
        if (empty($userInfo)) {
            $openid = Db::table('jw_user')->where(['openid' => $param['openid']])->find();
            if (empty($openid)) {
                $id = Db::table('jw_user')->insertGetId([
                    'openid' => $param['openid'],
                    'childName' => $param['childName'],
                    'parentName' => $param['parentName'],
                    'gender' => $param['gender'],
                    'img' => $param['img'],
                    'age' => $param['age'],
                    'city' => $param['city'],
                    'idCard' => $param['idCard'],
                    'phone' => $param['phone'],
                    'time' => time(),
                ]);
            }else{
                $id = Db::table('jw_user')->insertGetId([
                    'childName' => $param['childName'],
                    'parentName' => $param['parentName'],
                    'gender' => $param['gender'],
                    'age' => $param['age'],
                    'city' => $param['city'],
                    'idCard' => $param['idCard'],
                    'phone' => $param['phone'],
                    'time' => time(),
                ]);
            }
        }else{
            $id = $userInfo['id'];
            if ($userInfo['openid'] == null) {
                Db::name('user')->where('id', $userInfo['id'])->save([
                    'openid' => $param['openid'],
                    'img' => $param['img']
                ]);
            }
        }
        $userInfo = Db::table('jw_user')->where(['id' => $id])->find();
        // 是否答题 0没答题过 其他全是答题过
        $userInfo['is_over'] = Db::name('exam_log')->where('phone', $param['phone'])->count();
        return json(['code' => 200, 'msg' => '登录成功。', 'data' => $userInfo]);
    }
}
