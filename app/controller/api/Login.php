<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Cache;
use think\facade\Db;

class Login
{
    /**
     * @Description: 短信验证码登录
     * @Author: 林怼怼
     * @Date: 2024/12/18
     * @Time: 11:07
     */
    public function phoneLogin()
    {
        $param = input();
		$account = Db::name('member')->where('phone', $param['account'])->find();
        if (!$account) {
                return json(['code' => 201, 'msg' => '手机号错误或者未注册。']);
            }else{
                // 查询之前存好的验证码
                $code = Cache::get($param['account']);
                if ($code !== $param['code']) return json(['code' => 201, 'msg' => '验证码不正确。']);
            }
		if (!$account) {
			$account = Db::name('member')->where('number', $param['account'])->find();
			if (!$account) return json(['code' => 201, 'msg' => '账号不存在，请确认账号是否正确！']);
		}
        editUserVIP2($account['id']);
        if ($account['state'] == 0) return json(['code' => 201, 'msg' => '该用户审核中！']);
		if ($account['state'] == 2) return json(['code' => 201, 'msg' => '该用户已冻结,请重新登陆或开通身份！']);
		if ($account['state'] == 3) return json(['code' => 201, 'msg' => '该用户已删除！']);
		return json(['code' => 200, 'msg' => '登录成功。', 'data' => ['token' => encryptToken($account['id'], true)]]);
    }

    /**
     * @Description: 解绑微信
     * @Author: 林怼怼
     * @Date: 2024/1/4/004
     * @Time: 10:25
     */
    public function unbindVX()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            Db::name('member')->save(['id' => $id, 'openid' => '']);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '解绑成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 绑定账号
	 * @Author: Linxy
	 * @Time: 2023/11/30 9:38
	 */
	public function bindPhone()
	{
		$param = input();
        // 启动事务
        Db::startTrans();
        try {
            // 查询之前存好的验证码
            $code = Cache::get($param['phone']);
            if ($code !== $param['code']) return json(['code' => 201, 'msg' => '验证码不正确。']);
            $data = Db::name('member')
                ->where(['openid' => $param['openid']])
                ->find();
            if ($data) return json(['code' => 201, 'msg' => '微信已绑定账号，请先解绑。']);
            $data = Db::name('member')
                ->where(['phone' => $param['phone']])
                ->find();
            if (!$data) return json(['code' => 201, 'msg' => '手机未注册，请先注册。']);
            Db::name('member')
                ->where(['phone' => $param['phone']])
                ->update([
                    'avatar' => $param['avatar'],
                    'openid' => $param['openid']
                ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '绑定成功，自动登录。', 'data' => ['token' => encryptToken($data['id'], true)]]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 微信授权登录
	 * @Author: Linxy
	 * @Time: 2023/11/30 8:57
	 */
	public function wechatAuthLogin()
	{
		$code = input('code', '');
		$appid = env('WECHAT.appId');
		$secret = env('WECHAT.appSecret');
		$url = "https://api.weixin.qq.com/sns/oauth2/access_token";
		$data = [
			'appid' => $appid,
			'secret' => $secret,
			'code' => $code,
			'grant_type' => 'authorization_code',
		];
		$result = json_decode(http($url, $data, 'GET'), true);
        if (array_key_exists('errcode', $result)) return json(['code' => 302, 'msg' => '错误。']);
        // 查询用户信息
        $url = "https://api.weixin.qq.com/sns/userinfo";
        $data = [
            'access_token' => $result['access_token'],
            'openid' => $result['openid'],
            'lang' => "zh_CN"
        ];
        $userInfo = json_decode(http($url, $data, 'GET'), true);
		$data = Db::name('member')->where(['openid' => $result['openid']])->find();
		if (!$data) {
			return json(['code' => 301, 'msg' => '该微信未绑定账号。', 'data' => $userInfo]);
		}
		return json(['code' => 200, 'msg' => '登录成功。', 'data' => ['userInfo' => $userInfo, 'token' => encryptToken($data['id'], true)]]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 获取vxjsSDK
	 * @Author: Linxy
	 * @Time: 2023/11/30 9:23
	 */
	public function jsSDK()
	{
        $url = input('url');
        $jsapiTicket = vx_js_SDK();
        $timestamp = time();
        $nonceStr = $this->createNonceStr();
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
        $signature = sha1($string);
        $signPackage = array(
            "appId"     => env('WECHAT.appId'),
            "nonceStr"  => $nonceStr,
            "timestamp" => $timestamp,
            "url"       => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return json(['code' => 200, 'msg' => 'JSSDK。', 'data' => $signPackage]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 查看用户是否升级
	 * @Author: Linxy
	 * @Time: 2023/11/24 14:29
	 */
	public function getUserUp()
	{
		$userInfo = getUserInfo(true);
		$result = false;
		$data = Db::name('member_upgrade')
			->where(['id' => $userInfo['id'], 'state' => 1])
			->find();
		if ($data) {
			$result = true;
			$res = Db::name('member_upgrade')
				->where(['id' => $userInfo['id']])
				->update(['state' => 2]);
		}
		return json(['code' => 200, 'msg' => '成功。', 'data' => ['is_up' => $result]]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 登录
	 * @Author: Linxy
	 * @Time: 2023/10/17 11:16
	 */
    public function login()
	{
		$param = input();
		$account = Db::name('member')->where('phone', $param['account'])->find();
        if (!$account) {
			$account = Db::name('member')->where('number', $param['account'])->find();
			if (!$account) return json(['code' => 201, 'msg' => '账号不存在，请确认账号是否正确！']);
		}
        editUserVIP2($account['id']);
		if ($account['pwd'] !== encrypt_password($param['pwd'])) return json(['code' => 201, 'msg' => '密码不正确，请确认后重试！']);
        if ($account['state'] === 0) return json(['code' => 201, 'msg' => '该用户审核中！']);
		if ($account['state'] === 2) return json(['code' => 201, 'msg' => '该用户已冻结,请重新登陆或开通身份！']);
		if ($account['state'] === 3) return json(['code' => 201, 'msg' => '该用户已删除！']);
        $count = Db::name('member_grade')->where(['id' => $account['id'], 'grade' => 12])->count();
        if ($account['id'] != 1) {
            if ($count == 0) Db::name('member_grade')->save(['id' => $account['id']]);
        }
		return json(['code' => 200, 'msg' => '登录成功。', 'data' => ['token' => encryptToken($account['id'], true)]]);
	}

    private function createNonceStr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }


    /**
     * Created by PhpStorm.
     * @purpose 检测会员到期后一个月是否续费(定时任务)  需要测试
     * @Author: Linxy
     * @Time: 2023/11/24 15:36
     */
    public function editUserVIP()
    {
        // 启动事务
        Db::startTrans();
        try {
            // 查询出已经到期的
            $data = Db::name('member_grade')
                ->where([
                    ['vip_end_time', '<', time()],
                    ['grade', 'in', [1,4,7,10]]
                ])
                ->select()->toArray();
            // 查询到期以后除了账号
            $arr = [];
            foreach ($data as $key => $val) {
                // 到期后去掉他的等级身份
                if ($val['vip_end_time'] < time()) {
                    if ($val['grade'] === 1) {
                        // 去除365慧乐福身份
                        Db::name('member_grade')->where([
                            ['id', '=', $val['id']],
                            ['grade', 'in', [1,2]],
                        ])->delete();
                    }elseif ($val['grade'] === 4) {
                        // 去除521了了派身份
                        Db::name('member_grade')->where([
                            ['id', '=', $val['id']],
                            ['grade', 'in', [4]],
                        ])->delete();
                    }elseif ($val['grade'] === 7) {
                        // 去除365悦读慧身份
                        Db::name('member_grade')->where([
                            ['id', '=', $val['id']],
                            ['grade', 'in', [7,8]],
                        ])->delete();
                    }elseif ($val['grade'] === 10) {
                        // 去除好妈妈身份
                        Db::name('member_grade')->where([
                            ['id', '=', $val['id']],
                            ['grade', 'in', [10]],
                        ])->delete();
                    }
                }
                // 查看是否还有身份
                $count = Db::name('member_grade')->where('id', $val['id'])->count();
                if ($count === 0) {
                    // 如果没身份账号冻结
                    Db::name('member')->where('id', $val['id'])->update(['state' => 2]);
                }
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检测用户是否到期异常，错误：'.$e->getMessage();
            BackstageLog(1, '定时任务', $desc);
            return json(['code' => 201, 'msg' => '失败，异常。', 'data' => $e->getMessage()]);
        }
    }

    /**
     * Created by PhpStorm.
     * @purpose 检测会员如果没有到期但是账号被冻结了自动恢复解冻(定时任务)
     * @Author: Linxy
     * @Time: 2023/11/24 15:36
     */
    public function editUserVIP2()
    {
        // 启动事务
        Db::startTrans();
        try {
            // 检查是否有没到期的身份
            $data = Db::name('member')
                ->where('state', 2)
                ->select()->toArray();
            $arr = [];
            foreach ($data as $key => $val) {
                // 查询被冻结的用户有没有没到期的身份
                $user_data = Db::name('member_grade')->where('id', $val['id'])->select()->toArray();
                if (count($user_data) !== 0) {
                    // 不等于0说明有没到期的身份
                    foreach ($user_data as &$datum) {
                        if ($datum['vip_end_time'] > time()) {
                            // 没办法续身份直接修改账号状态为正常
                            Db::name('member')->save(['id' => $datum['id'], 'state' => 1]);
                        }else{
                            Db::name('member_grade')->where(['id' => $datum['id'], 'grade' => $datum['grade']])->delete();
                        }
                    }
                }
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检测用户没到期状态异常，错误：'.$e->getMessage();
            BackstageLog(1, '定时任务', $desc);
            return json(['code' => 201, 'msg' => '失败。']);
        }
    }

    /**
     * Created by PhpStorm.
     * @purpose 检查活动是否开始结束(定时任务)
     * @Author: Linxy
     * @Time: 2023/10/25 17:53
     */
    public function examine()
    {
        // 启动事务
        Db::startTrans();
        try {
            # 批量修改(进行中)
            $result = Db::name('meeting')
                ->whereTime('start_time', '<=', date('Y-m-d H:i:s'))
                ->whereTime('end_time', '>=', date('Y-m-d H:i:s'))
                ->where('state', '<>', 4)
                ->update(['state' => 2]);
            # 批量修改(已结束)
            $ids = Db::name('meeting')
                ->whereTime('end_time', '<=', date('Y-m-d H:i:s'))
                ->where('state', '<>', 4)
                ->column('id');
            Db::name('meeting')->whereIn('id', $ids)->update(['state' => 3]);
            Db::name('member_meeting')->where('complete', 1)->whereIn('m_id', $ids)->delete();
            # 批量修改(未开始)
            $result = Db::name('meeting')
                ->whereTime('start_time', '>=', date('Y-m-d H:i:s'))
                ->where('state', '<>', 4)
                ->update(['state' => 1]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '活动状态已更新。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检查活动是否开始结束异常，错误：'.$e->getMessage();
            BackstageLog(1, '定时任务', $desc);
            // 这是进行异常捕获
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }


    /**
     * @Description: 定时发布公告
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 15:56
     */
    public function saveNotice()
    {
        // 启动事务
        Db::startTrans();
        try {
            $data = Db::name('notice_time')
                ->where([
                    ['message_time', '<=', time()],
                    ['state', '=', 2]
                ])
                ->select()->toArray();
            foreach ($data as $key => $val) {
                $id = Db::name('notice')->insertGetId([
                    'title' => $val['title'],
                    'u_id' => $val['u_id'],
                    'img' => $val['img'],
                    'desc' => $val['desc'],
                    'time' => time()
                ]);
                $arr = [];
                $ids = explode(',', $val['ids']);
                $count = count($ids);
                for ($i = 0; $i < $count; $i++) {
                    $arr[] = ['id' => $id, 'u_id' => $ids[$i], 'state' => 2, 'time' => time()];
                }
                Db::name('notice_user')->insertAll($arr);
                Db::name('notice_time')->save(['id' => $val['id'], 'state' => 1]);
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'定时发布活动异常，错误：'.$e->getMessage();
            BackstageLog(1, '定时任务', $desc);
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }
}
