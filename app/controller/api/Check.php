<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Cache;
use think\facade\Db;

class Check
{
    /**
     * @Description: 删除预购订单
     * @Author: 林怼怼
     * @Date: 2024/12/12
     * @Time: 16:48
     */
    public function delCheckFist()
    {
        $param = input();
        Db::name('order')->delete($param['id']);
        return json(['code' => 200, 'msg' => '成功。']);
    }

    /**
     * @Description: 查询课程详情
     * @Author: 林怼怼
     * @Date: 2024/11/4
     * @Time: 14:07
     */
    public function findProduct()
    {
        $param = input();
        $data = Db::name('course')->find($param['id']);
        return json(['code' => 200, 'msg' => '通过。', 'data' => $data]);
    }

    /**
     * @Description:
     * @Author: 林怼怼
     * @Date: 2024/3/13/013
     * @Time: 14:46
     */
    public function saveProductList()
    {

    }

    /**
     * @Description: 注册时验证是否开通产品
     * @Author: 林怼怼
     * @Date: 2024/3/13/013
     * @Time: 11:12
     */
    public function auth()
    {
        $id = input('id');
        $uid = input('p_id');
        if ($id == 1) {
            // 思享荟
            $userInfo = Db::name('member_grade')
                ->where([
                    ['id', '=', $uid],
                    ['grade', 'in', [1, 3, 13, 14, 15, 16]]
                ])
                ->count();
        }elseif($id == 2) {
            // 了了派
            $userInfo = Db::name('member_grade')
                ->where([
                    ['id', '=', $uid],
                    ['grade', 'in', [4, 5, 6, 13, 14, 15, 16]]
                ])
                ->count();
        }elseif($id == 3) {
            // 悦读慧
            $userInfo = Db::name('member_grade')
                ->where([
                    ['id', '=', $uid],
                    ['grade', 'in', [7, 9, 13, 14, 15, 16]]
                ])
                ->count();
        }elseif($id == 11) {
            // 好妈妈
            $userInfo = Db::name('member_grade')
                ->where([
                    ['id', '=', $uid],
                    ['grade', 'in', [10, 5, 6, 13, 14, 15, 16]]
                ])
                ->count();
        }elseif($id == 8) {
            // 游客
//            $userInfo = Db::name('member_grade')
//                ->where([
//                    ['id', '=', $uid],
//                    ['grade', 'in', [10,11]]
//                ])
//                ->count();
            return json(['code' => 200, 'msg' => '通过。']);
        }else{
            return json(['code' => 201, 'msg' => '产品已下架。']);
        }
        if ($uid != 1) {
            if ($userInfo === 0) return json(['code' => 201, 'msg' => '请联系推荐人开通产品。']);
        }
        return json(['code' => 200, 'msg' => '通过。']);
    }

    /**
     * @Description: 查看课程记录
     * @Author: 林怼怼
     * @Date: 2024/3/8/008
     * @Time: 9:20
     */
    public function courseLog()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $where = [];
        if (!empty($param['id'])) $where[] = ['t1.c_id', '=', $param['id']];
        $where[] = ['t1.p_id', '=', $userInfo['id']];
        $where[] = ['t1.type', '=', 5];
        $where[] = ['t1.state', '=', 1];
        $data = Db::name('order')
            ->alias('t1')
            ->join('course t2', 't1.c_id = t2.id')
            ->join('member t3', 't1.u_id = t3.id')
            ->where($where)
            ->field('t3.id, SUM(t1.num) as num, t2.title, t3.avatar, t3.name, t3.phone, t1.id as c_id')
            ->group('t1.id')
            ->page((int)$param['page'], (int)$param['limit'])
            ->select()->toArray();
        $count = Db::name('order')
            ->alias('t1')
            ->join('course t2', 't1.c_id = t2.id')
            ->join('member t3', 't1.u_id = t3.id')
            ->where($where)
            ->group('t1.id')
            ->count();
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 课程报名申请
     * @Author: 林怼怼
     * @Date: 2024/1/18/018
     * @Time: 16:03
     */
    public function courseSave()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 查看购买的那个课程
        $course = Db::name('course')->where('id', $param['id'])->find();
        // 查询上级笑长微笑长或者总部
        $data = getParentCourse($userInfo['id']);
        // 启动事务
        Db::startTrans();
        try {
            $res = true;
            // 微笑长和笑长可以直接消耗库存购买
            if (in_array(5, $userInfo['level']) || in_array(6, $userInfo['level'])) {
                // 减少库存 (需要修改 扣除那种库存)
                $type = $course['type'];
                $num = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => $type])->value('num');
                if ($num < $param['num']) return json(['code' => 203, 'msg' => '库存不足。']);
                // 扣除库存
                Db::name('member_pay')
                    ->where([
                        'id' => $userInfo['id'],
                        'type' => $type,
                    ])
                    ->dec('num', (int)$param['num'])
                    ->update();
                // 添加订单
                Db::name('order')->save([
                    'u_id' => $userInfo['id'],
                    'p_id' => $data['id'],
                    'c_id' => $param['id'],
                    'num' => $param['num'],
                    'type' => 5,
                    'state' => 1,
                    'time' => time(),
                ]);
                $res = true;
            }else{
                // 普通身份增加订单
                // 添加订单
                Db::name('order')->save([
                    'u_id' => $userInfo['id'],
                    'p_id' => $data['id'],
                    'c_id' => $param['id'],
                    'num' => $param['num'],
                    'type' => 5,
                    'time' => time(),
                ]);
                $res = false;
            }
            // 提交事务
            Db::commit();
            if ($res) {
                return json(['code' => 200, 'msg' => '购买课程成功，库存已消耗。']);
            }else{
                return json(['code' => 201, 'msg' => '订单已申请，请联系上级笑长或微笑长审核。']);
            }
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 课程列表
     * @Author: 林怼怼
     * @Date: 2024/1/18/018
     * @Time: 15:30
     */
    public function courseList()
    {
        $param = input();
        $data = Db::name('course')
            ->page((int) $param['page'], (int) $param['limit'])
            ->select()->toArray();
        $count = Db::name('course')->count();
        foreach ($data as &$datum) {
            // 购买该课程的人数
            $datum['number'] = Db::name('order')->where(['c_id' => $datum['id'], 'type' => 5, 'state' => 1])->count();
            if ($datum['id'] === 51) {
                // 爱能营
                $datum['number'] += 2715;
            }elseif ($datum['id'] === 53) {
                // 学能营
                $datum['number'] += 668;
            }elseif ($datum['id'] === 54) {
                // 自信营
                $datum['number'] += 492;
            }elseif ($datum['id'] === 55) {
                // 管能营
                $datum['number'] += 244;
            }elseif ($datum['id'] === 56) {
                // 少年追梦赢
                $datum['number'] += 165;
            }elseif ($datum['id'] === 59) {
                // 效能营
                $datum['number'] += 573;
            }elseif ($datum['id'] === 60) {
                // 学历规划营
                $datum['number'] += 496;
            }elseif ($datum['id'] === 61) {
                // 思享荟
                $datum['number'] += 4200;
            }elseif ($datum['id'] === 62) {
                // 了了派
                $datum['number'] += 2003;
            }elseif ($datum['id'] === 63) {
                // 悦读慧
                $datum['number'] += 560;
            }
        }
        return json(['code' => 200, 'msg' => '成功', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 审核提醒
     * @Author: 林怼怼
     * @Date: 2023/12/30/030
     * @Time: 17:40
     */
    public function check_notice()
    {
        $userInfo = getUserInfo(true);
        $data = [];
        // 注册审核
        $data['user'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 1]
            ])
            ->count();
        // 预购审核
        $data['balance'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 4]
            ])
            ->count();
        // 续费审核
        $data['renew'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 2]
            ])
            ->count();
        // 活动审核
        $data['activity'] = 0;
        if ($userInfo['id'] == 1){
            $data['activity'] = Db::name('meeting')
                ->where([
                    ['state', '=', 4],
                ])
                ->count();
        }
        // 产品审核
        $data['product'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 3]
            ])
            ->count();
        // 课程审核
        $data['course'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 5]
            ])
            ->count();
        // 活动审核
        $data['meeting'] = Db::name('order')
            ->where([
                ['state', '=', 0],
                ['p_id', '=', $userInfo['id']],
                ['type', 'in', 6]
            ])
            ->count();

        // 我的预购申请
        $data['me_balance'] = Db::name('order')
            ->where([
                ['type', 'in', 4],
                ['state', '=', 0],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 我的续费申请
        $data['me_renew'] = Db::name('order')
            ->where([
                ['type', 'in', 2],
                ['state', '=', 0],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 我的活动申请
        $data['me_activity'] = Db::name('meeting')
            ->where([
                ['state', '=', 4],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 我的增产申请
        $data['me_product'] = Db::name('order')
            ->where([
                ['type', 'in', 3],
                ['state', '=', 0],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 我的课程审核
        $data['me_course'] = Db::name('order')
            ->where([
                ['type', 'in', 5],
                ['state', '=', 0],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 我的活动审核
        $data['me_meeting'] = Db::name('order')
            ->where([
                ['type', 'in', 6],
                ['state', '=', 0],
                ['u_id', '=', $userInfo['id']]
            ])
            ->count();
        // 消息推送
        $data['remind'] = Db::name('remind')
            ->where([
                ['c_id', '=', $userInfo['id']],
                ['state', '=', 1]
            ])
            ->count();
        $count = 0;
        $count += $data['user']+$data['balance']+$data['renew']+$data['activity']+$data['product']+$data['meeting'];
        return json(['code' => 200, 'msg' => '未处理审核。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 申请开通项目
     * @Author: 林怼怼
     * @Date: 2023/12/29/029
     * @Time: 15:05
     */
    public function save_product()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            // 查看是否开通这个产品
            if ($param['c_id'] == 1) {
                // 是否开通365慧乐福
                $res = Db::name('member_grade')->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', 'in', [1]],
                ])->find();
            }elseif($param['c_id'] == 2) {
                // 是否开通521了了派
                $res = Db::name('member_grade')->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', 'in', [4]],
                ])->find();
            }elseif($param['c_id'] == 3) {
                // 是否开通365悦读慧
                $res = Db::name('member_grade')->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', 'in', [7]],
                ])->find();
            }elseif($param['c_id'] == 8) {
                // 是否开通学员证
                $res = Db::name('member_grade')->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', 'in', [12]],
                ])->find();
            }elseif($param['c_id'] == 11) {
                // 是否开通好妈妈
                $res = Db::name('member_grade')->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', 'in', [10]],
                ])->find();
            }else{
                return json(['code' => 201, 'msg' => 'c_id参数错误。']);
            }
            if ($res) return json(['code' => 201, 'msg' => '会员已经开通了，尝试一下其他会员吧。']);
            if ($param['c_id'] == 8 || $param['c_id'] == 11) {
                Db::name('order')->insert([
                    'u_id' => $userInfo['id'],
                    'p_id' => $param['p_id'],
                    'c_id' => $param['c_id'],
                    'year' => $param['year'],
                    'type' => 3,
                    'time' => time()
                ]);
            }else{
                Db::name('order')->insert([
                    'u_id' => $userInfo['id'],
                    'p_id' => $param['p_id'],
                    'c_id' => $param['c_id'],
                    'year' => $param['year'],
                    'type' => 3,
                    'vip_start_time' => time(),
                    'vip_end_time' => strtotime("+" . $param['year'] . " year", time()),
                    'time' => time()
                ]);
            }

            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '申请已提交。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 购买名额申请
	 * @Author: Linxy
	 * @Time: 2023/11/28 17:30
	 */
    /**
     * 购买名额申请
     */
    public function saveQuota()
    {
        $params = input();
        $userInfo = getUserInfo(true);

        // 产品权限配置
        $productConfig = [
            4 => [1, 3, 13, 14, 15, 16],                    // 慧乐福
            5 => [4, 5, 6, 13, 14, 15, 16],                 // 了了派
            6 => [7, 9, 13, 14, 15, 16],                    // 悦读慧
            7 => [5, 6],                    // 999系列
            9 => [10, 11, 13, 14, 15, 16],                  // 好妈妈
//            10 => [10, 11, 13, 14, 15, 16],                 // 好妈妈课程
        ];

        // 验证产品是否存在
        if (!isset($productConfig[$params['c_id']])) {
            return json(['code' => 201, 'msg' => '产品已下架。']);
        }

        // 验证用户权限 - 只要有一个匹配即可
        if (empty(array_intersect($userInfo['level'], $productConfig[$params['c_id']]))) {
            return json(['code' => 201, 'msg' => '请先开通对应产品。']);
        }

        // 创建订单
        Db::name('order')->insert([
            'u_id' => $userInfo['id'],
            'p_id' => $params['p_id'],
            'c_id' => $params['c_id'],
            'image' => $params['img'],
            'num' => $params['num'],
            'type' => $params['type'],
            'time' => time(),
        ]);

        return json(['code' => 200, 'msg' => '申请已提交。']);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 续费会员
	 * @Author: Linxy
	 * @Time: 2023/11/14 16:29
	 */
	public function create()
	{
        $param = input();
        $userInfo = getUserInfo(true);
        $grade = 0;
        if ($param['c_id'] == 1) {
            // 查看是否有思享荟
            $count = Db::name('member_grade')
                ->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', '=', 1]
                ])->count();
            $grade = 1;
            if ($count == 0) return json(['code' => 201, 'msg' => '请先开通对应产品。']);
        }elseif($param['c_id'] == 2) {
            // 查看是否有了了派
            $count = Db::name('member_grade')
                ->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', '=', 4]
                ])->count();
            $grade = 4;
            if ($count == 0) return json(['code' => 201, 'msg' => '请先开通对应产品。']);
        }elseif($param['c_id'] == 3) {
            // 查看是否有阅读慧
            $count = Db::name('member_grade')
                ->where([
                    ['id', '=', $userInfo['id']],
                    ['grade', '=', 7]
                ])->count();
            $grade = 7;
            if ($count == 0) return json(['code' => 201, 'msg' => '请先开通对应产品。']);
        }else{
            return json(['code' => 201, 'msg' => '产品已下架。']);
        }

        $time = Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => $grade])->value('vip_end_time');
        // 启动事务
        Db::startTrans();
        try {
            Db::name('order')->save([
                'u_id' => $userInfo['id'],
                'p_id' => $param['p_id'],
                'c_id' => $param['c_id'],
                'year' => $param['year'],
                'type' => 2,
                'vip_start_time' => time(),
                'vip_end_time' => strtotime("+" . $param['year'] . " year", $time),
                'time' => time()
            ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '续费会员申请已经提交快去申请中心看看吧。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 验证码验证
	 * @Author: Linxy
	 * @Time: 2023/11/15 11:03
	 */
	public function SMS_verify_code()
	{
		$param = input();
		// 0代表本地不需要发送短信验证码
		if (env('ALY_SMS.ALY_STATE', '') == '0') {
			return json(['code' => 200, 'msg' => '验证通过。']);
		}
		# 查询之前存好的验证码
		$code = Cache::get($param['phone']);
		if ($code == $param['code']) {
			return json(['code' => 200, 'msg' => '验证通过。']);
		}
		return json(['code' => 201, 'msg' => '验证码不正确。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 审核用户/名额/课程
	 * @Author: Linxy
	 * @Time: 2023/10/13 10:01
	 */
    public function edit()
    {
        try {
            $param = $this->validateEditParams();

            $userInfo = getUserInfo(true);

            // 获取并验证订单信息
            $orderData = $this->validateOrderData($param['id'], $userInfo['id']);

            // 开始事务处理
            Db::startTrans();

            // 更新订单状态
            $this->updateOrderStatus($param, $orderData);

            // 处理不同类型的订单
            $this->processOrderByType($param, $orderData);

            Db::commit();
            if($orderData['c_id'] == 11) {
                Db::name('remind')->save([
                    'u_id' => $orderData['p_id'],
                    'c_id' => $orderData['u_id'],
                    'r_id' => 0,
                    'desc' => '您已成功开通好妈妈身份已获得 《好妈妈证书》 快去我的身份看看吧！',
                    'type' => 5,
                    'time' => time()
                ]);
            }
            return json(['code' => 200, 'msg' => '审核完成。']);
        } catch (\Throwable $e) {
            Db::rollback();
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * 验证编辑参数
     */
    private function validateEditParams(): array
    {
        $param = input();
        if (empty($param['id']) || !isset($param['state'])) {
            throw new \Exception('参数错误');
        }
        return $param;
    }

    /**
     * 验证订单数据
     */
    private function validateOrderData(int $orderId, int $userId): array
    {
        $orderData = Db::name('order')->find($orderId);
        if (!$orderData) {
            throw new \Exception('订单不存在。');
        }

        $parent = Db::name('member')->find($orderData['p_id']);
        if ($userId !== $parent['id']) {
            throw new \Exception('审核人不正确。');
        }

        return array_merge($orderData, ['parent' => $parent]);
    }

    /**
     * 更新订单状态
     */
    private function updateOrderStatus(array $param, array $orderData): void
    {
        Db::name('order')->save([
            'id' => $param['id'],
            'state' => $param['state'],
            'desc' => $param['desc'],
            'steate_time' => time()
        ]);
    }

    /**
     * 处理不同类型的订单
     */
    private function processOrderByType(array $param, array $orderData): void
    {
        if ($param['state'] != 1) {
            // 如果不是通过状态，只需要处理注册驳回的情况
            if ($orderData['type'] == 1) {
                $this->handleRegistrationRejection($orderData);
            }
            return;
        }

        $handlers = [
            1 => [$this, 'handleRegistration'],
            2 => [$this, 'handleRenewal'],
            3 => [$this, 'handleIncrease'],
            4 => [$this, 'handlePrePurchase'],
            5 => [$this, 'handleCourse'],
            6 => [$this, 'handleActivity']
        ];

        if (!isset($handlers[$orderData['type']])) {
            throw new \Exception('参数异常。');
        }

        call_user_func($handlers[$orderData['type']], $orderData);
    }

    /**
     * 处理注册驳回
     */
    private function handleRegistrationRejection(array $orderData): void
    {
        $tables = ['member', 'member_address', 'member_grade', 'member_parent', 'member_pay'];
        foreach ($tables as $table) {
            Db::name($table)->where('id', $orderData['u_id'])->delete();
        }
        Db::name('order')->where('id', $orderData['id'])->delete();
    }

    /**
     * 处理注册审核
     */
    private function handleRegistration(array $orderData): void
    {
        $memberConfig = $this->getMemberConfigUp($orderData['c_id']);
        if (!$memberConfig) {
            throw new \Exception('错误的会员类型。');
        }

        if ($memberConfig['type'] != 0) {
            $this->checkAndDeductStock($orderData['parent']['id'], $memberConfig['type']);
        }

        // 更新会员状态
        Db::name('member')->save([
            'id' => $orderData['u_id'],
            'state' => 1,
            'time' => time()
        ]);

        // 赠送积分
        integralInc($orderData['p_id']);
    }

    /**
     * 处理续费审核
     */
    private function handleRenewal(array $orderData): void
    {
        $memberConfig = $this->getMemberConfigUp($orderData['c_id']);
        if (!$memberConfig) {
            throw new \Exception('错误的会员类型。');
        }

        if ($orderData['c_id'] == 8) {
            throw new \Exception('学员证无需续费。');
        }

        $this->checkAndDeductStock($orderData['parent']['id'], $memberConfig['type'], $orderData['year']);

        // 更新会员等级
        Db::name('member_grade')->save([
            'id' => $orderData['u_id'],
            'grade' => $memberConfig['grade'],
            'vip_start_time' => time(),
            'vip_end_time' => $orderData['vip_end_time']
        ]);

        integralInc($orderData['p_id']);
    }

    /**
     * 获取会员配置
     */
    private function getMemberConfigUp(int $cId): ?array
    {
        $configs = [
            1 => ['type' => 1, 'grade' => 1],
            2 => ['type' => 2, 'grade' => 4],
            3 => ['type' => 3, 'grade' => 7],
            8 => ['type' => 0, 'grade' => 12],
            11 => ['type' => 5, 'grade' => 10]
        ];

        return $configs[$cId] ?? null;
    }

    /**
     * 检查并扣除库存
     */
    private function checkAndDeductStock(int $parentId, int $type, int $amount = 1)
    {
        $num = Db::name('member_pay')
            ->where(['id' => $parentId, 'type' => $type])
            ->value('num');

        if ($num < $amount) {
            return json(['code' => 203, 'msg' => '库存不足。']);
        }

        Db::name('member_pay')
            ->where(['id' => $parentId, 'type' => $type])
            ->dec('num', $amount)
            ->update();
    }

    /**
     * 处理增产审核
     */
    private function handleIncrease(array $orderData): void
    {
        $memberConfig = $this->getMemberConfig($orderData['c_id']);
        if (!$memberConfig) {
            throw new \Exception('错误的会员类型。');
        }

        // 学员证特殊处理
        if ($orderData['c_id'] == 8) {
            Db::name('member_grade')->insert([
                'id' => $orderData['u_id'],
                'grade' => $memberConfig['grade']
            ]);
            return;
        }

        // 检查并扣除库存
        $this->checkAndDeductStock(
            $orderData['parent']['id'],
            $memberConfig['type'],
            $orderData['year']
        );

        // 好妈妈特殊处理
        if ($orderData['c_id'] == 11) {
            Db::name('member_grade')->insert([
                'id' => $orderData['u_id'],
                'grade' => $memberConfig['grade'],
                'code' => substr(sprintf('%07s',
                    (int)Db::name('member_grade')
                        ->where('grade', 10)
                        ->max('code') + 1
                ), -6)
            ]);
        }else{
            // 增加会员等级
            Db::name('member_grade')->insert([
                'id' => $orderData['u_id'],
                'grade' => $memberConfig['grade'],
                'vip_start_time' => $orderData['vip_start_time'],
                'vip_end_time' => $orderData['vip_end_time']
            ]);
        }

        // 赠送积分
        integralInc($orderData['p_id']);
    }

    /**
     * 处理预购审核
     */
    private function handlePrePurchase(array $orderData): void
    {
        // 获取库存类型
        $typeConfig = [
            4 => 1,  // 慧乐福库存
            5 => 2,  // 了了派库存
            6 => 3,  // 悦读慧库存
            7 => 4,  // 999系列库存
            9 => 5,  // 好妈妈库存
            10 => 7  // 好妈妈课程库存
        ];

        if (!isset($typeConfig[$orderData['c_id']])) {
            throw new \Exception('库存已经下架。');
        }

        $type = $typeConfig[$orderData['c_id']];

        // 检查父级库存
        $this->checkAndDeductStock(
            $orderData['parent']['id'],
            $type,
            $orderData['num']
        );

        // 初始化用户库存记录
        $this->initializeUserStock($orderData['u_id'], $type);

        // 直接更新库存，不再重复扣除
        Db::name('member_pay')
            ->where([
                'id' => $orderData['u_id'],
                'type' => $type
            ])
            ->inc('num', $orderData['num'])
            ->update();
    }

    /**
     * 处理课程审核
     */
    private function handleCourse(array $orderData): void
    {
        if ($orderData['p_id'] === 1) {
            return; // 特殊处理，无需扣除库存
        }

        // 获取课程信息
        $courseData = Db::name('course')->find($orderData['c_id']);
        if (!$courseData) {
            throw new \Exception('课程不存在。');
        }

        // 检查并扣除库存
        $this->checkAndDeductStock($orderData['parent']['id'], 4);
    }

    /**
     * 处理活动审核
     */
    private function handleActivity(array $orderData): void
    {
        // 检查并扣除库存
        $this->checkAndDeductStock(
            $orderData['parent']['id'],
            7,
            $orderData['num']
        );

        // 生成二维码
        $qrCode = generateQrCode($orderData['image']);

        // 添加预约信息
        Db::name('member_meeting')->insertGetId([
            'qr_code' => $qrCode,
            'u_id' => $orderData['u_id'],
            'm_id' => $orderData['year'],
            'time' => $orderData['time']
        ]);
    }

    /**
     * 初始化用户库存记录
     */
    private function initializeUserStock(int $userId, int $type): void
    {
        $exists = Db::name('member_pay')
            ->where([
                'id' => $userId,
                'type' => $type
            ])
            ->find();

        if (!$exists) {
            Db::name('member_pay')->insert([
                'id' => $userId,
                'type' => $type,
                'num' => 0
            ]);
        }
    }

    /**
     * 转移库存
     */
    private function transferStock(int $fromId, int $toId, int $type, int $amount): void
    {
//        Db::name('member_pay')
//            ->where([
//                'id' => $fromId,
//                'type' => $type
//            ])
//            ->dec('num', $amount)
//            ->update();

        Db::name('member_pay')
            ->where([
                'id' => $toId,
                'type' => $type
            ])
            ->inc('num', $amount)
            ->update();
    }

    /**
     * 记录操作日志
     */
    private function logOperation(array $orderData, string $operation): void
    {
        Log::info("Order operation: {$operation}", [
            'order_id' => $orderData['id'],
            'user_id' => $orderData['u_id'],
            'type' => $orderData['type'],
            'time' => date('Y-m-d H:i:s')
        ]);
    }

	 /**
	  * Created by PhpStorm.
	  * @purpose 提交注册审核
	  * @Author: Linxy
	  * @Time: 2023/10/12 11:29
	  */
	 public function save()
	 {
         try {
             $param = $this->validateAndGetParams();

             $this->checkExistingUser($param['phone']);

             // 获取会员类型配置
             $memberConfig = $this->getMemberConfig($param['c_id']);
             if (!$memberConfig) {
                 throw new \Exception('c_id错误。');
             }

             // 验证推荐人权限
             $parentInfo = $this->validateParent((int) $param['p_id'], $memberConfig);
             if (!$parentInfo['success']) {
                 throw new \Exception($parentInfo['message']);
             }

             // 开始事务处理
             Db::startTrans();

             // 根据state参数决定用户和订单状态
             $userState = isset($param['state']) && $param['state'] == 0 ? 0 : 1;

             // 创建新用户
             $userId = $this->createNewUser($param, $userState, $parentInfo);

             // 处理会员相关数据
             $gradeData = $this->processMemberData($userId, $param, $memberConfig, $parentInfo, $userState);

             Db::commit();
             if ($param['c_id'] == 11) {
                 $code = Db::name('city_admin')->alias('t1')
                     ->where('t1.city', '=', $param['province'])
                     ->value('img');
             }else{
                 $code = '';
             }
             if ($param['c_id'] == 11 && $parentInfo['state'] == 1){
                 return json(['code' => 200, 'msg' => '申请注册已提交。', 'is_pop' => true, 'img' => $code, 'data' => ['name' => $param['name'], 'code' => $gradeData['code'], 'time' => date('Y年m月d日', time()), 'sex' => (int) $param['sex']]]);
             }
             return json(['code' => 200, 'msg' => '申请注册已提交。', 'is_pop' => false, 'img' => $code]);
	 	} catch (\Throwable $e){
	 		// 回滚事务
	 		Db::rollback();
	 		// 这是进行异常捕获
	 		return json(['code' => 201, 'msg' => $e->getMessage()]);
	 	}
	 }

    /**
     * 验证推荐人权限
     */
    private function validateParent(int $pId, array $memberConfig): array
    {
        if ($pId == 1) {
            return ['success' => true, 'state' => 1];
        }

        $parentGrades = Db::name('member_grade')
            ->where([
                ['id', '=', $pId],
                ['grade', 'in', $memberConfig['grades']]
            ])
            ->column('grade');

        if (empty($parentGrades) && $memberConfig['type'] != 0) {
            return ['success' => false, 'message' => '请联系推荐人开通产品。'];
        }

        $isDirectPass = array_intersect([3, 5, 6, 9, 13, 14, 15, 16, 0], $parentGrades);
        $state = !empty($isDirectPass) ? 1 : 0;

        return [
            'success' => true,
            'state' => $state,
            'grades' => $parentGrades
        ];
    }

    /**
     * 创建新用户
     */
    private function createNewUser(array $param, int $state, array $parentInfo): int
    {
        $number = substr(sprintf('%07s', (int)Db::name('member')->max('number') + 1), -6);
        return Db::name('member')->insertGetId([
            'name' => $param['name'],
            'phone' => $param['phone'],
            'number' => $number,
            'sex' => $param['sex'],
            'pwd' => encrypt_password('123458'),
            'state' => $parentInfo['state'],
            'time' => time(),
        ]);
    }

    /**
     * 处理会员相关数据
     */
    private function processMemberData(int $userId, array $param, array $memberConfig, array $parentInfo, int $state)
    {
        // 处理库存扣除
        //  表示他的审核人有这个身份1=有   表示消耗的是真实的库存0是假的库存    表示是扫码还是审核人添加扫码必须审核
        if ($parentInfo['state'] == 1 && $memberConfig['type'] != 0 && $param['state'] == 1) {
            $this->deductParentStock((int) $param['p_id'], $memberConfig['type']);
        }

        // 查询相关的推荐人代理
        if ($parentInfo['state'] == 0) {
            switch ($memberConfig['grade']) {
                case 1:
                    // 思享荟会员
                    $getParent = getParent365($param['p_id']);
                    break;
                case 4:
                    // 了了派会员
                    $getParent = getParentMama($param['p_id']);
                    break;
                case 7:
                    // 悦读慧会员
                    $getParent = getParent365s($param['p_id']);
                    break;
                case 10:
                    // 好妈妈会员
                    $getParent = getParentMama($param['p_id']);
                    break;
                case 12:
                    // 学员证会员
                    $getParent = getParentMama($param['p_id']);
                    break;
                default:
                    // 提交事务
                    return json(['code' => 201, 'msg' => '参数异常。']);
            }
        }else{
            $getParent = ['id' => $param['p_id']];
        }

        $param['s_id'] = $getParent['id'];

        // 创建订单
        $this->createOrder($userId, $param, $parentInfo['state']);

        // 初始化用户数据
        $gradeData = $this->initializeUserData($userId, $param, $memberConfig);

        // 添加消息提醒
        $this->createReminder($userId, $param);

        return $gradeData;
    }

    /**
     * 扣除父级库存
     */
    private function deductParentStock(int $parentId, int $type)
    {
        $num = Db::name('member_pay')->where(['id' => $parentId, 'type' => $type])->value('num');
        if ($num == 0) {
            return json(['code' => 203, 'msg' => '库存不足。']);
        }

        Db::name('member_pay')
            ->where(['id' => $parentId, 'type' => $type])
            ->dec('num')
            ->update();
    }

    /**
     * 创建订单
     */
    private function createOrder(int $userId, array $param, int $state): int
    {
        return Db::name('order')->insertGetId([
            'u_id' => $userId,
            'p_id' => $param['s_id'],
            'c_id' => $param['c_id'],
            's_id' => $param['s_id'],
            'type' => $param['type'],
            'state' => $state,
            'vip_start_time' => time(),
            'vip_end_time' => strtotime("+1 year", time()),
            'time' => time()
        ]);
    }

    /**
     * 初始化用户数据
     */
    private function initializeUserData(int $userId, array $param, array $memberConfig): array
    {
        // 添加默认库存
        Db::name('member_pay')->insertAll([
            ['id' => $userId, 'type' => 1],
            ['id' => $userId, 'type' => 2],
            ['id' => $userId, 'type' => 3],
            ['id' => $userId, 'type' => 4],
            ['id' => $userId, 'type' => 5],
            ['id' => $userId, 'type' => 6],
        ]);

        // 添加默认地址
        Db::name('member_address')->save([
            'id' => $userId,
            'p_id' => $param['province'],
            'c_id' => $param['city'],
            'd_id' => $param['area'],
            'address' => $param['address']
        ]);

        // 处理会员等级
        $gradeData = $this->createMemberGrade($userId, $memberConfig);

        // 记录用户推荐人
        Db::name('member_parent')->insert([
            'id' => $userId,
            'p_id' => $param['p_id']
        ]);
        return $gradeData;
    }

    /**
     * 创建会员等级记录
     */
    private function createMemberGrade(int $userId, array $memberConfig): array
    {
        $gradeData = [
            'id' => $userId,
            'grade' => $memberConfig['grade'],
        ];

        if ($memberConfig['grade'] == 1 || $memberConfig['grade'] == 7) {
            // 非学员证需要设置时间
            $gradeData['vip_start_time'] = time();
            $gradeData['vip_end_time'] = strtotime("+1 year", time());
        } else {
            // 好妈妈会员需要特殊处理
            $gradeData['code'] = substr(sprintf('%07s',
                (int)Db::name('member_grade')
                    ->where('grade', 10)
                    ->max('code') + 1
            ), -6);
        }

        Db::name('member_grade')->save($gradeData);
        return $gradeData;
    }

    /**
     * 创建提醒消息
     */
    private function createReminder(int $userId, array $param): void
    {
        $desc = $param['state'] == 1
            ? '你好，【'.date("Y-m-d H:i:s").'】会员'.$param['name'].'提交的注册信息，已经通过。'
            : '你好，【'.date("Y-m-d H:i:s").'】会员'.$param['name'].'提交了注册信息，快去看看吧。';
        if($param['c_id'] == 11) {
            Db::name('remind')->save([
                'u_id' => $param['p_id'],
                'c_id' => $userId,
                'r_id' => 0,
                'desc' => '您已成功开通好妈妈身份快去我的身份看看吧！',
                'type' => 5,
                'time' => time()
            ]);
        }else{
            Db::name('remind')->save([
                'u_id' => $userId,
                'c_id' => $param['p_id'],
                'r_id' => 0,
                'desc' => $desc,
                'type' => 1,
                'time' => time()
            ]);
        }
    }

    /**
     * 获取父级代理信息
     */
    private function getParentInfo(array $parentGrades, int $pId): array
    {
        if (in_array(3, $parentGrades) || in_array(0, $parentGrades)) {
            return ['id' => $pId];
        }

        if (in_array(5, $parentGrades) || in_array(6, $parentGrades)) {
            return ['id' => $pId];
        }

        if (in_array(9, $parentGrades)) {
            return ['id' => $pId];
        }

        // 根据不同会员类型获取上级代理
        if (in_array(1, $parentGrades) || in_array(3, $parentGrades)) {
            return getParent365($pId);
        } else {
            return getParentMama($pId);
        }
    }

    /**
     * 验证参数并返回处理后的参数
     */
    private function validateAndGetParams(): array
    {
        $param = input();

        // 验证码检查
        if ($param['p_id'] != 1 && env('ALY_SMS.ALY_STATE', '') !== '0') {
            if ($param['code'] !== Cache::get($param['phone'])) {
                throw new \Exception('验证码不正确。');
            }
        }
        unset($param['code']);

        return $param;
    }

    /**
     * 检查用户是否已存在
     */
    private function checkExistingUser(string $phone)
    {
        $userInfo = Db::name('member')->where('phone', $phone)->find();
        if ($userInfo) {
            $stateMessages = [
                0 => '该手机号已经申请了，快去通过吧。',
                1 => '该手机号已经注册过了请确认手机号是否正确。',
                2 => '该手机号已经注册过冻结了，快去联系客服解冻吧。',
                3 => '该手机号已经被封禁了，快去联系客服解封吧。'
            ];
            if (isset($stateMessages[$userInfo['state']])) {
                throw new \Exception($stateMessages[$userInfo['state']]);
            }
        }
        return $userInfo;
    }

    /**
     * 获取会员类型配置
     */
    private function getMemberConfig(int $cId): ?array
    {
        $configs = [
            1 => ['name' => '思享荟会员', 'grade' => 1, 'type' => 1, 'grades' => [1, 3, 13, 14, 15, 16]],
            2 => ['name' => '了了派会员', 'grade' => 4, 'type' => 2, 'grades' => [4, 5, 6, 13, 14, 15, 16]],
            3 => ['name' => '悦读慧会员', 'grade' => 7, 'type' => 3, 'grades' => [7, 9, 13, 14, 15, 16]],
            11 => ['name' => '好妈妈会员', 'grade' => 10, 'type' => 5, 'grades' => [10, 5, 6, 13, 14, 15, 16]],
            8 => ['name' => '学员证', 'grade' => 12, 'type' => 0, 'grades' => [0, 1, 3, 4, 5, 6, 7, 9, 13, 14, 15, 16, 10]]
        ];

        return $configs[$cId] ?? null;
    }

	
	/**
	 * Created by PhpStorm.
	 * @purpose 审核/发放名额列表
	 * @Author: Linxy
	 * @Time: 2023/10/12 10:39
	 */
	public function list()
	{
		$param = input();
        $userInfo = getUserInfo(true);
		$where = [];
		// 搜索
//		if (!empty($param['search'])) $where[] = ['t2.name|t2.phone', 'like', '%'.$param['search'].'%'];
		// 状态筛选
		if (!empty($param['state'])) {
            $where[] = ['t1.state', '=', $param['state']];
        }else{
            if ($param['state'] == 0) $where[] = ['t1.state', '=', $param['state']];
        }
        // 产品筛选
        if (!empty($param['c_id'])) $where[] = ['t1.c_id', '=', $param['c_id']];
        // 类型筛选
        if (!empty($param['type'])) $where[] = ['t1.type', '=', $param['type']];
		// 查询当前登录的用户需要审核的信息
		$where[] = ['t1.p_id', '=', $userInfo['id']];
		$data = Db::name('order')
			->alias('t1')
			->leftJoin('member t2', 't1.u_id = t2.id')
			->where($where)
			->field('t1.*, t2.name, t2.phone')
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.time', 'desc')
			->select()->toArray();
		$count = Db::name('order')
            ->alias('t1')
            ->leftJoin('member t2', 't1.u_id = t2.id')
            ->where($where)
			->count();
        foreach ($data as &$datum) {
            if (empty($datum['image'])) {
                $datum['img'] = [];
            }else{
                $datum['img'] = explode(',', $datum['image']);
            }
            if ($datum['type'] === 1) {
                $datum['title'] = Db::name('product')->where('id', $datum['c_id'])->value('title');
            } elseif ($datum['type'] === 2) {
                $datum['title'] = Db::name('product')->where('id', $datum['c_id'])->value('title');
            } elseif ($datum['type'] === 3) {
                $datum['title'] = Db::name('product')->where('id', $datum['c_id'])->value('title');
            } elseif ($datum['type'] === 4) {
                $datum['title'] = Db::name('product')->where('id', $datum['c_id'])->value('title');
            } elseif ($datum['type'] === 5) {
                $datum['title'] = Db::name('course')->where('id', $datum['c_id'])->value('title');
            } elseif ($datum['type'] === 6) {
                $datum['title'] = Db::name('product')->where('id', $datum['c_id'])->value('title');
            }else{
                $datum['title'] = '';
            }
        }
		return json(['code' => 200, 'msg' => '获取审核列表成功。', 'data' => $data, 'count' => $count]);
	}
}
