<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Db;

class City
{
    /**
     * @Description: 查询全部地址
     * @Author: 林怼怼
     * @Date: 2024/12/10
     * @Time: 17:39
     */
    public function cityAllList()
    {
        $p = Db::name('city')->where('depth', 1)->column('id, cityName');
        $c = Db::name('city')->where('depth', 2)->column('id, cityName');
        $q = Db::name('city')->where('depth', 3)->column('id, cityName');
        $data = [];
        foreach ($p as &$item) {
            $data['province_list'][$item['id']] = $item['cityName'];
        }
        foreach ($c as &$item) {
            $data['city_list'][$item['id']] = $item['cityName'];
        }
        foreach ($q as &$item) {
            $data['county_list'][$item['id']] = $item['cityName'];
        }
        return json(['code' => 200, 'msg' => '操作成功。', 'data' => $data]);
    }

    /**
     * @Description: 地址删除
     * @Author: 林怼怼
     * @Date: 2024/3/6/006
     * @Time: 9:48
     */
    public function cityDel()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            $count = Db::name('member_address')->where('id', $userInfo['id'])->count();
            if ($count === 1) return json(['code' => 201, 'msg' => '最少保留一个地址。']);
            Db::name('member_address')->where('a_id', $param['id'])->delete();
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 地址修改
     * @Author: 林怼怼
     * @Date: 2024/3/6/006
     * @Time: 9:48
     */
    public function cityEdit()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            if ($param['is_master'] == 1) {
                cityCorrect($userInfo['id']);
            }
            Db::name('member_address')->where('a_id', $param['a_id'])->save([
                'id' => $userInfo['id'],
                'p_id' => $param['p_id'],
                'c_id' => $param['c_id'],
                'd_id' => $param['d_id'],
                'address' => $param['address'],
                'is_master' => $param['is_master']
            ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 添加地址信息
     * @Author: 林怼怼
     * @Date: 2024/3/5/005
     * @Time: 17:48
     */
    public function citySave()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            if ($param['is_master'] == 1) {
                cityCorrect($userInfo['id']);
            }
            Db::name('member_address')->save([
                'id' => $userInfo['id'],
                'p_id' => $param['p_id'],
                'c_id' => $param['c_id'],
                'd_id' => $param['d_id'],
                'address' => $param['address'],
                'is_master' => $param['is_master']
            ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 用户地址列表
     * @Author: 林怼怼
     * @Date: 2024/3/5/005
     * @Time: 17:34
     */
    public function cityList()
    {
        $userInfo = getUserInfo(true);
        $data = Db::name('member_address')
            ->alias('t1')
            ->join('city t2', 't1.p_id = t2.id')
            ->join('city t3', 't1.c_id = t3.id')
            ->join('city t4', 't1.d_id = t4.id')
            ->where(['t1.id' => $userInfo['id']])
            ->field('t1.*, t2.cityName as cityName1, t3.cityName as cityName2, t4.cityName as cityName3')
            ->select()->toArray();
        $count = Db::name('member_address')
            ->alias('t1')
            ->join('city t2', 't1.p_id = t2.id')
            ->join('city t3', 't1.c_id = t3.id')
            ->join('city t4', 't1.d_id = t4.id')
            ->where(['t1.id' => $userInfo['id']])
            ->field('t1.*, t2.cityName as cityName1, t3.cityName as cityName2, t4.cityName as cityName3')
            ->count();
        return json(['code' => 200, 'msg' => '操作成功。', 'data' => $data, 'count' => $count]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 地址列表
	 * @Author: Linxy
	 * @Time: 2023/10/11 11:21
	 */
	public function list()
	{
		$id = input('id') === '' ? 100000 : input('id', 0);
        $data = Db::name('city')
            ->where('parentId', $id)
            ->order('id', 'asc')
            ->field('id, cityName as name')
            ->select()
            ->toArray();
		return json(['code' => 200, 'msg' => '地址列表。', 'data' => $data]);
	}
}
