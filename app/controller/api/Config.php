<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Db;
use think\Request;

class Config
{
	/**
	 * Created by PhpStorm.
	 * @purpose 查询配置
	 * @Author: Linxy
	 * @Time: 2023/10/23 14:53
	 */
	public function read()
	{
		$param = input();
		$data = Db::name('config')->where(['state' => 1])->find($param['id']);
		return json(['code' => 200, 'msg' => '查询配置成功。', 'data' => $data]);
	}
}
