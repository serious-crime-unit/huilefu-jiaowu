<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Cache;
use think\facade\Db;

class Meeting
{
    /**
     * @Description: 用户评价列表
     * @Author: 林怼怼
     * @Date: 2024/12/13
     * @Time: 14:15
     */
    public function getUserEvaluate()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $where = [];
        $where[] = ['t1.u_id', '=', $userInfo['id']];
        if (!empty($param['type'])) $where[] = ['t1.type', '=', $param['type']];
        $data = Db::name('evaluate')
            ->alias('t1')
            ->leftJoin('support t2', 't1.id = t2.p_id')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->field('t1.id, t1.u_id, t1.p_id, t1.desc, t1.img, t1.score, t1.type, t1.state, t1.time, IFNULL(COUNT(t2.id), 0) as support')
            ->group('t1.id')
//            ->order('support DESC, t1.time DESC')
            ->order('t1.time DESC')
            ->select()->toArray();
        $count = Db::name('evaluate')
            ->alias('t1')
            ->leftJoin('support t2', 't1.id = t2.p_id')
            ->where($where)
            ->group('t1.id')
            ->count();
        foreach ($data as &$datum) {
            $datum['img'] = explode(',', $datum['img']);
            $userName = Db::name('member')->where('id', $datum['u_id'])->find();
            $datum['userName'] = $userName['name'];
            $datum['avatar'] = $userName['avatar'];
            $datum['title'] = '';
            $datum['count'] = 0;
            $datum['is_sup'] = 2;
            $datum['is_del'] = 2;
            $datum['replyToUserName'] = '';
            $sup = Db::name('support')->where(['u_id' => $userInfo['id'], 'p_id' => $datum['id']])->count();
            if ($sup != 0) $datum['is_sup'] = 1;
            if ($userInfo['id'] == $datum['u_id']) $datum['is_del'] = 1;
            if ($userInfo['id'] == 1) $datum['is_del'] = 1;
            if ($datum['type'] == 1 || $datum['type'] == 2) {
                $datum['title'] = Db::name('meeting')->where(['id' => $datum['p_id']])->value('name');
                if ($datum['type'] == 1) {
                    $datum['count'] = Db::name('evaluate')->where([
                        ['id', 'in', getAllCommentIDs($datum['id'])],
                        ['type', '=', 3],
                        ['state', '=', 1]
                    ])->count();
                }else{
                    $datum['count'] = Db::name('evaluate')->where([
                        ['id', 'in', getAllCommentIDsjs($datum['id'])],
                        ['type', '=', 6],
                        ['state', '=', 1]
                    ])->count();
                }
            }
            if ($datum['type'] == 3 || $datum['type'] == 6) {
                $u_id = Db::name('evaluate')->where('id', $datum['p_id'])->value('u_id');
                if ($datum['p_id'] != $param['id']) $datum['replyToUserName'] = Db::name('member')->where('id', $u_id)->value('name');
            }
        }
        return json(['code' => 200, 'msg' => '评论查询成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 评价活动详情
     * @Author: 林怼怼
     * @Date: 2024/12/13
     * @Time: 14:00
     */
    public function getMeetingFist()
    {
        $param = input();
        $data = Db::name('meeting_city')->where(['id' => $param['id']])->field('id, p_id, userId, userName')->find();
        $meeting = Db::name('meeting')->where(['id' => $data['p_id']])->find();
        $meeting = Db::name('meeting')->where(['id' => $meeting['p_id']])->find();
        $data['p_id'] = $meeting['id'];
        $data['title'] = $meeting['name'];
        $data['pinyin'] = $meeting['pinyin'];
        $data['image'] = $meeting['img'];
        $data['desc'] = $meeting['desc'];
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data]);
    }

    /**
     * @Description: 会员预约详细
     * @Author: 林怼怼
     * @Date: 2024/12/11
     * @Time: 14:50
     */
    public function getEventFirst()
    {
        $param = input();
        $data = Db::name('member_meeting')->where(['mobile' => $param['mobile'], 'm_id' => $param['id']])->find();
        $res = Db::name('meeting_city')->where(['id' => $data['m_id']])->find();
        $result = Db::name('meeting')->where(['id' => $res['p_id']])->find();
        $result2 = Db::name('meeting')->where(['id' => $result['p_id']])->find();
        $data['address'] = $res['address'];
        $data['city'] = $res['city'];
        $data['start_time'] = $res['start_time'];
        $data['end_time'] = $res['end_time'];
        $data['userName'] = $res['userName'];
        $data['userPhone'] = $res['userPhone'];
        $data['name'] = $result['name'];
        $data['title'] = $result2['name'];
        $data['image'] = $result2['img'];
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data]);
    }

    /**
     * @Description: 用户删除评论
     * @Author: 林怼怼
     * @Date: 2024/12/6
     * @Time: 12:37
     */
    public function delComment()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            Db::name('evaluate')->delete($param['id']);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '删除评论成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 评论点赞/取消
     * @Author: 林怼怼
     * @Date: 2024/12/4
     * @Time: 10:01
     */
    public function support()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($param['state'] == 1) {
            // 点赞
            $result = Db::name('support')
                ->save([
                    'u_id' => $userInfo['id'],
                    'p_id' => $param['id'],
                    'pandn' => 1,
                    'time' => time()
                ]);
            return json(['code' => 200, 'msg' => '点赞成功。']);
        }else{
            // 取消点赞
            $result = Db::name('support')
                ->where(['u_id' => $userInfo['id'], 'p_id' => $param['id']])
                ->delete();
            return json(['code' => 200, 'msg' => '取消点赞。']);
        }
    }

    /**
     * @Description: 听课结束评价
     * @Author: 林怼怼
     * @Date: 2024/12/6
     * @Time: 09:49
     */
    public function saveEvaluates()
    {
        $param = input();
        $js_id = input('js_id', '');
        $js_desc = input('js_desc', '');
        $js_score = input('js_score', 0);
        $js_img = input('js_img', '');
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            // 评价
                $id = Db::name('evaluate')
                    ->insertGetId([
                        'u_id' => $userInfo['id'],
                        'p_id' => $param['p_id'],
                        'desc' => $param['desc'],
                        'img' => $param['img'],
                        'score' => $param['score'],
                        'type' => 1,
                        'state' => 1,
                        'time' => time()
                    ]);
                $js_id = Db::name('evaluate')
                    ->insertGetId([
                        'u_id' => $userInfo['id'],
                        'p_id' => $js_id,
                        'desc' => $js_desc,
                        'img' => $js_img,
                        'score' => $js_score,
                        'type' => 2,
                        'state' => 1,
                        'time' => time()
                    ]);
                Db::name('member_meeting')->where(['mobile' => $userInfo['phone'], 'm_id' => $param['id']])->update(['complete' => 3]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '发布成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 发布评论内容
     * @Author: 林怼怼
     * @Date: 2024/12/3
     * @Time: 14:59
     */
    public function saveEvaluate()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 启动事务
        Db::startTrans();
        try {
            $parent_comment_id = $param['parent_comment_id'];
            $data = [];
            $data['type'] = $param['type'];
            $data['userName'] = $userInfo['name'];
            $data['avatar'] = $userInfo['avatar'];
            $data['u_id'] = $userInfo['id'];
            $data['p_id'] = $param['p_id'];
            $data['parent_comment_id'] = $param['parent_comment_id'];
            $data['desc'] = $param['desc'];
            $data['img'] = $param['img'];
            $data['score'] = $param['score'];
            $data['is_sup'] = 2;
            $data['count'] = 0;
            $data['support'] = 0;
            $data['state'] = 1;
            $data['title'] = '';
            $data['time'] = time();
            $id = Db::name('evaluate')
                    ->insertGetId([
                        'u_id' => $userInfo['id'],
                        'p_id' => $param['p_id'],
                        'parent_comment_id' => $parent_comment_id,
                        'desc' => $param['desc'],
                        'img' => $param['img'],
                        'score' => $param['score'],
                        'type' => $param['type'],
                        'state' => 1,
                        'time' => time()
                    ]);
            if ($param['type'] == 1) {
                $data['title'] = Db::name('meeting')->where(['id' => $param['p_id']])->value('name');
            }
            $data['id'] = $id;
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '发布成功。', 'data' => $data]);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 精彩时刻列表
     * @Author: 林怼怼
     * @Date: 2024/12/2
     * @Time: 15:58
     */
    public function getMomentsList()
    {
        $param = input();
        $where = [];
        if ($param['type'] == 3 || $param['type'] == 6) {
            if ($param['type'] == 3) $where[] = ['t1.id', 'in', getAllCommentIDs($param['id'])];
            if ($param['type'] == 6) $where[] = ['t1.id', 'in', getAllCommentIDsjs($param['id'])];
        }elseif($param['type'] == 1 || $param['type'] == 4){
            $where[] = ['t1.p_id', 'in', getAllCommentMeeting($param['id'])];
        }else{
            $where[] = ['t1.p_id', '=', $param['id']];
        }
        $where[] = ['t1.type', '=', $param['type']];
        $where[] = ['t1.state', '=', 1];
        $data = Db::name('evaluate')
            ->alias('t1')
            ->leftJoin('support t2', 't1.id = t2.p_id')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->field('t1.id, t1.u_id, t1.p_id, t1.desc, t1.img, t1.poster, t1.score, t1.is_type, t1.type, t1.state, t1.time, IFNULL(COUNT(t2.id), 0) as support')
            ->group('t1.id')
//            ->order('support DESC, t1.time DESC')
            ->order('t1.time DESC')
            ->select()->toArray();
        $count = Db::name('evaluate')
            ->alias('t1')
            ->leftJoin('support t2', 't1.id = t2.p_id')
            ->where($where)
            ->group('t1.id')
            ->count();
        foreach ($data as &$datum) {
            $datum['img'] = explode(',', $datum['img']);
            $userName = Db::name('member')->where('id', $datum['u_id'])->find();
            $datum['userName'] = $userName['name'];
            $datum['avatar'] = $userName['avatar'];
            $datum['title'] = '';
            $datum['count'] = 0;
            $datum['is_sup'] = 2;
            $datum['is_del'] = 2;
            $datum['replyToUserName'] = '';
            $header = request()->header();
            if (isset($header['authorization']) && $header['authorization'] != null && !empty($header['authorization'])) {
                $getUserInfo = getUserInfo(true);
                if ($getUserInfo['code'] != 200) return json($getUserInfo);
                $sup = Db::name('support')->where(['u_id' => $getUserInfo['id'], 'p_id' => $datum['id']])->count();
                if ($sup != 0) $datum['is_sup'] = 1;
                if ($getUserInfo['id'] == $datum['u_id']) $datum['is_del'] = 1;
                if ($getUserInfo['id'] == 1) $datum['is_del'] = 1;
            }
            if ($datum['type'] == 1 || $datum['type'] == 2) {
                $datum['title'] = Db::name('meeting')->where(['id' => $datum['p_id']])->value('name');
                if ($datum['type'] == 1) {
                    $datum['count'] = Db::name('evaluate')->where([
                        ['id', 'in', getAllCommentIDs($datum['id'])],
                        ['type', '=', 3],
                        ['state', '=', 1]
                    ])->count();
                }else{
                    $datum['count'] = Db::name('evaluate')->where([
                        ['id', 'in', getAllCommentIDsjs($datum['id'])],
                        ['type', '=', 6],
                        ['state', '=', 1]
                    ])->count();
                }
            } elseif ($datum['type'] == 3 || $datum['type'] == 6) {
                $u_id = Db::name('evaluate')->where('id', $datum['p_id'])->value('u_id');
                if ($datum['p_id'] != $param['id']) $datum['replyToUserName'] = Db::name('member')->where('id', $u_id)->value('name');
            }
        }
        return json(['code' => 200, 'msg' => '评论查询成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 查询地址详情
     * @Author: 林怼怼
     * @Date: 2024/7/8/008
     * @Time: 17:00
     */
    public function getMeetingCityDetail()
    {
        $param = input();
        $data = Db::name('meeting_city')
            ->alias('t1')
            ->join('meeting t2', 't1.p_id = t2.id')
            ->where('t1.id', $param['id'])
            ->field('t1.*, t2.name, t2.img, t2.grade')
            ->find();
        $data['grade'] = explode(',', $data['grade']);
        return json(['code' => 200, 'msg' => '地址详情查询成功。', 'data' => $data]);
    }

    /**
     * @Description: 活动地址列表
     * @Author: 林怼怼
     * @Date: 2024/7/4/004
     * @Time: 12:49
     */
    public function getMeetingCityList()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $where = [];
//        $where[] = ['t1.end_time', '>', time()];
        // 状态筛选
        $where[] = ['p_id', '=', $param['id']];
        $data = Db::name('meeting_city')
            ->alias('t1')
            ->field('t1.id, t1.userName, t1.userPhone, t1.city, t1.start_time, t1.end_time, t1.time')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.start_time', 'desc')
            ->select()
            ->toArray();
        $count = Db::name('meeting_city')
            ->alias('t1')
            ->field('t1.id, t1.userName, t1.userPhone, t1.city, t1.start_time, t1.end_time, t1.time')
            ->where($where)
            ->count();
        $number = 0;
        foreach ($data as $key => &$val) {
            // 查看下级参加活动得人
            $result = Db::name('member_meeting')
                ->alias('t1')
                ->join('member t2', 't1.u_id = t2.id')
                ->where([
                    ['t1.m_id', '=', $val['id']]
                ])
                ->field('t2.id, t2.name, t2.avatar')
                ->select()->toArray();
            $val['getChildUser'] = [];
            if (!empty($result)) {
                $val['getChildUser'] = $result;
            }
            $val['num'] = Db::name('member_meeting')->where(['m_id' => $val['id']])->count();
            $num = Db::name('member_meeting')->where(['u_id' => $userInfo['id'], 'm_id' => $val['id']])->count();
            $val['type'] = $num !== 0 ? true : false;
            $val['complete'] = 1;
            if ($val['type']) {
                $val['complete'] = Db::name('member_meeting')->where(['u_id' => $userInfo['id'], 'm_id' => $val['id']])->find()['complete'];
            }
            $val['num1'] = Db::name('member_meeting')->where(['complete' => 2, 'm_id' => $val['id']])->count();
            $val['is_over'] = false;
            if ($val['end_time'] + 86400 <= time()) {
                $val['is_over'] = true;
                unset($data[$key]);
            }
            $number += $val['num1'];
        }
        $data = array_values($data);
        return json(['code' => 200, 'msg' => '查询活动地址成功。', 'data' => $data, 'count' => $count, 'num' => $number]);
    }

    /**
     * Created by PhpStorm.
     * @purpose 总部查看全部活动
     * @Author: Linxy
     * @Time: 2023/10/18 16:51
     */
    public function getMeetingList()
    {
        $param = input();
        $where = [];
        $userInfo = getUserInfo(true);
        // 搜索活动名字或者发起人名字
        $where[] = ['t1.name|t2.name', 'like', '%'.$param['search'].'%'];
        // 状态筛选
        if (!empty($param['state'])) $where[] = ['t1.state', '=', $param['state']];
        $query = Db::name('meeting')
            ->alias('t1')
            ->leftJoin('member t2', 't1.u_id = t2.id')
            ->field('t1.id, t1.userName, t1.userPhone, t1.address, t1.name, t1.img, t1.state, t1.start_time, t1.end_time, t1.time')
            ->where($where);
        // 预查询总数，优化分页
        $total_count = (clone $query)->count();
        $data = $query->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.start_time', 'desc')
            ->select()
            ->toArray();
        foreach ($data as &$val) {
            $val['img'] = explode(',', $val['img']);
            $dataMax = Db::name('meeting_city')
                ->where('p_id', '=', $val['id'])
                ->max('end_time');
            if ($dataMax > 100) {
                if ((int) $dataMax <= time()) {
                    Db::name('meeting')->where('id', $val['id'])->update(['state' => 3]);
                    $val['state'] = 3;
                }
            }
            $val['num'] = Db::name('meeting_city')
                ->where('p_id', '=', $val['id'])
                ->count();
        }
        return json(['code' => 200, 'msg' => '查询活动列表成功。', 'data' => $data, 'count' => $total_count]);
    }

    /**
     *
     * @Description: 会员预约列表（会员参加的活动）
     * @Author: Linxy
     * @Date: 2023/12/5/005
     * @Time: 15:23
     */
    public function getEventList()
    {
        $param = input();
        if ($param['id'] == 0)
        {
            $userInfo = getUserInfo(true);
        }else{
            $userInfo = Db::name('member')->find($param['id']);
        }
        $where = [];
        $where[] = ['t1.mobile', '=', $userInfo['phone']];
        $data = Db::name('member_meeting')
            ->alias('t1')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->field('t1.id, t1.qr_code, t1.m_id, t1.mobile, t1.complete, t1.time')
            ->order('t1.id', 'desc')
            ->select()->toArray();
        foreach ($data as &$datum)
        {
            $cityName = Db::name('meeting_city')->find($datum['m_id']);
            $meeting = Db::name('meeting')
                ->alias('t1')
                ->join('meeting t2', 't1.p_id = t2.id')
                ->where('t1.id', $cityName['p_id'])
                ->field('t2.id, t1.name, t2.name as title, t2.img')
                ->find();
            $datum['cityName'] = $cityName['city'];
            $datum['userName'] = $cityName['userName'];
            $datum['start_time'] = $cityName['start_time'];
            $datum['end_time'] = $cityName['end_time'];
            $datum['address'] = $cityName['address'];
            $datum['type'] = 2;
            if ($cityName['start_time'] > time()) $datum['type'] = 2; // 未开始
            if ($cityName['end_time'] > time() && $cityName['start_time'] < time()) $datum['type'] = 1; // 进行中
            if ($cityName['end_time'] < time() && $cityName['start_time'] < time()) $datum['type'] = 0; // 已结束
            $datum['name'] = $meeting['name'];
            $datum['title'] = $meeting['title'];
            $datum['img'] = explode(',', $meeting['img']);
            $datum['c_id'] = $meeting['id'];
//            // 是否评价
//            $result = Db::name('evaluate')
//            ->where([
//                ['u_id', '=', $userInfo['id']],
//                ['p_id', '=', $datum['m_id']],
//                ['type', '=', 1],
//                ['state', '=', 1],
//            ])->count();
//            if ($result != 0) $datum['complete'] = 3;
        }
        $count = Db::name('member_meeting')
            ->alias('t1')
            ->where($where)
            ->field('t1.id, t1.qr_code, t1.m_id, t1.mobile, t1.complete')
            ->order('t1.id', 'desc')
            ->count();
        return json(['code' => 200, 'msg' => '当前会员预约列表获取成功。', 'data' => $data, 'count' => $count]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 讲师/总部查询活动列表
	 * @Author: Linxy
	 * @Time: 2023/11/29 10:48
	 */
	public function getUserMeetingList()
	{
		$param = input();
        $where = [];
        if (!empty($param['type'])) $where[] = ['t1.complete', 'in', $param['type']];
        $where[] = ['t1.mobile', 'like', '%'.$param['mobile'].'%'];
        $where[] = ['t1.m_id', '=', $param['id']];
        $data = Db::name('member_meeting')
            ->alias('t1')
            ->leftJoin('member t2', 't1.mobile = t2.phone')
            ->where($where)
            ->distinct(true)
            ->field('t1.*, t1.name')
            ->select();
        $count = Db::name('member_meeting')
            ->alias('t1')
            ->leftJoin('member t2', 't1.mobile = t2.phone')
            ->where($where)
            ->distinct(true)
            ->count();
        return json(['code' => 200, 'msg' => '查询已参加人数。', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 修改为已读
	 * @Author: Linxy
	 * @Time: 2023/11/28 17:46
	 */
	public function update()
	{
		$params = input();
		$userInfo = getUserInfo(true);
		$result = Db::name('meeting_user')
			->where(['id' => $params['id'], 'u_id' => $userInfo['id']])
			->update(['state' => 2]);
		return json(['code' => 200, 'msg' => '完成已读。']);
	}

    /**
     * Created by PhpStorm.
     * @purpose 实时提醒接口
     * @Author: Linxy
     * @Time: 2023/11/3 10:52
     */
    public static function warn($id)
    {
        $data = Db::name('meeting')
            ->alias('a')
            ->where(['a.id' => $id])
            ->leftJoin('meeting_user b', 'a.id = b.id')
            ->fieldRaw('a.*, GROUP_CONCAT(b.u_id, "") as u_ids')
            ->find();
        $data['userInfo'] = Db::name('member')
            ->whereIn('id', $data['u_ids'])
            ->field('id, name, avatar')
            ->select()
            ->toArray();
        return $data;
    }

    /**
     * @Description: 扫码活动信息
     * @Author: 林怼怼
     * @Date: 2024/10/31
     * @Time: 09:41
     */
    public function verify_qr_user()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $data = Db::name('meeting_city')->find($param['id']);
        if ($userInfo['id'] !== $data['userId']) return json(['code' => 400, 'msg' => '用户无权限审核。']);
        $result = [];
        $result['name'] = '游客';
        $user = Db::name('member')
            ->where('phone', $param['mobile'])
            ->find();
        if ($user) $result['name'] = $user['name'];
        $memberCity = Db::name('meeting_city')->find($param['id']);
        $result['mobile'] = $param['mobile'];
        $result['city'] = $memberCity['city'];
        $result['userName'] = $memberCity['userName'];
        $result['userPhone'] = $memberCity['userPhone'];
        $meeting = Db::name('meeting')->find($memberCity['p_id']);
        $result['title1'] = $meeting['name'];
        $meeting = Db::name('meeting')->find($meeting['p_id']);
        $result['title'] = $meeting['name'];
        return json(['code' => 200, 'msg' => '参加完成。', 'data' => $result]);
    }

    /**
     *
     * @Description: 扫描验证码时审核
     * @Author: Linxy
     * @Date: 2023/12/4/004
     * @Time: 17:24
     *
     */
    public function verify_qr_code()
    {
        $id = input('id', 0);
        $mobile = input('mobile', 0);
        $data = Db::name('member_meeting')
            ->where(['m_id' => $id, 'mobile' => $mobile])
            ->find();
        if ($data['complete'] === 2) return json(['code' => 201, 'msg' => '该用户二维码已经参加过了。']);
        // 修改状态为参加
        Db::name('member_meeting')
            ->where(['m_id' => $id, 'mobile' => $mobile])
            ->update(['complete' => 2]);
        return json(['code' => 200, 'msg' => '参加完成。']);
    }

    /**
     * @Description: 用户取消预约
     * @Author: 林怼怼
     * @Date: 2024/4/10/010
     * @Time: 14:11
     */
    public function getMeetingEvent()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        // 查看活动是否结束
        $meetingCity = Db::name('meeting_city')->where(['id' => $param['id']])->find();
        if ($meetingCity['end_time']+86400 <= time()) return json(['code' => 201, 'msg' => '活动已结束。']);
        // 子查询
        $p_id = Db::name('meeting')->where('id', $meetingCity['p_id'])->value('p_id');
        $meeting = Db::name('meeting')
            ->alias('t1')
            ->field('t1.id, t1.name, t1.grade')
            ->find($p_id);
        // 启动事务
        Db::startTrans();
        try {
            Db::name('member_meeting')
                ->where([
                    ['m_id', '=', $param['id']],
                    ['mobile', '=', $userInfo['phone']],
                ])->delete();
            // 添加消息提醒
            remind([
                'u_id' => $userInfo['id'],
                'c_id' =>  Db::name('member_parent')->where('id', $userInfo['id'])->value('p_id'),
                'r_id' => 0,
                'desc' => $userInfo['name'].'取消预约活动['.$meeting['name'].'-'.$meetingCity['city'].']',
                'type' => 1,
            ]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '取消成功。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 预约活动
	 * @Author: Linxy
	 * @Time: 2023/11/27 15:40
	 */
	public function reserveEvent()
	{
        $param = input();
        $name = $param['name'];
        // 查看验证码是否正确
        $header = request()->header();
        if (isset($header['authorization']) && $header['authorization'] == null && empty($header['authorization'])) {
            $code = Cache::get($param['mobile']);
            if ($code != $param['code']) return json(['code' => 204, 'msg' => '验证码不正确。']);
            $userInfoCount = Db::name('member')->where('phone', $param['mobile'])->count();
            // 查看是否注册0代表没注册是游客 自动注册
            if ($userInfoCount == 0) {
                $name = '游客-'.$param['name'];
                // 启动事务
                Db::startTrans();
                try {
                    // 生成编号
                    $number = substr(sprintf('%07s', (int)Db::name('member')->max('number') + 1), -6);
                    // 添加用户数据
                    $id = Db::name('member')->insertGetId([
                        'name' => $name,
                        'phone' => $param['mobile'],
                        'number' => $number,
                        'pwd' => encrypt_password('123458'),
                        'state' => 1,
                        'time' => time(),
                    ]);
                    // 添加订单信息
                    $cid = Db::name('order')->insertGetId([
                        'u_id' => $id,
                        'p_id' => $param['u_id'],
                        'c_id' => 8,
                        'type' => 1,
                        'state' => 1,
                        'time' => time()
                    ]);
                    // 添加默认库存
                    Db::name('member_pay')->insertAll([
                        ['id' => $id, 'type' => 1],
                        ['id' => $id, 'type' => 2],
                        ['id' => $id, 'type' => 3],
                        ['id' => $id, 'type' => 4],
                        ['id' => $id, 'type' => 6],
                    ]);
                    // 添加默认地址
                    Db::name('member_address')->save([
                        'id' => $id,
                        'p_id' => 110000,
                        'c_id' => 110100,
                        'd_id' => 110114,
                        'address' => 'TBD云集中心'
                    ]);
                    // 记录用户等级
                    Db::name('member_grade')->save([
                        'id' => $id,
                        'grade' => 12,
                    ]);
                    // 记录用户推荐人
                    Db::name('member_parent')->insert([
                        'id' => $id,
                        'p_id' => $param['u_id']
                    ]);
                    // 添加消息提醒
                    Db::name('remind')->save([
                        'u_id' => $id,
                        'c_id' => $param['u_id'],
                        'r_id' => 0,
                        'desc' => '你好，【'.date("Y-m-d H:i:s").'】游客'.$param['name'].'已经注册，活动注册默认游客。',
                        'type' => 1,
                        'time' => time()
                    ]);
                    // 提交事务
                    Db::commit();
                } catch (\Throwable $e){
                    // 回滚事务
                    Db::rollback();
                    // 这是进行异常捕获
                    return json(['code' => 203, 'msg' => $e->getMessage()]);
                }
            }
        }
        // 查看活动是否结束
        $meetingCity = Db::name('meeting_city')->where(['id' => $param['id']])->find();
        if ($meetingCity['end_time']+86400 <= time()) return json(['code' => 201, 'msg' => '活动已结束。']);
        // 验证身份是否可以预约
        // 子查询
        $meeting2 = Db::name('meeting')->where('id', $meetingCity['p_id'])->find();
        $meeting = Db::name('meeting')
            ->alias('t1')
            ->field('t1.id, t1.name, t1.grade')
            ->find($meeting2['p_id']);

        // 验证是否预约过一阶段
        if ($meeting2['sort'] > 1)
        {
            // 代表报名的是2阶验证是否参加一阶段
            // 查询一阶段ID
            $meeting_id = Db::name('meeting')
                ->where(['p_id' => $meeting['id'], 'sort' => $meeting2['sort']-1])
                ->value('id');
            // 使用一阶段id查询所以一阶段的课
            $member_meeting_ids = Db::name('meeting_city')
                ->where(['p_id' => $meeting_id])
                ->column('id');
            // 使用课id查询手机号是否参加过
            $count = Db::name('member_meeting')
                ->where([
                    ['m_id', 'in', $member_meeting_ids],
                    ['mobile', '=', $param['mobile']],
                    ['complete', '=', 2]
                ])
                ->count();
            // 只要count 等于0不可以报名
            if ($count == 0) return json(['code' => 202, 'msg' => '对不起，您暂不符合报名条件。']);
        }
        if (strpos($meeting['grade'], '0'))
        {
            $user = Db::name('member')
                ->where('phone', $param['mobile'])
                ->find();
            $grade = Db::name('member_grade')
                ->where('id', $user['id'])
                ->column('grade');
            $bool = false;
            foreach ($grade as $value)
            {
                if (strpos($meeting['grade'], (string) $value) !== false) $bool = true;
            }
            if (!$bool) return json(['code' => 201, 'msg' => '请先开通对应身份。']);
        }
        // 是否预约过该活动
        $log = Db::name('member_meeting')
            ->where(['mobile' => $param['mobile'], 'm_id' => $param['id']])
            ->find();
        if ($log) {
            $qr_code = generateQrCode($param['url']);
            $data = [
                'id' => $param['id'],
                'qr_code' => $qr_code,
                'city' => $meetingCity['city'],
                'start_time' => $meetingCity['start_time'],
                'end_time' => $meetingCity['end_time'],
                'userName' => $meetingCity['userName'],
                'userPhone' => $meetingCity['userPhone'],
                'address' => $meetingCity['address'],
                'name' => $meeting2['name'],
                'title' => $meeting['name'],
            ];
            return json(['code' => 205, 'msg' => '活动已经预约过了。', 'data' => $data]);
        }

        // 启动事务
        Db::startTrans();
        try {
            $qr_code = generateQrCode($param['url']);
            // 审核人
            if ($param['u_id'] == 0) {
                $parent = Db::name('member')
                    ->where('phone', $param['mobile'])
                    ->find();
                $userInfo = Db::name('member')
                    ->alias('t1')
                    ->join('member_parent t2', 't1.id = t2.p_id')
                    ->where('t2.id', $parent['id'])
                    ->field('t1.id, t1.name, t1.avatar, t1.phone')
                    ->find();
                // 添加消息提醒
                remind([
                    'u_id' => 0,
                    'c_id' => $parent['id'],
                    'r_id' => 0,
                    'desc' => '用户电话:'.$param['mobile'].'成功预约活动['.$meeting['name'].'-'.$meetingCity['city'].']',
                    'type' => 3,
                ]);
            }else{
                $userInfo = Db::name('member')
                    ->alias('t1')
                    ->where(['t1.id' => $param['u_id']])
                    ->field('t1.id, t1.name, t1.avatar, t1.phone')
                    ->find();
                // 添加消息提醒
                remind([
                    'u_id' => 0,
                    'c_id' => $param['u_id'],
                    'r_id' => 0,
                    'desc' => '游客电话:'.$param['mobile'].'成功预约活动['.$meeting['name'].'-'.$meetingCity['city'].']',
                    'type' => 3,
                ]);
            }
            // 添加预约信息
            Db::name('member_meeting')->insertGetId([
                'qr_code' => $qr_code,
                'u_id' => 0,
                'm_id' => $param['id'],
                'name' => $name,
                'mobile' => $param['mobile'],
                'time' => $meetingCity['start_time']
            ]);
            $data = [
                'id' => $param['id'],
                'qr_code' => $qr_code,
                'city' => $meetingCity['city'],
                'start_time' => $meetingCity['start_time'],
                'end_time' => $meetingCity['end_time'],
                'userName' => $meetingCity['userName'],
                'userPhone' => $meetingCity['userPhone'],
                'address' => $meetingCity['address'],
                'name' => $meeting2['name'],
                'title' => $meeting['name'],
            ];
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '预约成功。', 'data' => $data]);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            // 这是进行异常捕获
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
	}

    /**
     * @Description: 讲师地址筛选
     * @Author: 林怼怼
     * @Date: 2024/10/29
     * @Time: 11:19
     */
    public function getCityList()
    {
        $param = input();
        $where = [];
        $result = Db::name('meeting_city')->distinct(true)->column('city');
        return json(['code' => 200, 'msg' => '查询可报名讲师列表。', 'data' => $result]);
    }

    /**
     * @Description: 获取活动筛选
     * @Author: 林怼怼
     * @Date: 2024/11/11
     * @Time: 10:51
     */
    public function getCityChild()
    {
        $param = input();
        $data = Db::name('meeting')->where('p_id', $param['id'])->field('id, img, name, content, desc')->select();
        return json(['code' => 200, 'msg' => '查询可报名讲师列表。', 'data' => $data]);
    }

    /**
     * @Description: 查询所有讲师列表
     * @Author: 林怼怼
     * @Date: 2024/10/29
     * @Time: 10:17
     */
//    public function getCityTeacher()
//    {
//        $param = input();
//        $header = request()->header();
//        if (!isset($header['authorization']) || $header['authorization'] == null) {
//            $getUserInfo = ['code' => 200, 'phone' => $param['mobile']];
//            if (empty($param['mobile'])) $getUserInfo['code'] = 401;
//        }else{
//            $getUserInfo = getUserInfo(true);
//        }
//        $where = [];
//        // 讲师筛选
//        if (!empty($param['id'])) $where[] = ['t1.userId', '=', $param['id']];
//        // 活动筛选
//        if (!empty($param['m_id'])) $where[] = ['t3.id', '=', $param['m_id']];
//        // 地区搜索
//        $where[] = ['t1.city|t1.userName|t1.address', 'like', '%'.$param['search'].'%'];
//        // 地址筛选
//        if (!empty($param['city'])) $where[] = ['t1.city|t1.address', 'like', '%'.$param['city'].'%'];
//        // 可报名场次
//        if(!empty($param['p_id'])) $where[] = ['t1.p_id', 'in', $param['p_id']];
//        // 时间查询
//        $start_time = empty($param['start_time']) ? '' : $param['start_time'] .' 00:00:01';
//        $end_time = empty($param['end_time']) ? '' : $param['end_time'] .' 23:59:59';
//        if(!empty($param['start_time'])) {
//            $where[] = ['t1.start_time', 'between time', [
//                $start_time,
//                $end_time
//            ]];
//        }
//        if ($param['type'] == 1) $where[] = ['t1.end_time', '>', time()];
//        $data = Db::name('meeting_city')
//            ->alias('t1')
//            ->join('meeting t2', 't1.p_id = t2.id')
//            ->join('meeting t3', 't2.p_id = t3.id')
//            ->where($where)
//            ->field('t1.id, t1.city, t1.address, t1.userId, t1.userName, t1.userPhone, t2.name, t3.id as p_id, t3.name as title, t1.start_time, t1.end_time')
//            ->page((int)$param['page'], (int)$param['limit'])
//            ->order('t1.start_time', 'asc')
//            ->select()->toArray();
//        $count = Db::name('meeting_city')
//            ->alias('t1')
//            ->join('meeting t2', 't1.p_id = t2.id')
//            ->join('meeting t3', 't2.p_id = t3.id')
//            ->where($where)
//            ->field('t1.id, t1.city, t1.address, t1.userId, t1.userName, t1.userPhone, t2.name, t3.name as title, t1.start_time, t1.end_time')
//            ->count();
//        $num = 0;
//        $ids = [];
//        foreach ($data as &$datum){
//            $ids[] = $datum['id'];
//            $datum['num'] = Db::name('member_meeting')
//                ->where([
//                    ['m_id', '=', $datum['id']]
//                ])
//                ->count();
//            $num += $datum['num'];
//            $complete = 0;
//            $complete1 = 0;
//            if ($getUserInfo['code'] != 401){
//                // 是否预约
//                $complete = Db::name('member_meeting')
//                    ->where([
//                        ['m_id', '=', $datum['id']],
//                        ['mobile', '=', (string) $getUserInfo['phone']],
//                    ])
//                    ->count();
//                // 是否参加
//                $complete1 = Db::name('member_meeting')
//                    ->where([
//                        ['m_id', '=', $datum['id']],
//                        ['mobile', '=', (string) $getUserInfo['phone']],
//                    ])
//                    ->value('complete');
//            }
//            $datum['type'] = 1;
//            if ($datum['end_time'] < time()) $datum['type'] = 0;
//            $datum['avatar'] = Db::name('member')->where('id', $datum['userId'])->value('avatar');
//            $datum['complete'] = 0;
//            if ($complete != 0) $datum['complete'] = $complete1;
//        }
//        // 实际报名人数
//        $number = Db::name('member_meeting')->whereIn('m_id', $ids)->group('mobile')->count();
//        return json(['code' => 200, 'msg' => '查询可报名讲师列表。', 'data' => $data, 'count' => $count, 'num' => $num, 'number' => $number]);
//    }

    /**
     * 查询所有讲师列表
     */
    public function getCityTeacher()
    {
        $param = input();
        $userInfo = $this->getUserAuthInfo($param);

        // 构建查询条件
        $where = $this->buildQueryConditions($param);

        // 获取基础查询构造器
        $baseQuery = Db::name('meeting_city')
            ->alias('t1')
            ->join('meeting t2', 't1.p_id = t2.id')
            ->join('meeting t3', 't2.p_id = t3.id')
            ->where($where);

        // 首先获取所有符合条件的场次ID（不分页）
        $allSessionIds = $baseQuery->column('t1.id');

        // 获取统计数据（基于所有符合条件的场次）
        $statistics = $this->getSignupStatistics($allSessionIds);

        // 获取分页数据
        $data = $baseQuery->field([
            't1.id', 't1.city', 't1.address', 't1.userId',
            't1.userName', 't1.userPhone', 't2.name',
            't3.id as p_id', 't3.name as title',
            't1.start_time', 't1.end_time'
        ])
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.start_time', 'asc')
            ->select()
            ->toArray();

        // 获取总记录数
        $count = $baseQuery->count();

        // 处理分页数据的详细信息
        $data = $this->processTeacherData($data, $userInfo, $statistics['signupCounts']);

        return json([
            'code' => 200,
            'msg' => '查询可报名讲师列表。',
            'data' => $data,
            'count' => $count,
            'num' => $statistics['totalSignups'],      // 所有场次的总报名人次
            'number' => $statistics['uniqueSignups']   // 所有场次的实际报名人数（去重）
        ]);
    }

    /**
     * 获取报名统计数据
     */
    private function getSignupStatistics($sessionIds)
    {
        if (empty($sessionIds)) {
            return ['signupCounts' => [], 'totalSignups' => 0, 'uniqueSignups' => 0];
        }

        // 获取每个场次的有效报名人次
        $signupQuery = Db::name('member_meeting')
            ->whereIn('m_id', $sessionIds)
            ->where('state', '<>', 0);  // 排除已取消的报名

        // 获取每个场次的报名数
        $signupCounts = $signupQuery->group('m_id')
            ->column('COUNT(*)', 'm_id');

        // 获取总报名人次
        $totalSignups = array_sum($signupCounts);

        // 获取实际报名人数（去重）
        $uniqueSignups = $signupQuery->group('mobile')->count();

        return [
            'signupCounts' => $signupCounts,
            'totalSignups' => $totalSignups,
            'uniqueSignups' => $uniqueSignups
        ];
    }

    /**
     * 处理讲师数据
     */
    private function processTeacherData($data, $userInfo, $signupCounts)
    {
        foreach ($data as &$item) {
            // 获取该场次的报名人数
            $item['num'] = $signupCounts[$item['id']] ?? 0;

            // 设置活动状态
            $item['type'] = $item['end_time'] < time() ? 0 : 1;

            // 获取讲师头像
            $item['avatar'] = Db::name('member')
                ->where('id', $item['userId'])
                ->value('avatar');

            // 获取用户报名状态
            $item['complete'] = 0;
            if ($userInfo['code'] != 401) {
                $memberMeeting = Db::name('member_meeting')
                    ->where([
                        ['m_id', '=', $item['id']],
                        ['mobile', '=', (string)$userInfo['phone']],
                        ['state', '<>', 0]  // 排除已取消的报名
                    ])
                    ->find();

                if ($memberMeeting) {
                    $item['complete'] = $memberMeeting['complete'];
                }
            }
        }

        return $data;
    }

    /**
     * 获取用户认证信息
     */
    private function getUserAuthInfo($param)
    {
        $header = request()->header();
        if (!isset($header['authorization']) || empty($header['authorization'])) {
            return [
                'code' => empty($param['mobile']) ? 401 : 200,
                'phone' => $param['mobile'] ?? ''
            ];
        }
        return getUserInfo(true);
    }

    /**
     * 构建查询条件
     */
    private function buildQueryConditions($param)
    {
        $where = [];

        // 讲师筛选
        if (!empty($param['id'])) {
            $where[] = ['t1.userId', '=', $param['id']];
        }

        // 活动筛选
        if (!empty($param['m_id'])) {
            $where[] = ['t3.id', '=', $param['m_id']];
        }

        // 搜索条件
        if (!empty($param['search'])) {
            $where[] = ['t1.city|t1.userName|t1.address', 'like', '%' . $param['search'] . '%'];
        }

        // 地址筛选
        if (!empty($param['city'])) {
            $where[] = ['t1.city|t1.address', 'like', '%' . $param['city'] . '%'];
        }

        // 可报名场次
        if (!empty($param['p_id'])) {
            $where[] = ['t1.p_id', 'in', $param['p_id']];
        }

        // 时间查询
        if (!empty($param['start_time'])) {
            $where[] = ['t1.start_time', 'between time', [
                $param['start_time'] . ' 00:00:01',
                ($param['end_time'] ?? $param['start_time']) . ' 23:59:59'
            ]];
        }

        // 未结束活动筛选
        if (($param['type'] ?? 0) == 1) {
            $where[] = ['t1.end_time', '>', time()];
        }

        return $where;
    }


    /**
     * @Description: 月份查询每月几号有课
     * @Author: 林怼怼
     * @Date: 2024/10/28
     * @Time: 17:21
     */
    public function calendar()
    {
        $param = input();
        $result = Db::name('meeting_city')
            ->whereTime('end_time', '>', time())
            ->field('id, start_time')
            ->select();
        return json(['code' => 200, 'msg' => '查询阶段讲师。', 'data' => $result]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 查询活动内容
	 * @Author: Linxy
	 * @Time: 2023/10/27 16:34
	 */
	public function getMeetingDetail()
	{
		$param = input();
        $data = Db::name('meeting')
            ->where([['id', '=', $param['id']]])
            ->field('id, name, desc, pinyin, content, video, img, text, userName, userPhone, start_time, end_time, time')
            ->find();
        $data['count'] = 0;
        $data['img'] = explode(',', $data['img']);
        $data['arr'] = Db::name('meeting')->where('p_id', $data['id'])->field('id, name, img, content, desc')->select()->toArray();
        $data['rank'] = getMeetingRank($data['id']);
        $data['evaluate'] = [
            Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 10, 'type' => 1, 'state' => 1])->count(),
            Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 8, 'type' => 1, 'state' => 1])->count(),
            Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 6, 'type' => 1, 'state' => 1])->count(),
            Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 4, 'type' => 1, 'state' => 1])->count(),
            Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 2, 'type' => 1, 'state' => 1])->count(),
        ];
        foreach ($data['arr'] as &$datum)
        {
            $ids = Db::name('meeting_city')->whereIn('p_id', $datum['id'])->column('id');
            $datum['count'] = Db::name('member_meeting')->whereIn('m_id', $ids)->count();
            $data['count'] += $datum['count'];
        }
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data]);
	}


    /**
     * @Description: 查询平台中的讲师
     * @Author: 林怼怼
     * @Date: 2024/10/27
     * @Time: 9:59
     */
    public function firstTeacher()
    {
//        $data = Db::name('meeting_city')
//            ->field('userId as id, userName as name, userPhone as phone')
//            ->group('id')
//            ->select();
        $data = Db::name('member')
            ->distinct(true)   // 启用去重
            ->where('is_teacher', 1)
            ->field('id, name, phone')
            ->select();
        return json(['code' => 200, 'msg' => '查询讲师成功。', 'data' => $data]);
    }

    /**
	 * Created by PhpStorm.
	 * @purpose 总部审核活动
	 * @Author: Linxy
	 * @Time: 2023/10/27 10:43
	 */
	public function edit()
	{
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] !== 1) return json(['code' => 203, 'msg' => '权限不足']);
        $result = Db::name('meeting')->save($param);
        return json(['code' => 200, 'msg' => '审核成功。']);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 查看活动列表
	 * @Author: Linxy
	 * @Time: 2023/10/26 11:26
	 */
	public function list()
	{
		$param = input();
        // 更新活动
        activityExamine();
		$where = [];
		$where[] = ['t1.p_id', '=', 0];
        // 搜索城市
        $where[] = ['t3.city', 'like', '%'.$param['city'].'%'];
        // 讲师筛选
		if (!empty($param['u_id'])) $where[] = ['t3.userId', '=', $param['u_id']];
		// 时间筛选
        $start_time = empty($param['start_time']) ? '2001-10-15 00:00:01' : $param['start_time'] .' 00:00:01';
        $end_time = empty($param['end_time']) ? date("Y-m-d 23:59:59", time()+2628000) : $param['end_time'] .' 23:59:59';
        $where[] = ['t3.start_time', 'between time', [
            $start_time,
            $end_time
        ]];
        $data = Db::name('meeting')
            ->alias('t1')
            ->leftJoin("meeting t2", 't2.p_id = t1.id')
            ->leftJoin('meeting_city t3', 't2.id = t3.p_id')
            ->where($where)
            ->field('t1.id, t1.num, t1.userName, t1.userPhone, t1.name as title, t1.img, t1.content, t1.desc, t1.state, t1.time')
            ->group('t1.id')
            ->page((int)$param['page'], (int)$param['limit'])
            ->select()->toArray();
        $count = Db::name('meeting')
            ->alias('t1')
            ->leftJoin("meeting t2", 't2.p_id = t1.id')
            ->leftJoin('meeting_city t3', 't2.id = t3.p_id')
            ->where($where)
            ->group('t1.id')
            ->count();
        foreach ($data as &$v)
        {
            $v['img'] = explode(',', $v['img']);
            $ids = Db::name('meeting')->where('p_id', $v['id'])->column('id');
            $c_ids = Db::name('meeting_city')->whereIn('p_id', $ids)->column('id');
            $v['count'] = Db::name('member_meeting')->whereIn('m_id', $c_ids)->count();
        }
        return json(['code' => 200, 'msg' => '查看活动列表。', 'data' => $data, 'count' => $count]);
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 检查活动是否开始结束
	 * @Author: Linxy
	 * @Time: 2023/10/25 17:53
	 */
	public function examine()
	{
		// 启动事务
		Db::startTrans();
		try {
			# 批量修改(进行中)
			$result = Db::name('meeting')
				->whereTime('start_time', '<=', date('Y-m-d H:i:s'))
				->whereTime('end_time', '>=', date('Y-m-d H:i:s'))
				->where('state', '<>', 4)
				->update(['state' => 2]);
			# 批量修改(已结束)
			$result = Db::name('meeting')
				->whereTime('end_time', '<=', date('Y-m-d H:i:s'))
				->where('state', '<>', 4)
				->update(['state' => 3]);
			# 批量修改(未开始)
			$result = Db::name('meeting')
				->whereTime('start_time', '>=', date('Y-m-d H:i:s'))
				->where('state', '<>', 4)
				->update(['state' => 1]);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '活动状态已更新。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 201, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 提示活动
	 * @Author: Linxy
	 * @Time: 2023/10/25 16:30
	 */
   public function read()
   {
        $userInfo = getUserInfo(true);
        $where[] = ['a.u_id', '=', $userInfo['id']];
        $where[] = ['b.state', 'in', '1,2,3'];
        $where[] = ['a.state', 'in', 1];
        $data = Db::name('meeting_user')
          ->alias('a')
          ->join('meeting b', 'a.id = b.id')
          ->where($where)
          ->whereTime('a.message_time', '<=', date('Y-m-d H:i:s'))
          ->field('b.id, b.userName, b.userPhone, b.address, b.name, b.img, b.state')
          ->select()
          ->toArray();
        $count = Db::name('meeting_user')
           ->alias('a')
           ->join('meeting b', 'a.id = b.id')
           ->where($where)
           ->whereTime('a.message_time', '<=', date('Y-m-d H:i:s'))
           ->count();
        foreach ($data as &$item) {
            $item['img'] = explode(',', $item['img'])[0];
        }
        return json(['code' => 200, 'msg' => '活动提醒。', 'data' => $data, 'count' => $count]);
        }
}
