<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Db;

class Notice
{

    /**
     * @Description: 查看公告详情
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 15:20
     */
    public function read()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $data = Db::name('notice')->find($id);
            $data['img'] = explode(',', $data['img']);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查看公告记录
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 15:15
     */
    public function index()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo(true);
            $data = Db::name('notice')
                ->alias('t1')
                ->join('notice_user t2', 't1.id = t2.id')
                ->where(['t2.u_id' => $userInfo['id']])
                ->field('t1.id, t1.title, t1.img, t1.time, t2.state')
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('t1.time', 'desc')
                ->select()->toArray();
            $count = Db::name('notice')
                ->alias('t1')
                ->join('notice_user t2', 't1.id = t2.id')
                ->where(['t2.u_id' => $userInfo['id']])
                ->order('t1.time', 'desc')
                ->count();
            foreach ($data as &$datum) {
                $datum['img'] = explode(',', $datum['img']);
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data, 'count' => $count]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 修改公告状态
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 14:55
     */
    public function edit()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo(true);
            if ($id == 0) {
                Db::name('notice_user')->where(['u_id' => $userInfo['id']])->update(['state' => 1]);
            }else{
                Db::name('notice_user')->where(['id' => $id, 'u_id' => $userInfo['id']])->update(['state' => 1]);
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 查看公告信息
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 14:43
     */
    public function list()
    {
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo(true);
            $data = Db::name('notice')
                ->alias('t1')
                ->join('notice_user t2', 't1.id = t2.id')
                ->where(['t2.u_id' => $userInfo['id'], 't2.state' => 2])
                ->field('t1.id, t1.title, t1.img, t1.desc, t1.time')
                ->order('t1.time', 'desc')
                ->select()->toArray();
            foreach ($data as &$datum) {
                $datum['img'] = explode(',', $datum['img']);
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => '异常', 'error' => $e->getMessage()]);
        }
    }
}
