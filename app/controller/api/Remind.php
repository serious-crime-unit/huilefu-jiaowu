<?php
declare (strict_types = 1);

namespace app\controller\api;

use think\facade\Db;

class Remind
{
    /**
     * @Description: 好妈妈提醒
     * @Author: 林怼怼
     * @Date: 2025/1/13
     * @Time: 16:53
     */
    public function getUserGradeVIPRemind()
    {
        $userInfo = getUserInfo(true);
        $count = Db::name('remind')
            ->where(['c_id' => $userInfo['id'], 'type' => 5, 'state' => 1])
            ->count();
        if ($count != 0) return json([
            'code' => 200,
            'msg' => true,
            'data' => [
                'id' => Db::name('remind')->where(['c_id' => $userInfo['id'], 'type' => 5, 'state' => 1])->value('id'),
                'name' => $userInfo['name'],
                'code' => Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => 10])->value('code'),
                'time' => date('Y年m月d日', Db::name('order')->where(['u_id' => $userInfo['id'], 'c_id' => 11])->value('time')),
                'title' => '恭喜你获得'.Db::name('grade')->where(['id' => 10])->value('name')
            ]
        ]);
        return json([
            'code' => 200,
            'msg' => false,
            'data' => [
                'name' => '',
                'code' => '',
                'time' => ''
            ]
        ]);
    }

    /**
     * @Description: 提醒用户审核
     * @Author: 林怼怼
     * @Date: 2024/4/8/008
     * @Time: 15:48
     */
    public function setParentRemind()
    {
        $param = input();
        $data = Db::name('member')->where('id', $param['id'])->find();
        if (empty($data['openid'])) {
            return json(['code' => 200, 'msg' => '用户未绑定微信请联系用户绑定。', 'data' => ['is_wx' => false, 'phone' => $data['phone']]]);
        }
        $arr = [];
        $arr['url'] = $param['url'];
        $arr['userName'] = $param['title'];
        $arr['desc'] = $param['desc'];
        $arr['time'] = date('Y-m-d H:i');
        $result = sendTemplateMessage2($data['openid'], $arr);
        return json(['code' => 200, 'msg' => '审核提醒已发送，请等待审核结果。', 'data' => ['is_wx' => true, 'phone' => $data['phone']]]);
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $where = [];
        $where[] = ['a.c_id', '=', $userInfo['id']];
        if ($param['type'] != 0) {
            $where[] = ['a.state', '=', $param['type']];
        }
        $data = Db::name('remind')
            ->alias('a')
            ->join('member b', 'a.u_id = b.id')
            ->where($where)
            ->order('a.time', 'desc')
            ->field('a.id, a.r_id, b.avatar, b.name, a.desc, a.type, a.state, a.time')
            ->page((int) $param['page'], (int) $param['limit'])
            ->select()
            ->toArray();
        $count = Db::name('remind')->alias('a')
            ->join('member b', 'a.u_id = b.id')
            ->where($where)
            ->order('a.time', 'desc')
            ->count();
        return json(['code' => 200, 'msg' => '消息列表。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 修改消息为已读
     * @Author: 林怼怼
     * @Date: 2024/2/26/026
     * @Time: 15:54
     */
    public function edit()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if (!empty($param['ids'])) {
            $result = Db::name('remind')
                ->where('id', 'in', $param['ids'])
                ->where('c_id', $userInfo['id'])
                ->update(['state' => 2]);
        }else{
            $result = Db::name('remind')
                ->where('c_id', $userInfo['id'])
                ->update(['state' => 2]);
        }
        return json(['code' => 200, 'msg' => '读取消息成功。']);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        $param = input();
        $result = Db::name('remind')
            ->where('id', 'in', $param['ids'])
            ->delete();
        return json(['code' => 200, 'msg' => '读取消息成功。']);
    }
}
