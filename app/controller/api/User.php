<?php
declare (strict_types = 1);

namespace app\controller\api;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Db;
use think\facade\Log;
use think\Request;

class User
{

    /**
     * 允许的文件类型
     */
    private $allowedTypes = [
        'video/mp4',
        'video/mpeg',
        'video/quicktime'
    ];

    /**
     * 最大文件大小 (500MB)
     */
    private $maxFileSize = 524288000;

    /**
     * @Description: 删除大区总裁
     * @Author: 林怼怼
     * @Date: 2025/2/28
     * @Time: 17:06
     */
    public function delCityBoss()
    {
        $param = input();
        $res = Db::name('city_admin')->delete($param['id']);
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 查看大区列表
     * @Author: 林怼怼
     * @Date: 2025/2/28
     * @Time: 16:49
     */
    public function getCityBoss()
    {
        $param = input();
        $data = Db::name('city_admin')->alias('t1')
            ->join('city t2', 't1.city = t2.id')
            ->where('t2.cityName', 'like', '%'.$param['city'].'%')
            ->field('t1.*, t2.cityName')
            ->page((int)$param['page'], (int)$param['limit'])
            ->select()->toArray();
        $count = Db::name('city_admin')->alias('t1')
            ->join('city t2', 't1.city = t2.id')
            ->where('t2.cityName', 'like', '%'.$param['city'].'%')
            ->field('t1.*, t2.cityName')
            ->count();
        return json([
            'code' => 200,
            'msg' => '成功.',
            'data' => $data,
            'count' => $count
        ]);
    }

    /**
     * @Description: 上传大区微信
     * @Author: 林怼怼
     * @Date: 2025/2/28
     * @Time: 15:45
     */
    public function saveCityBoss()
    {
        $param = input();
        $res = Db::name('city_admin')->save(
            [
                'city' => $param['city'],
                'img' => $param['img'],
                'time' => time()
            ]
        );
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 删除回放视频
     * @Author: 林怼怼
     * @Date: 2025/2/27
     * @Time: 15:36
     */
    public function delVideo()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '无权限下架视频']);
        $res = Db::name('video')->save(['id' => $param['id'], 'state' => $param['state']]);
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 查询回访列表
     * @Author: 林怼怼
     * @Date: 2025/2/27
     * @Time: 15:26
     */
    public function getVideo()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] == 1) {
            $data = Db::name('video')
                ->page((int)$param['page'], (int)$param['limit'])
                ->select()->toArray();
            $count = Db::name('video')->count();
        }else{
            $data = Db::name('video')
                ->where('grade', 'in', $param['grade'])
                ->where('state', '=', 1)
                ->page((int)$param['page'], (int)$param['limit'])
                ->select()->toArray();
            $count = Db::name('video')
                ->where('grade', 'in', $param['grade'])
                ->count();
        }
        return json([
            'code' => 200,
            'msg' => '成功.',
            'data' => $data,
            'count' => $count
        ]);
    }

    /**
     * @Description: 添加视频内容
     * @Author: 林怼怼
     * @Date: 2025/2/27
     * @Time: 13:42
     */
    public function setVideo()
    {
        $param = input();
        $res = Db::name('video')->save([
            'video' => $param['video'],
            'title' => $param['title'],
            'img' => $param['img'],
            'desc' => $param['desc'],
            'grade' => $param['grade'],
            'state' => $param['state'],
            'time' => time(),
        ]);
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 用户解绑孩子
     * @Author: 林怼怼
     * @Date: 2025/2/25
     * @Time: 10:10
     */
    public function chilDel()
    {
        $param = input();
        $res = Db::name('children')->where('id', $param['id'])->delete();
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 查询孩子列表
     * @Author: 林怼怼
     * @Date: 2025/2/25
     * @Time: 10:10
     */
    public function chilList()
    {
        $param = input();
        $data = Db::name('children')->where('u_id', $param['id'])->select()->toArray();
        $count = Db::name('children')->where('u_id', $param['id'])->count();
        return json([
            'code' => 200,
            'msg' => '成功.',
            'data' => $data,
            'count' => $count
        ]);
    }

    /**
     * @Description: 用户绑定孩子
     * @Author: 林怼怼
     * @Date: 2025/2/25
     * @Time: 10:02
     */
    public function saveChil()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        $res = Db::name('children')->save([
            'u_id' => $userInfo['id'],
            'chilName' => $param['chilName'],
            'chilImg' => $param['chilImg'],
            'age' => $param['age'],
            'sex' => $param['sex'],
            'time' => time()
        ]);
        return json([
            'code' => 200,
            'msg' => '成功.'
        ]);
    }

    /**
     * @Description: 谁点赞了我的评论
     * @Author: 林怼怼
     * @Date: 2025/1/15
     * @Time: 14:47
     */
    public function whoLikedMyComments()
    {
        $userInfo = getUserInfo(true); // 假设从请求中获取用户ID

        $ids = Db::name('evaluate')
            ->where(['u_id' => $userInfo['id'], 'state' => 1])
            ->column('id');
        $comments = Db::name('support')
            ->whereIn('p_id', $ids)
            ->where(['pandn' => 1])
            ->order([
                'time' => 'desc'
            ])
            ->select();
        $result = [];
        foreach ($comments as $item) {
            $data = Db::name('evaluate')->where('id', $item['p_id'])->find();
            $userInfo1 = Db::name('member')->where('id', $item['u_id'])->find();
            $result[] = [
                'id' => $data['id'],
                'comment_id' => $data['p_id'],
                'comment_content' => $data['desc'],
                'comment_img' => $data['img'],
                'comment_video' => $data['video'],
                'score' => $data['score'],
                'poster' => $data['poster'],
                'type' => $data['type'],
                'time' => $item['time'],
                'userName' => $userInfo1['name'],
                'avatar' => $userInfo1['avatar'],
            ];
        }

        return json([
            'code' => 200,
            'msg' => '成功.',
            'data' => $result
        ]);
    }

    /**
     * @Description: 查看我发布的评论
     * @Author: 林怼怼
     * @Date: 2025/1/15
     * @Time: 14:24
     */
    public function myComments()
    {
        $userInfo = getUserInfo(true); // 假设从请求中获取用户ID

        // 查询用户发布的评论（state=1）
        $comments = Db::name('evaluate')->where('u_id', $userInfo['id'])
            ->where(['u_id' => $userInfo['id'], 'state' => 1])
            ->order('time', 'desc')
            ->select()->toArray();
        foreach ($comments as &$items) {
            $items['userName'] = $userInfo['name']; // 用户名
            $items['avatar'] = $userInfo['avatar']; // 头像
            $data = getActivityID($items);
            $items['comment_id'] = $data['p_id'];
            $items['ParentName'] = '';
            if ($items['type'] == 1 || $items['type'] == 4) {
                // 查询活动课程名称
                $items['title'] = Db::name('meeting')->where('id', $data['p_id'])->value('name');
            }elseif($items['type'] == 2 || $items['type'] == 5) {
                // 查询用户讲师名称
                $items['title'] = Db::name('member')->where('id', $data['p_id'])->value('name');
            }else{
                // 查询评论回复用户名称
                if ($items['type'] == 3) {
                    $items['title'] = Db::name('meeting')->where('id', $data['p_id'])->value('name');
                    $u_id = Db::name('evaluate')->where('id', $items['p_id'])->value('u_id');
                    $items['ParentName'] = Db::name('member')->where('id', $u_id)->value('name');
                }else{
                    $items['title'] = Db::name('member')->where('id', $data['p_id'])->value('name');
                    $u_id = Db::name('evaluate')->where('id', $items['p_id'])->value('u_id');
                    $items['ParentName'] = Db::name('member')->where('id', $u_id)->value('name');
                }
            }
        }

        return json([
            'code' => 200,
            'msg' => '成功.',
            'data' => $comments
        ]);
    }

    /**
     * @Description: 粉丝关注点赞
     * @Author: 林怼怼
     * @Date: 2025/1/15
     * @Time: 13:54
     */
    public function getUserThreeList()
    {
        $userInfo = input();
        // 粉丝
        $num1 = Db::name('concern')->where(['u_id' => $userInfo['id']])->count();
        // 关注
        $num2 = Db::name('concern')->where(['id' => $userInfo['id']])->count();
        // 点赞
        $ids = Db::name('evaluate')->where(['u_id' => $userInfo['id']])->column('id');
        $num3 = Db::name('support')->whereIn('p_id', $ids)->where(['pandn' => 1])->count();
        return json(['code' => 200, 'msg' => '成功.', 'data' => ['fensi' => $num1, 'guanzhu' => $num2, 'dianzan' => $num3]]);
    }

    /**
     * @Description: 查询用户证书
     * @Author: 林怼怼
     * @Date: 2025/1/13
     * @Time: 15:39
     */
    public function getUserGradeVIPList()
    {
        $param = input();
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->join('order t3', 't1.id = t3.u_id AND t3.c_id = 11')
            ->join('grade t4', 't2.grade = t4.id')
            ->where([
                ['t1.id', '=', $param['id']],
                ['t2.grade', 'in', 10]
            ])
            ->field('t1.name, t1.sex, t2.code, t3.time, t4.name as title')
            ->page((int)$param['page'], (int)$param['limit'])
            ->order([
                't3.time' => 'desc'
            ])
            ->select()->toArray();
        $count = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->join('order t3', 't1.id = t3.u_id AND t3.c_id = 11')
            ->join('grade t4', 't2.grade = t4.id')
            ->where([
                ['t1.id', '=', $param['id']],
                ['t2.grade', 'in', 10]
            ])
            ->count();
        foreach ($data as &$datum) {
            $datum['time'] = date('Y年m月d日', $datum['time']);
        }
        return json(['code' => 200, 'msg' => '成功.', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 身份到期时间
     * @Author: 林怼怼
     * @Date: 2024/12/23
     * @Time: 15:14
     */
    public function getGradeDetails()
    {
        $param = input();
        if($param['id'] == 0) {
            $userInfo = getUserInfo(true);
        }else{
            $userInfo = $param;
        }
        $vip_end_time = Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => $param['grade']])->value('vip_end_time');
        if ($vip_end_time == null) $vip_end_time = 0;
        return json(['code' => 200, 'msg' => '成功.', 'data' => $vip_end_time]);
    }


    public function exportList()
    {
        $param = input();
        $start_time = strtotime($param['start_time'] == '' ? "2000-10-01 00:00:01" : $param['start_time'] . ' 00:00:01');
        $end_time = strtotime($param['end_time'] == '' ? date("Y-m-d 23:59:59") : $param['end_time'] . ' 23:59:59');

        // 创建一个新的Excel对象
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $data = Db::name('member')
            ->alias('t1')
            ->join('member_parent t2', 't1.id = t2.id')
            ->join('member t3', 't2.p_id = t3.id')
            ->join('member_grade t4', 't1.id = t4.id')
            ->where('t4.grade', $param['grade'])
            ->whereTime('t1.time', 'between', [$start_time, $end_time])
            ->field('t1.id, t1.name, t1.phone, t1.number, t3.id as nickID, t3.name as nickName, t3.phone as nickPhone')
            ->select()->toArray();
        $sheet->setCellValue('A'.(1), '姓名');
        $sheet->setCellValue('B'.(1), '电话');
        $sheet->setCellValue('C'.(1), '编号');
        $sheet->setCellValue('D'.(1), '上级姓名');
        $sheet->setCellValue('E'.(1), '上级电话');
        $sheet->setCellValue('F'.(1), '代理名字');
        $sheet->setCellValue('G'.(1), '代理电话');
        $sheet->setCellValue('H'.(1), '等级');
        foreach ($data as $key => &$val) {
            $level = Db::name('member_grade')->where('id', $val['id'])->column('grade');
            $val['level'] = implode(',', Db::name('grade')->whereIn('id', $level)->column('name'));
            $sheet->setCellValue('A'.((int)$key+2), $val['name']);
            $sheet->setCellValue('B'.((int)$key+2), $val['phone']);
            $sheet->setCellValue('C'.((int)$key+2), $val['number']);
            $sheet->setCellValue('D'.((int)$key+2), $val['nickName']);
            $sheet->setCellValue('E'.((int)$key+2), $val['nickPhone']);
            $res = getParent_365_521($val['id']);
            $sheet->setCellValue('F'.((int)$key+2), $res['name']);
            $sheet->setCellValue('G'.((int)$key+2), $res['phone']);
            $sheet->setCellValue('H'.((int)$key+2), $val['level']);
        }

        // 导出Excel文件
        $filename = '好妈妈活动报名表.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        // 设置响应头，告诉浏览器输出Excel文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        // 将Excel文件输出到浏览器
        $writer->save('php://output');

        // 结束请求，确保不会输出其他内容
        return json(['code' => 200, 'msg' => '导入Excel完成.']);
    }

    /**
     * Created by PhpStorm.
     * @purpose 用户下级列表
     * @Author: Linxy
     * @Time: 2023/10/17 9:28
     */
    public function getChildList()
    {
        $param = input();
        if ($param['id'] != 0) {
            $userInfo = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_address t2', 't1.id = t2.id AND t2.is_master = 1 AND t2.state = 1')
                ->leftJoin('city t4', 't2.c_id = t4.id')
                ->fieldRaw('t1.id, t4.id as city_id')
                ->find($param['id']);
        }else{
            $userInfo = getUserInfo(true);
        }
        $where = [];
        // 不计算总部账号
        $where[] = ['t1.id', '<>', 1];
        // 默认地址
        $where[] = ['t3.is_master', '=', 1];
        // 搜索
        $where[] = ['t1.name|t1.phone|t1.number', 'like', '%'.$param['search'].'%'];
        // 地区搜索
        $where[] = ['t3.p_id|t3.c_id|t3.d_id', 'like', '%'.$param['city'].'%'];
        // 级别筛选
        if (!empty($param['level'])) $where[] = ['t5.grade', 'in', $param['level']];
        // 状态筛选
        if ($param['state'] != '') $where[] = ['t1.state', '=', $param['state']];
        // 注册时间筛选
        $start_time = empty($param['start_time']) ? '2001-10-15 00:00:01' : $param['start_time'] .' 00:00:01';
        $end_time = empty($param['end_time']) ? date("Y-m-d 23:59:59") : $param['end_time'] .' 23:59:59';
        $where[] = ['t1.time', 'between time', [
            $start_time,
            $end_time
        ]];
        // 查看是否是本地
        if ($param['area'] == 1) {
            // 本地
            $where[] = ['t3.c_id', '=', $userInfo['city_id']];
        }elseif($param['area'] == 2) {
            // 外地
            $where[] = ['t3.c_id', '<>', $userInfo['city_id']];
        }
        // 筛选直接或间接推荐
        if ($param['type'] == 1) {
            // 查询直接下级
            $ids = Db::name('member_parent')
                ->alias('t1')
                ->where([
                    ['t1.p_id', 'in', $userInfo['id']]
                ])
                ->column('t1.id');
        }elseif($param['type'] == 2) {
            // 查询间接下级
            $ids = Db::name('member_parent')
                ->alias('t1')
                ->where([
                    ['t1.p_id', 'in', $userInfo['id']]
                ])
                ->column('t1.id');
            $ids = getChildUserPhoneS($ids);
        }else{
            $ids = getChildUserPhoneS($userInfo['id']);
        }
        $where[] = ['t1.id', 'in', $ids];
        // 不查询子级的代理
        $subQueryWhere = [];
        $subQueryWhere[] = ['t2.grade', 'in', [1,4,7,10]];
        $subQueryWhere[] = ['t3.state', '=', 1];
        $subQuery = Db::name('member_parent')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->join('member t3', 't1.id = t3.id')
            ->where($subQueryWhere)
            ->field('t1.p_id, count(DISTINCT t1.id) num')
            ->group('t1.p_id')
            ->buildSql();
        $data = Db::name('member')
            ->alias('t1')
            ->leftJoin("$subQuery t2", 't1.id = t2.p_id')
            ->leftJoin("member_address t3", 't1.id = t3.id')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->where($where)
            ->group('t1.id')
            ->page((int)$param['page'], (int)$param['limit'])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, IFNULL(t2.num, 0) as num, t1.state')
            ->order([
                'num' => $param['order'],
                't1.id' => 'desc'
            ])
            ->select()->toArray();
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("$subQuery t2", 't1.id = t2.p_id')
            ->leftJoin("member_address t3", 't1.id = t3.id')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->where($where)
            ->group('t1.id')
            ->count();
//        foreach ($data as &$datum) {
//            $datum['level'] = array_level($datum['id']);
//        }
        $data = array_map(function ($item) {
            $item['level'] = array_level($item['id']);
            return $item;
        }, $data);
        return json(['code' => 200, 'msg' => '查询用户列表成功', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 查询当前用户的身份用来筛选
     * @Author: 林怼怼
     * @Date: 2024/3/14/014
     * @Time: 12:35
     */
    public function getUserGradeInfo()
    {
        $userInfo = getUserInfo(true);
        $arr = [];
        if (in_array(3, $userInfo['level'])) $arr[] = 3;
        if (in_array(5, $userInfo['level'])) $arr[] = 5;
        if (in_array(6, $userInfo['level'])) $arr[] = 6;
        if (in_array(9, $userInfo['level'])) $arr[] = 9;
        if (in_array(11, $userInfo['level'])) $arr[] = 11;
        if ($userInfo['id'] === 1) $arr = [3,5,6,9,11];
        $data = Db::name('grade')->whereIn('id', $arr)->field('id as value, name as text')->select()->toArray();
        return json(['code' => 200, 'msg' => '成功', 'data' => $data]);

    }

    /**
     * @Description: 添加用户身份
     * @Author: 林怼怼
     * @Date: 2024/3/13/013
     * @Time: 13:28
     */
    public function saveUserGrade()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '权限不足。']);
        // 启动事务
        Db::startTrans();
        try {
            $count = Db::name('member_grade')->where(['id' => $param['id'], 'grade' => $param['grade']])->count();
            if ($count > 0) return json(['code' => 201, 'msg' => '身份已存在。']);
            if ($param['grade'] == 1 || $param['grade'] == 4 || $param['grade'] == 7 || $param['grade'] == 10) {
                // 普通身份
                Db::name('member_grade')
                    ->insert([
                        'id' => $param['id'],
                        'grade' => $param['grade'],
                        'vip_start_time' => time(),
                        'vip_end_time' => strtotime("+1 year", time())
                    ]);
            }else{
                if ($param['grade'] == 5) {
                    $data = Db::name('member_grade')
                        ->where([
                            'id' => $param['id'],
                            'grade' => 6
                        ])->find();
                    if ($data) return json(['code' => 201, 'msg' => '已有笑长身份，请先修改或删除。']);
                }elseif($param['grade'] == 6) {
                    $data = Db::name('member_grade')
                        ->where([
                            'id' => $param['id'],
                            'grade' => 5
                        ])->find();
                    if ($data) return json(['code' => 201, 'msg' => '已有微笑长身份，请先修改或删除。']);
                }
                // 代理身份
                Db::name('member_grade')
                    ->insert([
                        'id' => $param['id'],
                        'grade' => $param['grade'],
                        'vip_start_time' => null,
                        'vip_end_time' => null
                    ]);
            }
            BackstageLog($userInfo['id'], '添加用户身份', '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'用户id【'.$param['id'].'】添加的身份是'.$param['grade'].'。');
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 搜索推荐人
     * @Author: 林怼怼
     * @Date: 2024/3/12/012
     * @Time: 17:25
     */
    public function getParentList()
    {
        $param = input();
        $ids = Db::name('member_parent')->where('p_id', $param['id'])->column('id');
        $ids[] = (int) $param['id'];
        $data = Db::name('member')
            ->where([
                ['id', 'not in', $ids],
                ['state', '=', 1],
                ['name|phone', 'like', '%'.$param['search']]
            ])
            ->field('id, avatar, name, phone')
            ->page((int) $param['page'], (int) $param['limit'])
            ->select()->toArray();
        $count = Db::name('member')
            ->where([
                ['state', '=', 1],
                ['name|phone', 'like', '%'.$param['search']]
            ])
            ->count();
        return json(['code' => 200, 'msg' => '操作成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 总部删除等级
     * @Author: 林怼怼
     * @Date: 2024/3/12/012
     * @Time: 14:47
     */
    public function delUserGrade()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '权限不足。']);
        // 启动事务
        Db::startTrans();
        try {
            $count = Db::name('member_grade')->where('id', $param['id'])->count();
            if ($count === 1) return json(['code' => 201, 'msg' => '必须保留一个等级。']);
            Db::name('member_grade')
                ->where([
                    'id' => $param['id'],
                    'grade' => $param['grade']
                ])
                ->delete();
            BackstageLog($userInfo['id'], '删除用户身份', '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'用户id【'.$param['id'].'】删除的身份是'.$param['grade'].'。');
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 修改用户等级
     * @Author: 林怼怼
     * @Date: 2024/3/12/012
     * @Time: 14:21
     */
    public function setUserGrade()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '权限不足。']);
        // 查询修改的用户
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_parent t2', 't1.id = t2.id')
            ->where(['t1.id' => $param['id']])
            ->field('t1.id, t1.phone, t1.name, t2.p_id')
            ->find();
        if ($param['grade'] == $param['level']) return json(['code' => 201, 'msg' => '身份已存在。']);
        // 启动事务
        Db::startTrans();
        try {
            $grade = Db::name('member_grade')->where(['id' => $data['id'], 'grade' => $param['grade']])->count();
            if ($grade !== 0) return json(['code' => 201, 'msg' => '身份已存在。']);
            // 修改等级
            if ($param['grade'] == 1 || $param['grade'] == 4 || $param['grade'] == 7 || $param['grade'] == 10) {
                // 修改成会员
                Db::name('member_grade')
                    ->where([
                        'id' => $param['id'],
                        'grade' => $param['level']
                    ])->update([
                    'grade' => $param['grade'],
                    'vip_start_time' => time(),
                    'vip_end_time' => strtotime("+1 year", time())
                ]);
            } else {
                // 修改成代理
                Db::name('member_grade')
                    ->where([
                        'id' => $param['id'],
                        'grade' => $param['level']
                    ])->update([
                        'grade' => $param['grade'],
                        'vip_start_time' => null,
                        'vip_end_time' => null
                ]);
                // 消息提醒
                Db::name('remind')->save([
                    'u_id' => $param['id'],
                    'c_id' => $data['p_id'],
                    'r_id' => 0,
                    'desc' => '您推荐的用户【' . $data['name'] . '】升级为代理身份。',
                    'type' => 2,
                    'time' => time(),
                ]);
            }
            $desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员信息用户id【'.$param['id'].'】 修改内容如下：原身份'. $param['level'].'=>修改为'.$param['grade'].'。';
            BackstageLog($userInfo['id'], '修改用户身份', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }


    /**
     * @Description: 修改用户库存
     * @Author: 林怼怼
     * @Date: 2025/3/13
     * @Time: 14:03
     */
    public function setUserInfoPay()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '权限不足。']);
        // 启动事务
        Db::startTrans();
        try {
            if ($param['type'] === 4) {
                $type = 1;
            }elseif ($param['type'] == 5) {
                $type = 2;
            }elseif ($param['type'] == 6) {
                $type = 3;
            }elseif ($param['type'] == 7) {
                $type = 4;
            }elseif ($param['type'] == 9) {
                $type = 5;
            }elseif ($param['type'] == 10) {
                $type = 7;
            }else{
                $type = 6;
            }
            // 修改库存
            $find = Db::name('member_pay')->where(['id' => $param['id'], 'type' => $type])->find();
            if (empty($find)) {
                Db::name('member_pay')->save(['id' => $param['id'], 'type' => $type,'num' => $param['num']]);
            }else{
                Db::name('member_pay')->where(['id' => $param['id'], 'type' => $type])->update(['num' => $param['num']]);
            }
            // 记录订单
            Db::name('order')->save(['u_id' => $param['id'], 'p_id' => $userInfo['id'], 'c_id' => $param['type'], 'type' => 4, 'state' => 1, 'num' => $param['num']-$find['num'], 'time' => time()]);
            $desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员信息用户id【'.$param['id'].'】 修改内容如下/库存修改为:'.$param['num'].'。';
            BackstageLog($userInfo['id'], '修改用户信息', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 总部修改用户信息
     * @Author: 林怼怼
     * @Date: 2024/3/12/012
     * @Time: 13:50
     */
    public function setUserInfo()
    {
        $param = input();
        $pay = input('pay');
        $userInfo = getUserInfo(true);
        if ($userInfo['id'] != 1) return json(['code' => 201, 'msg' => '权限不足。']);
        // 启动事务
        Db::startTrans();
        try {
            // 修改手机号
            Db::name('member')->save([
                'id' => $param['id'],
                'name' => $param['name'],
                'phone' => $param['phone'],
            ]);

            // 修改推荐人
            if ($param['p_id'] === $param['id']) return json(['code' => 201, 'msg' => '归属不能是用户本人。']);
            Db::name('member_parent')
                ->where(['id' => $param['id']])
                ->update(['p_id' => $param['p_id']]);
            // 修改库存
            foreach ($pay as $val) {
                if ($val['id'] == 4) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 1])->update(['num' => $val['num']]);
                }elseif ($val['id'] == 5) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 2])->update(['num' => $val['num']]);
                }elseif ($val['id'] == 6) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 3])->update(['num' => $val['num']]);
                }elseif ($val['id'] == 7) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 4])->update(['num' => $val['num']]);
                }elseif ($val['id'] == 9) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 5])->update(['num' => $val['num']]);
                }elseif ($val['id'] == 10) {
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 7])->update(['num' => $val['num']]);
                }else{
                    Db::name('member_pay')->where(['id' => $param['id'], 'type' => 6])->update(['num' => $val['num']]);
                }
            }
            $desc = '会员'.$userInfo['name'].'['. date('Y年m月d日 H时i分s秒') .']'.'修改会员信息用户id【'.$param['id'].'】 修改内容如下：用户名'. $param['name'].'、电话'.$param['phone'].'、推荐人id'.$param['p_id'].'。';
            BackstageLog($userInfo['id'], '修改用户信息', $desc);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 取消关注
     * @Author: 林怼怼
     * @Date: 2024/1/18/018
     * @Time: 14:50
     */
    public function del_concern()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo(true);
            Db::name('concern')->where(['id' => $userInfo['id'], 'u_id' => $id])->delete();
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 用户关注列表
     * @Author: 林怼怼
     * @Date: 2024/1/18/018
     * @Time: 14:44
     */
    public function list_concern()
    {
        $param = input();

        $userInfo = getUserInfo(true);
        if ($param['type'] == 1) {
            $ids = Db::name('concern')->where(['u_id' => $userInfo['id']])->column('u_id');
        }elseif($param['type'] == 2){
            $ids = Db::name('evaluate')
                ->where(['u_id' => $userInfo['id'], 'state' => 1])
                ->column('id');
            $comments = Db::name('support')
                ->whereIn('p_id', $ids)
                ->where(['pandn' => 1])
                ->order([
                    'time' => 'desc'
                ])
                ->select();
            $count = Db::name('support')
                ->whereIn('p_id', $ids)
                ->where(['pandn' => 1])
                ->order([
                    'time' => 'desc'
                ])
                ->count();
            $result = [];
            foreach ($comments as $item) {
                $data = Db::name('evaluate')->where('id', $item['p_id'])->find();
                $userInfo1 = Db::name('member')->where('id', $item['u_id'])->find();
                $result[] = [
                    'comment_id' => $data['p_id'],
                    'comment_content' => $data['desc'],
                    'comment_img' => $data['img'],
                    'comment_video' => $data['video'],
                    'score' => $data['score'],
                    'poster' => $data['poster'],
                    'type' => $data['type'],
                    'time' => $item['time'],
                    'userName' => $userInfo1['name'],
                    'avatar' => $userInfo1['avatar'],
                    'id' => $userInfo1['id'],
                ];
            }

            return json([
                'code' => 200,
                'msg' => '成功.',
                'data' => $result,
                'count' => $count
            ]);
        }else{
            $ids = Db::name('concern')->where(['id' => $userInfo['id']])->column('u_id');
        }
        $data = Db::name('member')
            ->alias('t1')
            ->whereIn('t1.id', $ids)
            ->field('t1.id, name, avatar, phone, state, t1.time')
            ->page((int) $param['page'], (int) $param['limit'])
            ->select()
            ->toArray();
        $count = Db::name('member')
            ->whereIn('id', $ids)
            ->count();
        foreach ($data as $key => &$val) {
            $val['level'] = array_level($val['id']);
        }
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 用户点击关注
     * @Author: 林怼怼
     * @Date: 2024/1/18/018
     * @Time: 14:32
     */
    public function save_concern()
    {
        $id = input('id');
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = getUserInfo(true);
            $res = Db::name('concern')->where(['id' => $userInfo['id'], 'u_id' => $id])->find();
            if ($res) return json(['code' => 201, 'msg' => '已关注，无需重复操作。']);
            Db::name('concern')->insert(['id' => $userInfo['id'], 'u_id' => $id, 'time' => time()]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '操作成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 用户忘记密码
     * @Author: 林怼怼
     * @Date: 2023/12/27/027
     * @Time: 11:23
     */
    public function forgot_pwd()
    {
        $param = input();
        // 启动事务
        Db::startTrans();
        try {
            $userInfo = Db::name('member')->where('phone', $param['phone'])->find();
            if (!$userInfo) {
                return json(['code' => 201, 'msg' => '手机号错误或者未注册。']);
            }else{
                // 查询之前存好的验证码
                $code = Cache::get($param['phone']);
                if ($code !== $param['code']) return json(['code' => 201, 'msg' => '验证码不正确。']);
                Db::name('member')->save(['id' => $userInfo['id'], 'pwd' => encrypt_password($param['pwd'])]);
            }
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '修改密码成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }
    }

    /**
     * @Description: 用户自己排名
     * @Author: 林怼怼
     * @Date: 2023/12/23/023
     * @Time: 10:02
     */
    public function user_rank()
    {
        $level = input('level', 3);
        // 当前用户的信息
        $userInfo = getUserInfo(true);
        if (!in_array((int)$level, $userInfo['level']))  return json(['code' => 201, 'msg' => '请开通对应产品代理']);
        $data = [];
        // 查询全部下级
        $ids = getChildUserPhoneS($userInfo['id']);
        $data['userInfo'] = [
            'id' => $userInfo['id'],
            'name' => $userInfo['name'],
            'phone' => $userInfo['phone'],
            'avatar' => $userInfo['avatar'],
        ];
        // 子查询条件
        $where = [];
        $where[] = ['t2.grade', 'like', '%'. $level .'%']; // 条件2: 查询中心级别
        $where[] = ['t1.state', '=', 1]; // 条件3: 查询正常的中心
        // 查询全部中心
        $rank = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_address t3", 't1.id = t3.id')
            ->leftJoin("member_grade t2", 't1.id = t2.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone')
            ->where($where)
            ->select()
            ->toArray();
        foreach ($rank as &$value) {
            $ids = getChildUserPhoneS($value['id']);
            $whereGrade = [];
            $whereGrade[] = ['t1.id', 'in', $ids];
            $whereGrade[] = ['t2.grade', 'not in', [3,5,6,9,11]];
            if ($level == 3) {
                // 思享荟代理
                $whereGrade[] = ['t2.grade', '=', 1];
            }elseif($level == 5) {
                // 微笑长
                $whereGrade[] = ['t2.grade', 'in', [4]];
            }elseif($level == 6) {
                // 笑长
                $whereGrade[] = ['t2.grade', 'in', [4,5]];
            }elseif ($level == 9) {
                // 悦读慧代理
                $whereGrade[] = ['t2.grade', '=', 7];
            }elseif ($level == 11) {
                // 站长
                $whereGrade[] = ['t2.grade', '=', 10];
            }else{
                return json(['code' => 201, 'msg' => '错误：只有代理级别排行。']);
            }
            $whereGrade[] = ['t1.state', '=', 1];
            $ids = Db::name('member')
                ->alias('t1')
                ->join('member_grade t2', 't1.id = t2.id')
                ->where($whereGrade)
                ->column('t1.id');
            $value['num'] = count($ids);
        }
        $rank = bubbleSortDesc($rank, 'num');
        $count = 0;
        $data['rank_one'] = [];
        $data['rank_two'] = [];
        $data['rank_three'] = [];
        foreach ($rank as $key => $val) {
            // 自己的排行
            if ($val['phone'] === $userInfo['phone']) {
                $data['userInfo']['num'] = $val['num'];
                $data['userInfo']['rank'] = $key + 1;
                if ($key === 0) {
                    $count = 0;
                }else{
                    $count += $rank[$key - 1]['num'] - $data['userInfo']['num']+1;
                }
            }
            // 排行第一
            if ($key === 0) {
                $data['rank_one']['id'] = $val['id'];
                $data['rank_one']['name'] = $val['name'];
                $data['rank_one']['avatar'] = $val['avatar'];
                $data['rank_one']['phone'] = $val['phone'];
                $data['rank_one']['num'] = $val['num'];
                $data['rank_one']['rank'] = $key + 1;
            }
            // 排行第二
            if ($key === 1) {
                $data['rank_two']['id'] = $val['id'];
                $data['rank_two']['name'] = $val['name'];
                $data['rank_two']['avatar'] = $val['avatar'];
                $data['rank_two']['phone'] = $val['phone'];
                $data['rank_two']['num'] = $val['num'];
                $data['rank_two']['rank'] = $key + 1;
            }
            // 排行第三
            if ($key === 2) {
                $data['rank_three']['id'] = $val['id'];
                $data['rank_three']['name'] = $val['name'];
                $data['rank_three']['avatar'] = $val['avatar'];
                $data['rank_three']['phone'] = $val['phone'];
                $data['rank_three']['num'] = $val['num'];
                $data['rank_three']['rank'] = $key + 1;
            }
        }
        return json(['code' => 200, 'msg' => '成功。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 修改密码
     * @Author: 林怼怼
     * @Date: 2023/12/18/018
     * @Time: 9:18
     */
    public function updatePwd()
    {
        // 启动事务
        Db::startTrans();
        try {
            $param = input();
            $userInfo = getUserInfo(true);
            if ($userInfo['phone'] !== $param['phone']) return json(['code' => 201, 'msg' => '手机号与当前登录的账号不符。']);
            # 查询之前存好的验证码
            $code = Cache::get($param['phone']);
            if ($code !== $param['code']) return json(['code' => 201, 'msg' => '验证码不正确。']);
            Db::name('member')->save(['id' => $userInfo['id'], 'pwd' => encrypt_password($param['pwd'])]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '修改密码成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 用户修改手机号
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 11:44
     */
    public function updatePhone()
    {
        // 启动事务
        Db::startTrans();
        try {
            $param = input();
            $userInfo = getUserInfo(true);
            // 查询之前存好的验证码
            $code = Cache::get($param['phone']);
            if ($code !== $param['code']) return json(['code' => 201, 'msg' => '验证码不正确。']);
            $data = Db::name('member')->where('phone',$param['phone'])->find();
            if ($data) return json(['code' => 201, 'msg' => '该手机号已注册。']);
            Db::name('member')->save(['id' => $userInfo['id'], 'phone' => $param['phone']]);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '修改手机号成功。']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 获取会员信息
	 * @Author: Linxy
	 * @Time: 2023/11/30 15:48
	 */
	public function read()
	{
        $id = input('id');
        $p_id = input('p_id', 0);
        if (empty($id)) {
            $userInfo = getUserInfo(true);
        }else{
            $userInfo = Db::name('member')->where('id', $id)->find();
        }
        $data = Db::name('member')
            ->alias('t1')
            ->leftJoin('member_address t2', 't1.id = t2.id AND t2.is_master = 1 AND t2.state = 1')
            ->leftJoin('city t3', 't2.p_id = t3.id')
            ->leftJoin('city t4', 't2.c_id = t4.id')
            ->leftJoin('city t8', 't2.c_id = t8.id')
            ->leftJoin('city t5', 't2.d_id = t5.id')
            ->leftJoin('user_role t6', 't1.id = t6.id')
            ->leftJoin('role t7', 't6.r_id = t7.id')
            ->leftJoin('member_parent t10', 't1.id = t10.id')
            ->leftJoin('member t9', 't10.p_id = t9.id')
            ->fieldRaw('t1.id, t1.name, t1.pinyin, t1.openid, t1.avatar, t1.phone, t10.p_id, t9.name as userParentTop, t1.number, t1.desc, IFNULL(t7.code, "") as roles, t2.a_id, IFNULL(t3.cityName, "") province_name, t3.id as province_id, IFNULL(t4.cityName, "") city_name, t4.id as city_id, IFNULL(t5.cityName, "") area_name, t5.id as d_id, t2.address, t1.is_teacher, t1.state, t1.is_admin, t1.time')
            ->find($id);
        // 获取等级
        $data['level'] = array_level($data['id']);
        $data['pay1'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 1])->value('num');
        $data['pay2'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 2])->value('num');
        $data['pay3'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 3])->value('num');
        $data['pay4'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 4])->value('num');
        $data['pay6'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 6])->value('num');
        $data['evaluate1'] = Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 1, 'type' => 2, 'state' => 1])->count();
        $data['evaluate2'] = Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 2, 'type' => 2, 'state' => 1])->count();
        $data['evaluate3'] = Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 3, 'type' => 2, 'state' => 1])->count();
        $data['evaluate4'] = Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 4, 'type' => 2, 'state' => 1])->count();
        $data['evaluate5'] = Db::name('evaluate')->where(['p_id' => $data['id'], 'score' => 5, 'type' => 2, 'state' => 1])->count();
        if (in_array(3, $data['level']) || in_array(5, $data['level']) || in_array(6, $data['level']) || in_array(9, $data['level']) || in_array(11, $data['level'])){
            $data['userParentMaster'] = Db::name('member')->where('id', 1)->value('name');
        }else{
            $data['userParentMaster'] = getParent_365_521($data['id'])['name'];
        }
        $data['concern'] = 0;
        $res = Db::name('concern')->where([
            'id' => $p_id,
            'u_id' => $id,
        ])->find();
        if ($res) $data['concern'] = 1;
		return json(['code' => 200, 'msg' => '获取会员信息。', 'data' => $data]);
	}

    /**
     * @Description: 查看课程报名量
     * @Author: 林怼怼
     * @Date: 2024/4/12/012
     * @Time: 9:57
     */
    public function getUserCourseChart()
    {
        $data = [];
        $userInfo = getUserInfo(true);
        $course = Db::name('course')->where(['state' => 1])->select()->toArray();
        foreach ($course as &$item) {
            $count = Db::name('order')
                ->where(['p_id' => $userInfo['id'], 'c_id' => $item['id'], 'state' => 1, 'type' => 5])
                ->count();
            $data[] = ['value' => $count, 'name' => $item['title']];
        }
        return json(['code' => 200, 'msg' => '查询成功！', 'data' => $data]);
    }

    /**
     * @Description: 根据地区显示每个区得用户量
     * @Author: 林怼怼
     * @Date: 2024/4/12/012
     * @Time: 9:57
     */
    public function getUserCityChart()
    {
        $data = [];
        $userInfo = getUserInfo(true);
        $city = Db::name('city')->where(['parentId' => 100000])->select()->toArray();
        $ids = getChildUserPhoneS($userInfo['id']);
        foreach ($city as &$item) {
            $count = Db::name('member_address')
                ->where(['id' => $ids, 'p_id' => $item['id'], 'is_master' => 1, 'state' => 1])
                ->count();
            $data[] = ['value' => $count, 'name' => $item['cityName']];
        }
        return json(['code' => 200, 'msg' => '查询成功！', 'data' => $data]);
    }

    /**
     * @Description: 漏斗图
     * @Author: 林怼怼
     * @Date: 2024/4/12/012
     * @Time: 9:14
     */
    public function getUserFunnelChart()
    {
        $userInfo = getUserInfo(true);
        $weekStart = input('start_time');
        $weekEnd = input('end_time');
        if (empty($weekStart)) $weekStart = date('Y-m-d 00:00:01', strtotime('-5 years'));
        if (empty($weekEnd)) $weekEnd = date('Y-m-d 23:59:59', strtotime('today'));
        $data = [];
        $ids = getChildUserPhoneS($userInfo['id']);
        // 新用户
        $count = Db::name('member')
            ->alias('t1')
            ->where([
                ['t1.time', 'between time', [$weekStart, $weekEnd]],
                ['t1.id', 'in', $ids],
            ])
            ->group('t1.id')
            ->count();
        $data[] = ['value' => $count, 'name' => '新客户'];
        // 流失客户
        $count = Db::name('member')
            ->alias('t1')
            ->where([
                ['t1.time', 'between time', [$weekStart, $weekEnd]],
                ['t1.state', '=', 2],
                ['t1.id', 'in', $ids],
            ])
            ->where('state', '2')
            ->group('t1.id')
            ->count();
        $data[] = ['value' => $count, 'name' => '流失客户'];
        // 续费客户
        $count = Db::name('member')
            ->alias('t1')
            ->join('order t2', 't1.id = t2.id')
            ->where([
                ['t1.time', 'between time', [$weekStart, $weekEnd]],
                ['t2.type', '=', 2],
                ['t2.p_id', '=', $userInfo['id']],
            ])
            ->where('type', '2')
            ->group('t1.id')
            ->count();
        $data[] = ['value' => $count, 'name' => '续费客户'];
        $data = bubbleSortDesc($data, 'value');
        return json(['code' => 200, 'msg' => '查询成功！', 'data' => $data]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 折线图
	 * @Author: Linxy
	 * @Time: 2023/11/25 12:56
	 */
	public function getUserQuotaChart()
	{
		$param = input();
		$weekStart = input('start_time', date('Y-m-d', strtotime('this week Monday')));
		$weekEnd = input('end_time', date('Y-m-d', strtotime('this week Sunday')));
		$userInfo = getUserInfo(true);
        $where = [];
        $where2 = [];
        $where[] = ['p_id', '=', $userInfo['id']];
        $where[] = ['type', '=', 4];
        $where[] = ['state', '=', 1];
        $where2[] = ['t1.state', '=', 1];
        $ids = getChildUserPhoneS($userInfo['id']);
        $where2[] = ['t1.id', 'in', $ids];
        if (!empty($param['c_id']))$where2[] = ['t2.grade', '=', $param['c_id']];
		if ($param['type'] == 1) {
			// 总部账号每天卖出的名额
			$currentDate = $weekStart;
			while ($currentDate <= $weekEnd) {
				// 查询每天的总销量
				$result = Db::name('order')
					->whereDay('time', $currentDate)
					->where($where)
					->field("IFNULL(SUM(num), 0) AS total_amount")
					->find();
				$data[] = ['time' => $currentDate, 'total_amount' => (int) $result['total_amount']];
				$currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
			}
		}elseif ($param['type'] == 2) {
			// 总部账号每月卖出的名额
			$currentDate = $weekStart;
			while ($currentDate <= $weekEnd) {
				// 查询每月的总销量
				$result = Db::name('order')
					->whereMonth('time', $currentDate)
                    ->where($where)
					->field("IFNULL(SUM(num), 0) AS total_amount")
					->find();
				$data[] = ['time' => $currentDate, 'total_amount' => (int) $result['total_amount']];
				$currentDate = date('Y-m', strtotime($currentDate . ' +1 month'));
			}
		}elseif ($param['type'] == 3) {
			// 每日新增会员
			$currentDate = $weekStart;
			while ($currentDate <= $weekEnd) {
				// 查询每日注册量
				$count = Db::name('member')
                    ->alias('t1')
                    ->join('member_grade t2', 't1.id = t2.id')
					->whereDay('t1.time', $currentDate)
                    ->where($where2)
                    ->group('t1.id')
					->count();
				$data[] = ['time' => $currentDate, 'total_amount' => (int) $count];
				$currentDate = date('Y-m-d', strtotime($currentDate . ' +1 day'));
			}
		}elseif ($param['type'] == 4) {
			// 每月新增会员
			$currentDate = $weekStart;
			while ($currentDate <= $weekEnd) {
				// 查询每月注册量
				$count = Db::name('member')
                    ->alias('t1')
                    ->join('member_grade t2', 't1.id = t2.id')
                    ->whereDay('t1.time', $currentDate)
                    ->where($where2)
                    ->group('t1.id')
					->count();
				$data[] = ['time' => $currentDate, 'total_amount' => (int) $count];
				$currentDate = date('Y-m', strtotime($currentDate . ' +1 month'));
			}
		}else{
			return json(['code' => 201, 'msg' => '参数错误！']);
		}
		return json(['code' => 200, 'msg' => '查询成功！', 'data' => $data]);
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 我的申请列表
	 * @Author: Linxy
	 * @Time: 2023/11/16 17:38
	 */
	public function checkList()
	{
		$param = input();
        $where = [];
        $userInfo = getUserInfo(true);
        $where[] = ['t1.u_id', '=', $userInfo['id']];
		// 状态筛选
        if ($param['state'] != '') $where[] = ['t1.state', '=', (int) $param['state']];
        // 类型筛选
		if (!empty($param['type'])) $where[] = ['t1.type', 'in', $param['type']];
        if ($param['type'] != 5) {
            $subQuery = Db::table('jw_product')
                ->field('id, title, "" as course_imgs')
                ->buildSql();
        }else{
            $subQuery = Db::table('jw_course')
                ->field('id, title, IFNULL(img, "") as course_imgs')
                ->buildSql();
        }
        $data = Db::name('order')
            ->alias('t1')
            ->join('member t2', 't1.p_id = t2.id')
            ->join("$subQuery t3", 't1.c_id = t3.id')
            ->field('t1.*, t2.name userName, t2.phone, t2.avatar, t3.title, course_imgs')
            ->where($where)
            ->page((int)$param['page'], (int)$param['limit'])
            ->order('t1.time', 'desc')
            ->select()->toArray();
        $count = Db::name('order')
            ->alias('t1')
            ->join('member t2', 't1.p_id = t2.id')
            ->join("$subQuery t3", 't1.c_id = t3.id')
            ->where($where)
            ->count();
        foreach ($data as &$datum) {
            if (empty($datum['image'])) {
                $datum['img'] = [];
            }else{
                $datum['img'] = explode(',', $datum['image']);
            }
        }
        return json(['code' => 200, 'msg' => '我的申请列表获取成功。', 'data' => $data, 'count' => $count]);
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 发送短信验证码
	 * @Author: Linxy
	 * @Time: 2023/11/14 11:07
	 */
	public function sendVerifyCode()
	{
		$phone = input('phone');
		// 生成随机验证码
		$code = mt_rand(100000, 999999);
		// 发送短信
		try {
			AlibabaCloud::accessKeyClient(Config::get('aliyun.access_key_id'), Config::get('aliyun.access_key_secret'))
				->regionId('cn-hangzhou')
				->asDefaultClient();

			AlibabaCloud::rpc()
				->product('Dysmsapi')
				->scheme('https')
				->version('2017-05-25')
				->action('SendSms')
				->method('POST')
				->options([
					'query' => [
						'PhoneNumbers' => $phone,
						'SignName' => Config::get('aliyun.sign_name'),
						'TemplateCode' => Config::get('aliyun.template_code'),
						'TemplateParam' => json_encode(['code' => $code]),
					],
				])
				->request();

			// 短信发送成功，将验证码保存到数据库等操作
			Cache::set($phone, $code, 600);
			return json(['code' => 200, 'msg' => '短信验证码发送成功']);
		} catch (ClientException $e) {
			return json(['code' => 201, 'msg' => '短信验证码发送失败：' . $e->getErrorMessage()]);
		} catch (ServerException $e) {
			return json(['code' => 202, 'msg' => '短信验证码发送失败：' . $e->getErrorMessage()]);
		}
	}

    /**
     * Created by PhpStorm.
     * @purpose 上傳视频
     * @Author: Linxy
     * @Time: 2023/10/17 15:52
     */
    public function video()
    {
        try {
            // 验证文件
//            $this->validateFile($_FILES['file']);

            // 获取参数
            $params = $this->validateParams();

            // 创建目录
            $chunkPath = $this->createChunkDirectory($params['identifier']);

            // 保存分片
            $this->saveChunk($chunkPath, $params);

            // 如果是最后一个分片，合并文件
            if ($params['chunkNumber'] == $params['totalChunks']) {
                $lockFile = $chunkPath . '.lock';
                $lock = fopen($lockFile, 'w+');

                if (!flock($lock, LOCK_EX | LOCK_NB)) {
                    throw new \Exception('文件正在处理中');
                }

                try {
                    // 合并文件
                    $finalFile = $this->mergeChunks(
                        $chunkPath,
                        $params['filename'],
                        $params['totalChunks']
                    );

                    // 上传到OSS
                    $ossPath = 'videos/' . date('Ymd') . '/' . basename($finalFile);
                    $fileUrl = uploadFile($ossPath, $finalFile);

                    // 清理临时文件
                    @unlink($finalFile);

                    // 确保所有分片文件都被删除
                    $this->cleanupChunks($chunkPath, $params);

                    // 删除分片目录
                    $this->removeDirectory($chunkPath);

                    return json([
                        'code' => 200,
                        'msg' => 'success',
                        'data' => $fileUrl
                    ]);

                } finally {
                    // 释放锁
                    flock($lock, LOCK_UN);
                    fclose($lock);
                    @unlink($lockFile);
                }
            }

            return json(['code' => 200, 'msg' => 'chunk uploaded']);

        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'error' => '视频上传失败'
            ]);
            return json(['code' => 500, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * 获取上传错误信息
     */
    private function getUploadError($error)
    {
        switch ($error) {
            case UPLOAD_ERR_INI_SIZE:
                return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
            case UPLOAD_ERR_FORM_SIZE:
                return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
            case UPLOAD_ERR_PARTIAL:
                return 'The uploaded file was only partially uploaded';
            case UPLOAD_ERR_NO_FILE:
                return 'No file was uploaded';
            case UPLOAD_ERR_NO_TMP_DIR:
                return 'Missing a temporary folder';
            case UPLOAD_ERR_CANT_WRITE:
                return 'Failed to write file to disk';
            case UPLOAD_ERR_EXTENSION:
                return 'File upload stopped by extension';
            default:
                return 'Unknown upload error';
        }
    }

    /**
     * 清理分片文件
     * @param string $chunkPath 分片目录
     * @param array $params 上传参数
     */
    private function cleanupChunks($chunkPath, $params)
    {
        try {
            $filename = pathinfo($params['filename'], PATHINFO_FILENAME);

            // 删除所有分片文件
            for ($i = 1; $i <= $params['totalChunks']; $i++) {
                $chunkFile = $chunkPath . DIRECTORY_SEPARATOR . $filename . '.part' . $i;
                if (file_exists($chunkFile)) {
                    @unlink($chunkFile);
                }
            }

            // 删除其他可能存在的临时文件
            $files = glob($chunkPath . DIRECTORY_SEPARATOR . '*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        } catch (\Exception $e) {
            Log::error('清理分片文件失败', [
                'dir' => $chunkPath,
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * 验证上传文件
     */
    private function validateFile($file)
    {
        if (!isset($file['error']) || $file['error'] !== UPLOAD_ERR_OK) {
            throw new \Exception('文件上传失败');
        }

        if (!in_array($file['type'], $this->allowedTypes)) {
            throw new \Exception('不支持的文件类型');
        }

        if ($file['size'] > $this->maxFileSize) {
            throw new \Exception('文件大小超出限制');
        }
    }

    /**
     * 验证上传参数
     */
    private function validateParams()
    {
        $params = [
            'chunkNumber' => input('resumableChunkNumber/d'),
            'totalChunks' => input('resumableTotalChunks/d'),
            'identifier' => input('resumableIdentifier/s'),
            'filename' => input('resumableFilename/s')
        ];

        foreach ($params as $key => $value) {
            if (empty($value)) {
                throw new \Exception("参数 {$key} 不能为空");
            }
        }

        // 过滤文件名
        $params['filename'] = $this->sanitizeFilename($params['filename']);

        return $params;
    }

    /**
     * 创建分片存储目录
     */
    private function createChunkDirectory($identifier)
    {
        $chunkPath = runtime_path('uploads/chunks/' . $identifier);

        if (!is_dir($chunkPath)) {
            if (!mkdir($chunkPath, 0777, true)) {
                throw new \Exception('无法创建上传目录');
            }
        }

        return $chunkPath;
    }

    /**
     * 保存分片文件
     */
    private function saveChunk($chunkPath, $params)
    {
        $filename = pathinfo($params['filename'], PATHINFO_FILENAME);
        $chunkFile = $chunkPath . '/' . $filename . '.part' . $params['chunkNumber'];

        if (!move_uploaded_file($_FILES['file']['tmp_name'], $chunkFile)) {
            throw new \Exception('保存分片文件失败');
        }
    }

    /**
     * 合并分片文件
     */
    private function mergeChunks($chunkPath, $filename, $totalChunks)
    {
        // 生成最终文件名
        $finalFilename = md5($filename . time() . uniqid()) . '.mp4';
        $uploadDir = runtime_path('uploads');

        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        $finalFile = $uploadDir . DIRECTORY_SEPARATOR . $finalFilename;

        // 如果文件已存在，先删除
        if (file_exists($finalFile)) {
            unlink($finalFile);
        }

        // 打开最终文件
        $fp = fopen($finalFile, 'wb');
        if (!$fp) {
            throw new \Exception('无法创建最终文件');
        }

        try {
            // 合并所有分片
            for ($i = 1; $i <= $totalChunks; $i++) {
                $chunkFile = $chunkPath . DIRECTORY_SEPARATOR .
                    pathinfo($filename, PATHINFO_FILENAME) .
                    '.part' . $i;

                if (!file_exists($chunkFile)) {
                    throw new \Exception("分片文件丢失: part{$i}");
                }

                // 使用流式读写，减少内存占用
                $handle = fopen($chunkFile, 'rb');
                while (!feof($handle)) {
                    fwrite($fp, fread($handle, 8192));
                }
                fclose($handle);

                // 删除分片
                unlink($chunkFile);
            }
        } finally {
            fclose($fp);
        }

        return $finalFile;
    }

    /**
     * 递归删除目录及其内容
     * @param string $dir 目录路径
     * @return bool
     */
    private function removeDirectory($dir)
    {
        try {
            if (!is_dir($dir)) {
                return false;
            }

            // 遍历目录中的所有文件和子目录
            $files = array_diff(scandir($dir), ['.', '..']);

            foreach ($files as $file) {
                $path = $dir . DIRECTORY_SEPARATOR . $file;

                // 如果是目录，递归删除
                if (is_dir($path)) {
                    $this->removeDirectory($path);
                } else {
                    // 如果是文件，直接删除
                    if (file_exists($path)) {
                        @unlink($path);
                    }
                }
            }

            // 删除空目录
            return @rmdir($dir);

        } catch (\Exception $e) {
            Log::error('删除目录失败', [
                'dir' => $dir,
                'error' => $e->getMessage()
            ]);
            return false;
        }
    }

    /**
     * 过滤文件名
     */
    private function sanitizeFilename($filename)
    {
        // 移除特殊字符
        $filename = preg_replace('/[^a-zA-Z0-9\-_.]/', '', $filename);
        // 防止目录遍历
        $filename = str_replace('..', '', $filename);
        return $filename;
    }

    /**
     * 清理过期的临时文件
     */
    private function cleanupTempFiles()
    {
        $chunksDir = runtime_path('uploads/chunks');
        if (!is_dir($chunksDir)) {
            return;
        }

        // 清理24小时前的临时文件
        $expire = time() - 86400;

        $dirs = glob($chunksDir . '/*', GLOB_ONLYDIR);
        foreach ($dirs as $dir) {
            if (filemtime($dir) < $expire) {
                $this->removeDirectory($dir);
            }
        }
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 上傳文件
	 * @Author: Linxy
	 * @Time: 2023/10/17 15:52
	 */
	public function upload(Request $request)
	{
		// 启动事务
		Db::startTrans();
		try {
			$files = $_FILES['file'];
			// 判断文件类型
			$ext = strtolower(pathinfo(@$files['name'], PATHINFO_EXTENSION));
			if (!in_array($ext, ['jpg', 'png', 'jpeg', 'gif'])) {
				return json(['code'=>201,'msg'=>'文件类型错误！']);
			}
			// 临时存放路径
			$path = $_SERVER['DOCUMENT_ROOT']."/uploads/";
			// 判断是否存在上传到的目录
			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}
			// 生成唯一的文件名
			$fileName = md5(date('Y-m-d H:i:s:u')) . '.' . $ext;
			// 将文件名拼接到指定的目录下
			$destName = $path . $fileName;
			// 进行文件移动
			if (!move_uploaded_file($files['tmp_name'], $destName)) {
				return json(['code'=>201,'msg'=>'文件上传失败！']);
			}
			// 上传到oss
			$result = uploadFile($fileName, $destName);
			// 删除临时文件
			unlink($destName);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '上传文件成功。', 'img' => $result['info']['url']]);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 修改用户信息
	 * @Author: Linxy
	 * @Time: 2023/10/26 9:58
	 */
	public function edit()
	{
		$param = input();
		// 启动事务
		Db::startTrans();
		try {
			$data = [];
			$data['id'] = $param['id'];
			if (!empty($param['name'])) $data['name'] = $param['name'];
			if (!empty($param['avatar'])) $data['avatar'] = $param['avatar'];
			if (!empty($param['province'])) $data['province'] = $param['province'];
			if (!empty($param['sex'])) $data['sex'] = $param['sex'];
			if (!empty($param['desc'])) $data['desc'] = $param['desc'];
			Db::name('member')->save($data);
			// 提交事务
			Db::commit();
			return json(['code' => 200, 'msg' => '修改用户信息成功。']);
		} catch (\Throwable $e){
			// 回滚事务
			Db::rollback();
			// 这是进行异常捕获
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 中心数据排行
	 * @Author: Linxy
	 * @Time: 2023/10/20 11:16
	 */
	public function ranking()
	{
		$param = input();
		$userInfo = getUserInfo(true);
        $start_time = strtotime($param['start_time'] == '' ? date("Y-m-d 00:00:01") : $param['start_time'] . ' 00:00:01');
        $end_time = strtotime($param['end_time'] == '' ? date("Y-m-d 23:59:59") : $param['end_time'] . ' 23:59:59');

        if ($userInfo['id'] !== 1) return json(['code' => 201, 'msg' => '级别不足无法访问。']);
		$where[] = ['t2.grade', 'in', $param['level']]; // 条件2: 查询中心级别
		$where[] = ['t1.state', '=', 1]; // 条件3: 查询正常的中心
        $num = 0;
		if ($param['type'] == 2) {
			// 会员排行（中心审核过的人）
            // 子查询条件
            $where = [];
            // 条件2: 查询中心级别
            if (!empty($param['level'])) {
                $where[] = ['t2.grade', 'like', '%'.$param['level'].'%'];
            }else{
                $where[] = ['t2.grade', 'in', [3,5,6,9,11]];
            }
            // 条件3: 查询正常的中心
            $where[] = ['t1.state', '=', 1];
            // 查询全部中心
            $rank = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_address t3", 't1.id = t3.id')
                ->leftJoin("member_grade t2", 't1.id = t2.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone')
                ->where($where)
                ->group('t1.id')
                ->select()
                ->toArray();
            foreach ($rank as &$value) {
                $value['level'] = array_level($value['id']);
                $ids = getChildUserPhoneS($value['id']);
                $whereGrade = [];
                $whereGrade[] = ['t1.id', 'in', $ids];
                $whereGrade[] = ['t1.state', '=', 1];
                if ($param['level'] == 3) {
                    // 思享荟代理
                    $whereGrade[] = ['t2.grade', 'in', [1]];
                }elseif($param['level'] == 5) {
                    // 微笑长
                    $whereGrade[] = ['t2.grade', 'in', [4]];
                }elseif($param['level'] == 6) {
                    // 笑长
                    $whereGrade[] = ['t2.grade', 'in', [4,5]];
                }elseif($param['level'] == 9) {
                    // 悦读慧
                    $whereGrade[] = ['t2.grade', 'in', [7]];
                }elseif($param['level'] == 11) {
                    // 悦读慧
                    $whereGrade[] = ['t2.grade', 'in', [10]];
                }else{
                    return json(['code' => 201, 'msg' => '错误：只有代理级别排行。']);
                }
                $ids = Db::name('member')
                    ->alias('t1')
                    ->join('member_grade t2', 't1.id = t2.id')
                    ->where($whereGrade)
                    ->column('t1.id');
                $value['num'] = count($ids);
            }
            $rank = bubbleSortDesc($rank, 'num');
            // 分页参数
            $page = (int)$param['page']; // 当前页数
            $pageSize = (int)$param['limit']; // 每页显示的记录数

            // 计算分页起始索引
            $startIndex = ($page - 1) * $pageSize;

            // 使用 array_slice 函数获取指定页数的数据
            $data = array_slice($rank, $startIndex, $pageSize);
            $count = count($rank);
		}elseif ($param['type'] == 6) {
            // 每个代理卖出的预购排行
            $where = [];
            // 条件2: 查询中心级别
            if (!empty($param['level'])) {
                $where[] = ['t2.grade', 'like', '%'.$param['level'].'%'];
            }else{
                $where[] = ['t2.grade', 'in', [3,5,6,9,11]];
            }
            $where[] = ['t1.state', '=', 1];

            // 子查询
            $subQuery = Db::name('order')
                ->alias('t1')
                ->where([
                    ['type', '=', 4],
                    ['state', '=', 1],
                    ['c_id', '=', $param['c_id']],
                    ['time', 'between time', [$start_time, $end_time]]
                ])
                ->field('t1.p_id, SUM(t1.num) num')
                ->group('t1.p_id')
                ->buildSql();
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_grade t2', 't1.id = t2.id')
                ->leftJoin("$subQuery t3", 't1.id = t3.p_id')
                ->where($where)
                ->field('t1.id, t1.name, t1.avatar, t1.phone, IFNULL(num, 0) num, t1.state, t1.time')
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('num', $param['order'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_grade t2', 't1.id = t2.id')
                ->leftJoin("$subQuery t3", 't1.id = t3.p_id')
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['type'] == 7) {
            // 每个代理卖出的预购排行
            $where = [];
            // 条件2: 查询中心级别
            if (!empty($param['level'])) {
                $where[] = ['t2.grade', 'like', '%'.$param['level'].'%'];
            }else{
                $where[] = ['t2.grade', 'in', [3,5,6,9,11]];
            }
            $where[] = ['t1.state', '=', 1];
            if ($param['c_id'] == 4) {
                $where[] = ['t3.type', '=', 1];
            }elseif ($param['c_id'] == 5) {
                $where[] = ['t3.type', '=', 2];
            }elseif ($param['c_id'] == 6) {
                $where[] = ['t3.type', '=', 3];
            }elseif ($param['c_id'] == 7) {
                $where[] = ['t3.type', '=', 4];
            }elseif ($param['c_id'] == 9) {
                $where[] = ['t3.type', '=', 5];
            }elseif ($param['c_id'] == 10) {
                $where[] = ['t3.type', '=', 7];
            }else{
                return json(['code' => 201, 'msg' => 'c_id参数异常。']);
            }
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_grade t2', 't1.id = t2.id')
                ->leftJoin("member_pay t3", 't1.id = t3.id')
                ->where($where)
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t3.num, t1.state, t1.time')
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('num', $param['order'])
//                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_grade t2', 't1.id = t2.id')
                ->leftJoin("member_pay t3", 't1.id = t3.id')
                ->where($where)
//                ->group('t1.id')
                ->count();
        }else{
            return json(['code' => 201, 'msg' => 'type参数异常。']);
        }
        // 总审核人数
        foreach ($data as &$datum) {
            $num += $datum['num'];
        }
		return json(['code' => 200, 'msg' => '排行查询成功。', 'data' => $data, 'count' => $count, 'num' => $num]);
	}

    /**
     * @Description: 查询推荐人
     * @Author: 林怼怼
     * @Date: 2024/3/4/004
     * @Time: 16:41
     */
    public function getParentFirst()
    {
        $param = input();
        $userInfo = getUserInfo(true);
        
        // 产品配置
        $productConfig = [
            // 999系列
            7 => [
                'levels' => [
                    6 => function($userId) {
                        // 笑长购买逻辑
                        $parentId = Db::name('member_parent')->where('id', $userId)->value('p_id');
                        if ($parentId === 1) {
                            return Db::name('member')->find(1);
                        }
                        
                        // 判断订单量
                        $orderNum = Db::name('order')->where([
                            'u_id' => $userId,
                            'type' => 4,
                            'state' => 1
                        ])->sum('num');
                        
                        return $orderNum >= 50 
                            ? Db::name('member')->find(1) 
                            : getParent521($userId);
                    },
                    5 => 'getParent521',      // 微笑长购买
                    4 => 'getParentCourse'     // 普通会员
                ]
            ],
            // 521库存
            5 => [
                'levels' => [
                    6 => function($userId) { return Db::name('member')->find(1); },
                    13 => function($userId) { return Db::name('member')->find(1); },
                    14 => function($userId) { return Db::name('member')->find(1); },
                    15 => function($userId) { return Db::name('member')->find(1); },
                    16 => function($userId) { return Db::name('member')->find(1); },
                    5 => 'getParent521',
                    4 => 'getParentCourse'
                ]
            ],
            // 思享荟库存
            4 => [
                'levels' => [
                    3 => function($userId) { return Db::name('member')->find(1); },
                    13 => function($userId) { return Db::name('member')->find(1); },
                    14 => function($userId) { return Db::name('member')->find(1); },
                    15 => function($userId) { return Db::name('member')->find(1); },
                    16 => function($userId) { return Db::name('member')->find(1); },
                    1 => 'getParent365',
                    2 => 'getParent365'
                ]
            ],
            // 悦读慧库存
            6 => [
                'levels' => [
                    9 => function($userId) { return Db::name('member')->find(1); },
                    13 => function($userId) { return Db::name('member')->find(1); },
                    14 => function($userId) { return Db::name('member')->find(1); },
                    15 => function($userId) { return Db::name('member')->find(1); },
                    16 => function($userId) { return Db::name('member')->find(1); },
                    7 => 'getParent365s'
                ]
            ],
            // 好妈妈库存
            9 => [
                'levels' => [
                    11 => function($userId) { return Db::name('member')->find(1); },
                    13 => function($userId) { return Db::name('member')->find(1); },
                    14 => function($userId) { return Db::name('member')->find(1); },
                    15 => function($userId) { return Db::name('member')->find(1); },
                    16 => function($userId) { return Db::name('member')->find(1); },
                    10 => 'getParentMama'
                ]
            ],
            10 => [
                'levels' => [
                    11 => function($userId) { return Db::name('member')->find(1); },
                    13 => function($userId) { return Db::name('member')->find(1); },
                    14 => function($userId) { return Db::name('member')->find(1); },
                    15 => function($userId) { return Db::name('member')->find(1); },
                    16 => function($userId) { return Db::name('member')->find(1); },
                    10 => 'getParentMama'
                ]
            ]
        ];

        // 处理续费或开通产品的情况
        $renewalProducts = [1, 2, 3, 8, 11];
        if (in_array($param['id'], $renewalProducts)) {
            return json([
                'code' => 200, 
                'msg' => '查询推荐人。', 
                'data' => getParentMaster($userInfo['id'], $param['id'])
            ]);
        }

        // 验证产品是否存在
        if (!isset($productConfig[$param['id']])) {
            return json(['code' => 201, 'msg' => '产品已经下架请重新选择。']);
        }

        // 查找用户等级对应的处理方法
        $config = $productConfig[$param['id']];
        $data = null;
        
        foreach ($config['levels'] as $level => $handler) {
            if (in_array($level, $userInfo['level'])) {
                $data = is_callable($handler) 
                    ? $handler($userInfo['id'])
                    : call_user_func($handler, $userInfo['id']);
                break;
            }
        }

        if ($data === null) {
            return json(['code' => 201, 'msg' => '请先开通对应产品。']);
        }

        return json(['code' => 200, 'msg' => '查询推荐人。', 'data' => $data]);
    }

    // /**
    //  * @Description: 查询推荐人
    //  * @Author: 林怼怼
    //  * @Date: 2024/3/4/004
    //  * @Time: 16:41
    //  */
    // public function getParentFirst()
    // {
    //     $param = input();
    //     $userInfo = getUserInfo(true);
    //     if ($param['id'] == 7) {
    //         // 999系列
    //         if (in_array(6, $userInfo['level'])) {
    //             // 笑长购买 上级/满足50单找总部/
    //             $id = Db::name('member_parent')->where('id', $userInfo['id'])->value('p_id');
    //             if ($id === 1) {
    //                 $data = Db::name('member')->find(1);
    //             }else{
    //                 // 笑长需要判断订单量
    //                 $num = Db::name('order')->where([
    //                     'u_id' => $userInfo['id'],
    //                     'type' => 4,
    //                     'state' => 1
    //                 ])->sum('num');
    //                 // 满足50单找总部
    //                 if ($num >= 50) {
    //                     $data = Db::name('member')->find(1);
    //                 }else{
    //                     $data = getParent521($userInfo['id']);
    //                 }
    //             }
    //         } elseif(in_array(5, $userInfo['level'])) {
    //             // 微笑长购买 找笑长/总部
    //             $data = getParent521($userInfo['id']);
    //         }elseif(in_array(4, $userInfo['level'])) {
    //             // 普通了了会员 找笑长/微笑长/总部
    //             $data = getParentCourse($userInfo['id']);
    //         }
    //     }elseif($param['id'] == 5) {
    //         // 521库存
    //         if (in_array(6, $userInfo['level'])) {
    //             $data = Db::name('member')->find(1);
    //         } elseif(in_array(5, $userInfo['level'])) {
    //             // 微笑长购买 找笑长/总部
    //             $data = getParent521($userInfo['id']);
    //         }elseif(in_array(4, $userInfo['level'])) {
    //             // 普通了了会员 找笑长/微笑长/总部
    //             $data = getParentCourse($userInfo['id']);
    //         }
    //     }elseif($param['id'] == 4) {
    //         // 思享荟库存
    //         if (in_array(3, $userInfo['level'])) {
    //             // 思享荟合伙人购买 找总部
    //             $data = Db::name('member')->find(1);
    //         } elseif(in_array(1, $userInfo['level']) || in_array(2, $userInfo['level'])) {
    //             // 思享荟会员购买 找思享荟合伙人
    //             $data = getParent365($userInfo['id']);
    //         }else{
    //             return json(['code' => 201, 'msg' => '请先开通对应产品。']);
    //         }
    //     }elseif($param['id'] == 6) {
    //         // 悦读慧库存
    //         if (in_array(9, $userInfo['level'])) {
    //             // 悦读慧合伙人购买 找总部
    //             $data = Db::name('member')->find(1);
    //         } elseif(in_array(7, $userInfo['level'])) {
    //             // 悦读慧会员购买 找悦读慧合伙人
    //             $data = getParent365s($userInfo['id']);
    //         }else{
    //             return json(['code' => 201, 'msg' => '请先开通对应产品。']);
    //         }
    //     }elseif($param['id'] == 9 || $param['id'] == 10) {
    //         // 好妈妈库存
    //         if (in_array(11, $userInfo['level'])) {
    //             // 好妈妈站长购买 找总部
    //             $data = Db::name('member')->find(1);
    //         } elseif(in_array(10, $userInfo['level'])) {
    //             // 好妈妈会员购买 找好妈妈站长
    //             $data = getParentMama($userInfo['id']);
    //         }else{
    //             return json(['code' => 201, 'msg' => '请先开通对应产品。']);
    //         }
    //     }elseif($param['id'] == 1 || $param['id'] == 2 || $param['id'] == 3 || $param['id'] == 8 || $param['id'] == 11) {
    //         // 续费或者开通产品 找开通了对应产品的推荐人
    //         $data = getParentMaster($userInfo['id'], $param['id']);
    //     }else{
    //         return json(['code' => 201, 'msg' => '产品已经下架请重新选择。']);
    //     }
    //     return json(['code' => 200, 'msg' => '查询推荐人。', 'data' => $data]);
    // }

    /**
     * @Description: 排行分类
     * @Author: 林怼怼
     * @Date: 2024/6/25/025
     * @Time: 18:02
     */
    public function cateRank()
    {
        $param = input();
        $id = input('id');
        $where = [];
        $arr = [];
        if (!empty($id)) {
            $userInfo = Db::name('member')->find($id);
            $userInfo['level'] = array_level($id);
        }else{
            $userInfo = getUserInfo(true);
        }
        // 思享慧
        if ($id == 3){
            $arr[] = 4;
        }
        // 了了派
        if($id == 5 || $id == 6) {
            $arr[] = 5;
            $arr[] = 7;
        }
        // 阅读慧
        if($id == 9) {
            $arr[] = 6;
        }
        // 好妈妈
        if($id == 11) {
            $arr[] = 9;
            $arr[] = 10;
        }
        $where[] = ['id', 'in', $arr];
        $data = Db::name('product')
            ->where($where)
            ->select()->toArray();
        foreach ($data as &$datum) {
            if ($datum['id'] === 4) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 1])->value('num') ?? 0;
            if ($datum['id'] === 5) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 2])->value('num') ?? 0;
            if ($datum['id'] === 6) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 3])->value('num') ?? 0;
            if ($datum['id'] === 7) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 4])->value('num') ?? 0;
            if ($datum['id'] === 9) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 5])->value('num') ?? 0;
            if ($datum['id'] === 10) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 7])->value('num') ?? 0;
        }
        return json(['code' => 200, 'msg' => '查询数据列表。', 'data' => $data]);

    }

    /**
     * @Description: 全部产品列表
     * @Author: 林怼怼
     * @Date: 2024/3/4/004
     * @Time: 12:46
     */
    public function cate()
    {
        $param = input();
        $id = input('id');
        $where = [];
        if (!empty($param['ids'])) $where[] = ['id', 'in', $param['ids']];
        if (!empty($param['type'])) $where[] = ['type', '=', $param['type']];
        if ($param['type'] == 2) {
            $arr = [];
            if (!empty($id)) {
                $userInfo = Db::name('member')->find($id);
                $userInfo['level'] = array_level($id);
            }else{
                $userInfo = getUserInfo(true);
            }
            // 思享慧
            if (in_array(0, $userInfo['level']) || in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level']) || in_array(1, $userInfo['level']) || in_array(3, $userInfo['level'])){
                $arr[] = 4;
            }
            // 了了派
            if(in_array(0, $userInfo['level']) || in_array(4, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level'])) {
                $arr[] = 5;
                $arr[] = 7;
            }
            // 阅读慧
            if(in_array(0, $userInfo['level']) || in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level']) || in_array(7, $userInfo['level']) || in_array(9, $userInfo['level'])) {
                $arr[] = 6;
            }
            // 好妈妈
            if(in_array(0, $userInfo['level']) || in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level']) || in_array(10, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level'])) {
                $arr[] = 9;
            }

            // 好妈妈
            if(in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level'])) {
                $arr[] = 5;
            }
            $where[] = ['id', 'in', $arr];
            $data = Db::name('product')
                ->where($where)
                ->where('state', 1)
                ->select()->toArray();
            foreach ($data as &$datum) {
                if ($datum['id'] === 4) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 1])->value('num') ?? 0;
                if ($datum['id'] === 5) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 2])->value('num') ?? 0;
                if ($datum['id'] === 6) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 3])->value('num') ?? 0;
                if ($datum['id'] === 7) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 4])->value('num') ?? 0;
                if ($datum['id'] === 9) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 5])->value('num') ?? 0;
                if ($datum['id'] === 10) $datum['num'] = Db::name('member_pay')->where(['id' => $userInfo['id'], 'type' => 7])->value('num') ?? 0;
            }
        }else{
            $data = Db::name('product')
                ->where($where)
                ->where('state', 1)
                ->select()->toArray();
        }
        foreach ($data as &$datum) {
            if ($datum['id'] == 1) {
                $datum['img'] = Db::name('grade')->where('id', 1)->value('img');
            }elseif ($datum['id'] == 2) {
                $datum['img'] = Db::name('grade')->where('id', 4)->value('img');
            }elseif ($datum['id'] == 3) {
                $datum['img'] = Db::name('grade')->where('id', 7)->value('img');
            }elseif ($datum['id'] == 8) {
                $datum['img'] = Db::name('grade')->where('id', 12)->value('img');
            }elseif ($datum['id'] == 11) {
                $datum['img'] = Db::name('grade')->where('id', 10)->value('img');
            }else{
                $datum['img'] = '';
            }
        }
        return json(['code' => 200, 'msg' => '查询数据列表。', 'data' => $data]);
    }

    /**
     * @Description: 首页数据统计
     * @Author: 林怼怼
     * @Date: 2024/1/16/016
     * @Time: 15:23
     */
    public function registerList()
    {
        $param = input();
        $start_time = strtotime($param['start_time'] == '' ? '2000-10-15'.' 00:00:01' : $param['start_time'] . ' 00:00:01');
        $end_time = strtotime($param['end_time'] == '' ? date("Y-m-d 23:59:59") : $param['end_time'] . ' 23:59:59');
        // 获取当前用户
        if (!empty($param['id'])) {
            $userInfo = Db::name('member')->find($param['id']);
            $userInfo['level'] = array_level($userInfo['id']);
        }else{
            $userInfo = getUserInfo(true);
        }
        $ids = getChildUserPhoneS($userInfo['id']);

        // 查询条件
        $where = [];
        $arr = [];

        if ($param['c_id'] == 11) {
            // 思享荟代理
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 3],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('t1.time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 3],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 12) {
            // 了了派微笑长
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 5],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 5],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 13) {
            // 了了派笑长
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 6],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 6],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 14) {
            // 悦读慧代理
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 9],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 9],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 1) {
            // 思享荟会员
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 1],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 1],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->order('time', 'desc')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 2) {
            // 了了派会员
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 4],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 4],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 3) {
            // 悦读荟会员
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 7],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 7],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 16) {
            // 好妈妈会员
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 10],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 10],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 15) {
            // 学员证
            $data = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 12],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->page((int)$param['page'], (int)$param['limit'])
                ->order('time', 'desc')
                ->group('t1.id')
                ->select()
                ->toArray();
            $count = Db::name('member')
                ->alias('t1')
                ->leftJoin("member_grade t5", 't1.id = t5.id')
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->where([
                    ['t1.id', 'in', $ids],
                    ['t5.grade', '=', 12],
                    ['t1.time', 'between', [$start_time, $end_time]],
                ])
                ->where($where)
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 4) {
            // 思享荟会员续费
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 1]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 1]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 5) {
            // 了了派会员续费
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 4]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 4]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 6) {
            // 悦读荟会员续费
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 7]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 7]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 18) {
            // 好妈妈会员续费
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 10]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->leftJoin('member_grade t3', 't1.id = t3.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 2],
                    ['t2.state', '=', 1],
                    ['t3.grade', '=', 10]
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 7) {
            // 思享荟预购
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 4],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 4],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 8) {
            // 了了派预购
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 5],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 5],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 9) {
            // 悦读荟预购
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 6],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 6],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 10) {
            // 999预购
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 7],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 7],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }elseif ($param['c_id'] == 19) {
            // 好妈妈预购
            $data = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 9],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->page((int) $param['page'], (int) $param['limit'])
                ->group('t1.id')
                ->select()->toArray();
            $count = Db::name('order')
                ->alias('t2')
                ->leftJoin('member t1', 't2.u_id = t1.id')
                ->where([
                    ['t2.p_id', '=', $userInfo['id']],
                    ['t2.type', '=', 4],
                    ['t2.state', '=', 1],
                    ['t2.c_id', '=', 9],
                ])
                ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
                ->group('t1.id')
                ->count();
        }else{
            return json(['code' => 201, 'msg' => 'c_id错误。']);
        }
        foreach ($data as $key => &$val) {
            $val['level'] = array_level($val['id']);
        }
        return json(['code' => 200, 'msg' => '查询数据列表。', 'data' => $data, 'count' => $count]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 注册量
	 * @Author: Linxy
	 * @Time: 2023/10/20 10:38
	 */
	public function registerNumber()
	{
        $param = input();
        // 获取当前用户
        if (!empty($param['id'])) {
            $userInfo = Db::name('member')->find($param['id']);
            $userInfo['level'] = array_level($userInfo['id']);
        }else{
            $userInfo = getUserInfo(true);
        }

        // 获取全部推荐
        $ids = getChildUserPhoneS($userInfo['id']);

        // 查询条件
        $where = [];
        $arr = []; // 全部会员
        $num = 0; // 全部会员数量

        $arr1 = []; // 全部续费
        $num1 = 0; // 全部续费数量

        $arr2 = []; // 全部会员
        $num2 = 0; // 全部会员数量
        // 思享荟会员
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 1],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 1, 'name' => '思享荟会员', 'num' => $count];

        // 了了派会员
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 4],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 2, 'name' => '了了派会员', 'num' => $count];

        // 悦读荟会员
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 7],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 3, 'name' => '悦读慧会员', 'num' => $count];

        // 好妈妈会员
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 10],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 16, 'name' => '好妈妈会员', 'num' => $count];

        // 成员
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 12],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 15, 'name' => '学员', 'num' => $count];

        // 思享荟代理
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 3],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 11, 'name' => '思享荟合伙人', 'num' => $count];

        // 了了派微笑长
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 5],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 12, 'name' => '微笑长', 'num' => $count];

        // 了了派笑长
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 6],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 13, 'name' => '笑长', 'num' => $count];

        // 悦读慧代理
        $count = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->where([
                ['t1.id', 'in', $ids],
                ['t5.grade', '=', 9],
            ])
            ->where($where)
            ->group('t1.id')
            ->count();
        $num += $count;
        $arr[] = ['id' => 14, 'name' => '悦读慧合伙人', 'num' => $count];

        // 站长
//        $count = Db::name('member')
//            ->alias('t1')
//            ->leftJoin("member_grade t5", 't1.id = t5.id')
//            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
//            ->where([
//                ['t1.id', 'in', $ids],
//                ['t5.grade', '=', 11],
//            ])
//            ->where($where)
//            ->group('t1.id')
//            ->count();
//        $num += $count;
//        $arr[] = ['id' => 17, 'name' => '站长', 'num' => $count];

        $data[] = ['id' => 1, 'name' => '全部人次', 'num' => $num, 'data' => $arr];

        // 思享荟会员续费
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->leftJoin('member_grade t3', 't1.id = t3.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 2],
                ['t2.state', '=', 1],
                ['t3.grade', '=', 1]
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num1 += $count;
        $arr1[] = ['id' => 4, 'name' => '思享荟会员续费', 'num' => $count];

        // 了了派会员续费
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->leftJoin('member_grade t3', 't1.id = t3.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 2],
                ['t2.state', '=', 1],
                ['t3.grade', '=', 4]
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num1 += $count;
        $arr1[] = ['id' => 5, 'name' => '了了派会员续费', 'num' => $count];

        // 悦读荟会员续费
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->leftJoin('member_grade t3', 't1.id = t3.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 2],
                ['t2.state', '=', 1],
                ['t3.grade', '=', 7]
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num1 += $count;
        $arr1[] = ['id' => 6, 'name' => '悦读慧会员续费', 'num' => $count];

        // 好妈妈续费
//        $count = Db::name('order')
//            ->alias('t2')
//            ->leftJoin('member t1', 't2.u_id = t1.id')
//            ->leftJoin('member_grade t3', 't1.id = t3.id')
//            ->where([
//                ['t2.p_id', '=', $userInfo['id']],
//                ['t2.type', '=', 2],
//                ['t2.state', '=', 1],
//                ['t3.grade', '=', 10]
//            ])
//            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
//            ->group('t1.id')
//            ->count();
//        $num1 += $count;
//        $arr1[] = ['id' => 18, 'name' => '好妈妈续费', 'num' => $count];

        $data[] = ['id' => 2, 'name' => '续费人次', 'num' => $num1, 'data' => $arr1];

        // 思享荟预购
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 4],
                ['t2.state', '=', 1],
                ['t2.c_id', '=', 4],
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num2 += $count;
        $arr2[] = ['id' => 7, 'name' => '思享荟销量', 'num' => $count];

        // 了了派预购
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 4],
                ['t2.state', '=', 1],
                ['t2.c_id', '=', 5],
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num2 += $count;
        $arr2[] = ['id' => 8, 'name' => '了了派销量', 'num' => $count];

        // 悦读荟预购
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 4],
                ['t2.state', '=', 1],
                ['t2.c_id', '=', 6],
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num2 += $count;
        $arr2[] = ['id' => 9, 'name' => '悦读荟销量', 'num' => $count];

        // 999预购
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 4],
                ['t2.state', '=', 1],
                ['t2.c_id', '=', 7],
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num2 += $count;
        $arr2[] = ['id' => 10, 'name' => '999销量', 'num' => $count];

        // 好妈妈预购
        $count = Db::name('order')
            ->alias('t2')
            ->leftJoin('member t1', 't2.u_id = t1.id')
            ->where([
                ['t2.p_id', '=', $userInfo['id']],
                ['t2.type', '=', 4],
                ['t2.state', '=', 1],
                ['t2.c_id', '=', 9],
            ])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state, t1.time')
            ->group('t1.id')
            ->count();
        $num2 += $count;
        $arr2[] = ['id' => 19, 'name' => '好妈妈预购', 'num' => $count];

        $data[] = ['id' => 3, 'name' => '人次销量', 'num' => $num2, 'data' => $arr2];
		return json(['code' => 200, 'msg' => '查询数据。', 'data' => $data]);
	}

	/**
	 * Created by PhpStorm.
	 * @purpose 总部查看用户列表
	 * @Author: Linxy
	 * @Time: 2023/10/20 10:10
	 */
	public function list()
	{
        $param = input();
        if ($param['id'] != 0) {
            $userInfo = Db::name('member')
                ->alias('t1')
                ->leftJoin('member_address t2', 't1.id = t2.member_id AND t2.is_master = 1 AND t2.state = 1')
                ->leftJoin('city t4', 't2.c_id = t4.id')
                ->fieldRaw('t1.id, t4.id as city_id')
                ->find($param['id']);
        }else{
            $userInfo = getUserInfo(true);
        }
        $where = [];
        $where[] = ['t1.id', '<>', 1];
        $where[] = ['t3.is_master', '=', 1];
        // 搜索
        if (!empty($param['search'])) $where[] = ['t1.name|t1.phone|t1.number', 'like', '%'.$param['search'].'%'];
        // 地区搜索
        if (!empty($param['city'])) $where[] = ['t3.p_id|t3.c_id|t3.d_id', 'like', '%'.$param['city'].'%'];
        // 级别筛选
        if (!empty($param['level'])) $where[] = ['t5.grade', 'in', $param['level']];
        // 状态筛选
        if ($param['state'] != '') $where[] = ['t1.state', '=', $param['state']];
        // 注册时间筛选
        $start_time = empty($param['start_time']) ? '2001-10-15 00:00:01' : $param['start_time'] .' 00:00:01';
        $end_time = empty($param['end_time']) ? date("Y-m-d 23:59:59") : $param['end_time'] .' 23:59:59';
        if (!empty($param['start_time'])) $where[] = ['t1.time', 'between time', [$start_time, $end_time]];
        // 查看是否是本地
        if ($param['area'] == 1) {
            $where[] = ['t3.c_id', '=', $userInfo['city_id']];
        }elseif($param['area'] == 2) {
            $where[] = ['t3.c_id', '<>', $userInfo['city_id']];
        }
        // 筛选直接或间接推荐
        $ids = getChildUserPhoneS($userInfo['id']);
        if ($param['type'] == 1) {
            // 查询直接下级
            $ids = Db::name('member_parent')
                ->where('p_id', '=', $userInfo['id'])
                ->column('id');
        }elseif($param['type'] == 2) {
            // 查询间接下级
            $ids = Db::name('member_parent')
                ->where('p_id', '=', $userInfo['id'])
                ->column('id');
            $ids = getChildUserPhoneS($ids);
        }
        $where[] = ['t1.id', 'in', $ids];
        $query = Db::name('member')
            ->alias('t1')
            ->leftJoin("member_address t3", 't1.id = t3.id')
            ->leftJoin("member_grade t5", 't1.id = t5.id')
            ->where($where)
            ->group('t1.id');

        // 预查询总数，优化分页
        $total_count = (clone $query)->count();
        $data = $query->page((int)$param['page'], (int)$param['limit'])
            ->field('t1.id, t1.name, t1.avatar, t1.phone, t1.state')
            ->order(['t1.id' => $param['order']])
            ->select()
            ->toArray();
        $data = array_map(function ($item) {
            $item['level'] = array_level($item['id']);
            return $item;
        }, $data);
        return json(['code' => 200, 'msg' => '查询用户列表成功', 'data' => $data, 'count' => $total_count]);
	}

    /**
     * @Description:
     * @Author: 林怼怼
     * @Date: 2024/3/13/013
     * @Time: 8:54
     */
    public function getGradeInfo()
    {
        $userInfo = getUserInfo(true);
        if (in_array(6, $userInfo['level'])) {
            // 笑长 不查询微笑长
            $data = Db::name('grade')
                ->where([
                    ['id', 'not in', [0,5]]
                ])
                ->where('state', 1)
                ->order('list', 'asc')->select()->toArray();
        }elseif(in_array(5, $userInfo['level'])) {
            // 微笑长  不查询笑长
            $data = Db::name('grade')
                ->where([
                    ['id', 'not in', [0,6]]
                ])
                ->where('state', 1)
                ->order('list', 'asc')->select()->toArray();
        }else{
            // 其他
            if (in_array(3, $userInfo['level']) || in_array(9, $userInfo['level']) || in_array(11, $userInfo['level']) || in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level'])) {
                $data = Db::name('grade')
                    ->where([
                        ['id', 'not in', [0]]
                    ])
                    ->where('state', 1)
                    ->order('list', 'asc')->select()->toArray();
            }else{
                $data = Db::name('grade')
                    ->where([
                        ['id', 'not in', [0,3,5,6,9,11,13,14,15,16]]
                    ])
                    ->where('state', 1)
                    ->order('list', 'asc')->select()->toArray();
            }
        }
        foreach ($data as $key => &$val) {
            $val['type'] = false;
            $val['vip_end_time'] = '';
            $val['img3'] = '';
            if ($val['id'] == 10) {
                $val['img3'] = Db::name('city_admin')->alias('t1')
                    ->where('t1.city', '=', $userInfo['province_id'])
                    ->value('img');
            }
            if (in_array($val['id'], $userInfo['level'])) {
                // 如果等于这个等级就返回true
                $val['type'] = true;
                $end_time = Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => $val['id']])->value('vip_end_time');
                $val['vip_end_time'] = $end_time === null ? '' : $end_time;
            }
        }

        return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
    }

    /**
     * @Description: 查看等级身份
     * @Author: 林怼怼
     * @Date: 2024/3/1/001
     * @Time: 12:36
     */
    public function userGradeList()
    {
        $id = input('id', '');
        if (empty($id)) {
            $userInfo = getUserInfo(true);
        }else{
            $userInfo = ['id' => $id, 'level' => array_level($id)];
        }
        if (in_array(3, $userInfo['level']) || in_array(6, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(9, $userInfo['level']) || in_array(11, $userInfo['level'])) {
            // 代理
            $data = Db::name('grade')
                ->where([
                    ['id', 'not in', [0,6,15]]
                ])
                ->order('list', 'asc')->select()->toArray();
        }elseif(in_array(0, $userInfo['level']) || in_array(13, $userInfo['level']) || in_array(14, $userInfo['level']) || in_array(15, $userInfo['level']) || in_array(16, $userInfo['level'])) {
            // 总部
            $data = Db::name('grade')
                ->where([
                    ['id', 'not in', 0,15]
                ])
                ->order('list', 'asc')->select()->toArray();
        }else{
            // 普通用户
            $data = Db::name('grade')->whereIn('id', [1,4,7,10])->order('list', 'asc')->select()->toArray();
        }
        foreach ($data as $key => &$val) {
            $val['type'] = false;
            $val['vip_end_time'] = '';
            if (in_array($val['id'], $userInfo['level'])) {
                // 如果等于这个等级就返回true
                $val['type'] = true;
                $val['vip_end_time'] = Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => $val['id']])->value('vip_end_time') == null ? '' : Db::name('member_grade')->where(['id' => $userInfo['id'], 'grade' => $val['id']])->value('vip_end_time');
            }
        }
        return json(['code' => 200, 'msg' => '成功', 'data' => $data]);
    }

	/**
	 * Created by PhpStorm.
	 * @purpose 获取用户信息
	 * @Author: Linxy
	 * @Time: 2023/10/18 9:28
	 */
    public function getUserInfo()
	{
		$userInfo = getUserInfo(true);
		return json(['code' => 200, 'msg' => '查询会员信息成功。', 'data' => ['userInfo' => $userInfo, 'token' => encryptToken($userInfo['id'], true)]]);
	}
}
