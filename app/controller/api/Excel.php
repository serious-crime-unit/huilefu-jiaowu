<?php
declare (strict_types = 1);
namespace app\controller\api;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use think\facade\Db;
use think\Request;

class Excel
{
    /**
     * @Description: 查询可导出列表
     * @Author: 林怼怼
     * @Date: 2025/2/11
     * @Time: 15:37
     */
    public function exportExcelList()
    {
        $param = input();
        $where = [];
        $where[] = ['t1.id', '<>', 1];
        if (!empty($param['grade'])) $where[] = ['t2.grade', 'in', $param['grade']];
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->where($where)
            ->whereBetween('t1.time',[$param['start_time'], $param['end_time']])
            ->field('t1.id, t1.name, t1.phone')
            ->page((int)$param['page'], (int)$param['limit'])
            ->group('t1.id')  // 设置去重
            ->select()->toArray();
        $count = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->where($where)
            ->whereBetween('t1.time',[$param['start_time'], $param['end_time']])
            ->field('t1.id, t1.name, t1.phone')
            ->group('t1.id')  // 设置去重
            ->count();
        return json(['code' => 200, 'msg' => 'ok。', 'data' => $data, 'count' => $count]);
    }

    /**
     * @Description: 导出用户等级时间筛选
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 15:43
     */
    public function exportExcelAPI()
    {
        $param = input();
        
        // 增加脚本最大执行时间
        set_time_limit(0);  // 设置为 0 表示不限制执行时间

        // 增加内存限制，确保有足够内存处理数据
        ini_set('memory_limit', '512M');  // 可以根据实际需求设置更高的值

        $where = [];
        $where[] = ['t1.id', '<>', 1];
        if (!empty($param['grade'])) $where[] = ['t2.grade', 'in', $param['grade']];
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->where($where)
            ->whereBetween('t1.time',[$param['start_time'], $param['end_time']])
            ->field('t1.id, t1.name, t1.phone, t1.number, t1.time')
            ->group('t1.id')  // 设置去重
            ->select()->toArray();

        // 创建一个新的 Spreadsheet 对象
        // 使用流式方式输出文件
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // 在表格中填充数据
        $sheet->setCellValue('A1', '姓名'); // 姓名
        $sheet->setCellValue('B1', '电话'); // 电话
        $sheet->setCellValue('C1', '推荐人'); // 推荐人
        $sheet->setCellValue('D1', '推荐人电话'); // 推荐人电话
        $sheet->setCellValue('E1', '省'); // 省
        $sheet->setCellValue('F1', '市'); // 市
        $sheet->setCellValue('G1', '编号'); // 编号
        $sheet->setCellValue('H1', '注册时间'); // 注册时间

        foreach ($data as $key => $v) {
            // 推荐人电话
            $id = Db::name('member_parent')->where('id', $v['id'])->value('p_id');
            $tuijianren_phone = Db::name('member')->where('id', $id)->find();
            // 获取省市区
            $city = Db::name('member_address')->where('id', $v['id'])->find();
            $city1 = Db::name('city')->where('id', $city['p_id'])->value('cityName');
            $city2 = Db::name('city')->where('id', $city['c_id'])->value('cityName');
            $sheet->setCellValue('A'.((int)$key+2), $v['name']); // 姓名
            $sheet->setCellValue('B'.((int)$key+2), $v['phone']); // 电话
            $sheet->setCellValue('C'.((int)$key+2), $tuijianren_phone['name']); // 推荐人
            $sheet->setCellValue('D'.((int)$key+2), $tuijianren_phone['phone']); // 推荐人电话
            $sheet->setCellValue('E'.((int)$key+2), $city1); // 省
            $sheet->setCellValue('F'.((int)$key+2), $city2); // 市
            $sheet->setCellValue('G'.((int)$key+2), $v['number']); // 编号
            $sheet->setCellValue('H'.((int)$key+2), $v['time']); // 注册时间
        }

        // 设置文件名
        $filename = 'export_' . date('YmdHis') . '.xlsx';

        // 直接输出到浏览器
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
    }


    /**
     * @Description: 导入excel(活动)
     * @Author: 林怼怼
     * @Date: 2024/8/14/014
     * @Time: 9:19
     */
    public function importMeeting(Request $request)
    {
        // 启动事务
        Db::startTrans();
        try {
            $file = request()->file('file');
            // 读取文件
            $data = $this->readExcel($file->getRealPath());
            // 错误内容
            $error_count = 0;
            // 错误提示表格
            $error_data[] = ['name' => '位置', 'phone' => '电话', 'desc' => '错误描述'];
            // 成功数量
            $count = 0;
            $num = 0;
            foreach ($data as $key => $val) {
                $num++;
                // 不需要获取第一行 第一行跳过
                if ($key === 0) continue;
                // 查看是否有信息
                if (empty($val['1'])) continue;

                // 参加人是否注册
                $buyer = Db::name('member')->where('phone', $val[1])->value('id');
                if (!$buyer) {
                    $error_data[] = ['name' => '第'. ($key+1) .'行', 'phone' => $val[1], 'desc' => '买家未注册、'];
                    $error_count++;
                    continue;
                }

                // 查询活动是否存在
                $course = Db::name('meeting')->where('name', 'like', '%'.$val[2].'%')->value('id');
                if (!$course) {
                    $error_data[] = ['name' => '第'. ($key+1) .'行', 'phone' => $val[1], 'desc' => '该对应活动不存在、'];
                    $error_count++;
                    continue;
                }
                // 查询具体活动场次
                $city_id = Db::name('meeting_city')->where([
                    ['p_id', '=', $course],
                    ['address', 'like', '%'.$val[3].'%']
                ])->value('id');
                if (!$city_id) {
                    $error_data[] = ['name' => '第'. ($key+1) .'行', 'phone' => $val[1], 'desc' => '该对应活动场次不存在、'];
                    $error_count++;
                    continue;
                }

                // 添加订单
                $res = Db::name('member_meeting')->insertGetId([
                    'u_id' => $buyer,
                    'm_id' => $city_id,
                    'state' => 2,
                    'complete' => 2,
                    'time' => time(),
                ]);
                $count++;
            }
            // 提交事务
            Db::commit();
            if ($error_count !== 0) $this->exportExcelMin($error_data, $count, $error_count);
            return json(['code' => 200, 'msg' => '导入Excel完成'.$count.'条，失败'.$error_count.'条。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 导入excel(课程)
     * @Author: 林怼怼
     * @Date: 2024/8/14/014
     * @Time: 9:19
     */
    public function importCourseOrder(Request $request)
    {
        // 启动事务
        Db::startTrans();
        try {
            $file = request()->file('file');
            // 读取文件
            $data = $this->readExcel($file->getRealPath());
            // 错误内容
            $error_count = 0;
            // 错误提示表格
            $error_data[] = ['name' => '位置', 'phone' => '电话', 'desc' => '错误描述'];
            // 成功数量
            $count = 0;
            foreach ($data as $key => $val) {
                // 不需要获取第一行 第一行跳过
                if ($key === 0) continue;
                // 查看是否有信息
                if (empty($val['1'])) continue;

                // 卖家是否注册
                $p_id = 1;
                if ($val[3] != 123456) {
                    $p_id = Db::name('member')->where('phone', $val[3])->value('id');
                    if (!$p_id) {
                        $error_data[] = ['name' => '第'. ($key+1) .'行', 'phone' => $val[1], 'desc' => '卖家未注册、'];
                        $error_count++;
                        continue;
                    }
                }
                // 买家是否注册
                $buyer = Db::name('member')->where('phone', $val[1])->value('id');
                if (!$buyer) {

                    // 编号
                    $number = substr(sprintf('%07s', Db::name('member')->max('number')+1), -6);

                    // 密码
                    $pwd = encrypt_password('123458');

                    // 注册时间
                    $time = time();

                    // 添加用户表
                    $id = Db::name('member')->insertGetId([
                        'name' => $val[0],
                        'avatar' => 'http://amrj-public-read.oss-cn-beijing.aliyuncs.com/fenzhidao/voucher/07f2720056963aaeb95f8d7e04764a6e.png',
                        'phone' => $val[1],
                        'number' => $number,
                        'pwd' => $pwd,
                        'is_admin' => 2,
                        'state' => 1,
                        'time' => $time,
                    ]);

                    // 添加推荐人关联
                    Db::name('member_parent')->insert(['id' => $id, 'p_id' => $p_id]);

                    // 添加库存
                    Db::name('member_pay')->insertAll([
                        ['id' => $id, 'num' => 0, 'type' => 1],
                        ['id' => $id, 'num' => 0, 'type' => 4],
                        ['id' => $id, 'num' => 0, 'type' => 2],
                        ['id' => $id, 'num' => 0, 'type' => 3],
                    ]);

                    // 添加地址
                    Db::name('member_address')->save([
                        'id' => $id,
                        'p_id' => 110000,
                        'c_id' => 110100,
                        'd_id' => 110114,
                        'address' => '无'
                    ]);

                    // 添加等级
                    $gradeDate = [];
                    $gradeDate[] = ['id' => $id, 'grade' => 12, 'vip_start_time' => null, 'vip_end_time' => null];
                    Db::name('member_grade')->insertAll($gradeDate);
                    $buyer = $id;
                }

                // 查询课程是否存在
                $course = Db::name('course')->where('title', 'like', '%'.$val[4].'%')->value('id');
                if (!$course) {
                    $error_data[] = ['name' => '第'. ($key+1) .'行', 'phone' => $val[1], 'desc' => '该对应课程未上架不存在、'];
                    $error_count++;
                    continue;
                }
                // 添加订单
                $res = Db::name('order')->insertGetId([
                    'u_id' => $buyer,
                    'p_id' => $p_id,
                    'c_id' => $course,
                    'num' => 1,
                    'type' => 5,
                    'state' => 1,
                    'time' => time(),
                ]);
                $count++;
            }
            if ($error_count !== 0) $this->exportExcelMin($error_data, $count, $error_count);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '导入Excel完成'.$count.'条，失败'.$error_count.'条。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 导出好妈妈excel
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 15:43
     */
    public function exportExcelMOMO()
    {
        // 创建一个新的Excel对象
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $data = Db::name('member_meeting')
            ->alias('a1')
            ->join('member_parent a2', 'a1.u_id = a2.id')
            ->where([
                ['a1.m_id', '=', 1],
                ['a1.u_id', '<>', 1]
            ])
            ->field('a1.u_id, a2.p_id, a1.time, a1.complete')
            ->select()->toArray();
        // 在表格中填充数据
        foreach ($data as $key => $v) {
            $arr = Db::name('member')->where('id', $v['p_id'])->find();
            $arr1 = Db::name('member')->where('id', $v['u_id'])->find();
            $sheet->setCellValue('A'.((int)$key+1), $arr['name']);
            $sheet->setCellValue('B'.((int)$key+1), $arr['phone']);
            $sheet->setCellValue('C'.((int)$key+1), $arr1['name']);
            $sheet->setCellValue('D'.((int)$key+1), $arr1['phone']);
            $sheet->setCellValue('E'.((int)$key+1), date('Y-m-d H:i:s', $v['time']));
            if ($v['complete'] == 1) {
                $sheet->setCellValue('F'.((int)$key+1), '否');
            }else{
                $sheet->setCellValue('F'.((int)$key+1), '是');
            }
        }

        // 导出Excel文件
        $filename = '好妈妈活动报名表.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        // 设置响应头，告诉浏览器输出Excel文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        // 将Excel文件输出到浏览器
        $writer->save('php://output');

        // 结束请求，确保不会输出其他内容
        return json(['code' => 200, 'msg' => '导入Excel完成.']);
    }


    /**
     * @Description: 更新库存到最新
     * @Author: 林怼怼
     * @Date: 2024/8/14/014
     * @Time: 9:20
     */
    public function importPay(Request $request)
    {
        // 启动事务
        Db::startTrans();
        try {
            $file = request()->file('file');
            // 读取文件
            $data = $this->readExcel($file->getRealPath());
            // 错误内容
            $error_count = 0;
            // 错误提示表格
            $error_data[] = ['name' => '姓名', 'phone' => '电话', 'desc' => '错误描述'];
            // 成功数量
            $count = 0;
            foreach ($data as $key => $val) {
                // 更新库存
                $id = Db::name('member')->where('phone', $val[0])->value('id');
                Db::name('member_pay')->where(['id' => $id, 'type' => 1])->update([
                    'num' => $val[1]
                ]);
                $count++;
            }
            if ($error_count !== 0) $this->exportExcelMin($error_data, $count, $error_count);
            // 提交事务
            Db::commit();
            return json(['code' => 200, 'msg' => '导入Excel完成'.$count.'条，失败'.$error_count.'条。']);
        } catch (\Throwable $e){
            // 回滚事务
            Db::rollback();
            return json(['code' => 203, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * @Description: 导入excel
     * @Author: 林怼怼
     * @Date: 2024/8/14/014
     * @Time: 9:19
     */
    public function import(Request $request)
	{
        // 启动事务
        Db::startTrans();
        try {
			$file = request()->file('file');
			// 读取文件
			$data = $this->readExcel($file->getRealPath());
            // 错误内容
            $error_count = 0;
            // 错误提示表格
            $error_data[] = ['name' => '姓名', 'phone' => '电话', 'desc' => '错误描述'];
            // 成功数量
            $count = 0;
            foreach ($data as $key => $val) {
                // 不需要获取第一行 第一行跳过
                if ($key === 0) continue;
                // 查看是否有信息
                if (empty($val['1'])) continue;
                // 查看自己是否注册
                $userInfo = Db::name('member')
                    ->alias('t1')
                    ->join('member_parent t2', 't1.id = t2.id')
                    ->where('phone', $val['1'])
                    ->field('t1.id, t2.p_id')
                    ->find();

                // 查看推荐人id
                $p_id = 1;
                if ($val[3] != 123456) {
                    $p_id = Db::name('member')->where('phone', $val[3])->value('id');
                    if (!$p_id) {
                        $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '推荐人未注册。'];
                        $error_count++;
                        continue;
                    }
                }
                // 查询推荐人是否注册
                if ($userInfo) {
                    if ($userInfo['p_id'] == $p_id) {
                        $count++;
                        continue;
                    }
                    $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '电话已经注册过了。'];
                    $error_count++;
                    continue;
                }
                // 编号
                $number = $val[4];
                if (empty($val[4]) || $val[4] == '无') {
                    $number = substr(sprintf('%07s', Db::name('member')->max('number')+1), -6);
                }
                $count = Db::name('member')->where('number', $val[4])->count();
                if ($count !== 0) {
                    $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '编号重复。'];
                    $error_count++;
                    continue;
                }

                // 密码
                $pwd = $val[5];
                if (strlen((string)$val[5]) < 10){
                    $pwd = encrypt_password($val[5]);
                }

                // 注册时间
                $time = $val[19];
                if ($val[19] == '无' || $val[19] == '') {
                    $time = time();
                }else{
                    if (strlen((string)$val[19]) !== 10) {
                        $excelDate = ($val[19] - 25569) * 86400; // 转换为 Excel 格式日期
                        $time = $excelDate;
                    }
                }
                // 添加用户表
                $id = Db::name('member')->insertGetId([
                    'name' => $val[0],
                    'avatar' => 'http://amrj-public-read.oss-cn-beijing.aliyuncs.com/fenzhidao/voucher/07f2720056963aaeb95f8d7e04764a6e.png',
                    'phone' => $val[1],
                    'number' => $number,
                    'pwd' => $pwd,
                    'is_admin' => (int)$val[15],
                    'state' => 1,
                    'time' => $time,
                ]);
                // 添加推荐人关联
                Db::name('member_parent')->insert(['id' => $id, 'p_id' => $p_id]);
                // 添加库存
                Db::name('member_pay')->insertAll([
                    ['id' => $id, 'num' => $val[6]??'', 'type' => 1],
                    ['id' => $id, 'num' => $val[7]??'', 'type' => 4],
                    ['id' => $id, 'num' => $val[8]??'', 'type' => 2],
                    ['id' => $id, 'num' => $val[9]??'', 'type' => 3],
                ]);
                // 添加地址
                $province = Db::name('city')->where('cityName', '=', $val[11])->value('id');
                $city = Db::name('city')->where('cityName', '=', $val[12])->value('id');
                $area = Db::name('city')->where('cityName', '=', $val[13])->value('id');
                if (!$province) $province = 0;
                if (!$city) $city = 0;
                if (!$area) $area = 0;
                Db::name('member_address')->save([
                    'id' => $id,
                    'p_id' => $province,
                    'c_id' => $city,
                    'd_id' => $area,
                    'address' => empty($val[14]) ? '' :$val[14]
                ]);
                // 添加等级
                $gradeDate = [];
                foreach (explode(',',(string)$val[2]) as $k => $v) {
                    if ($v == 1 || $v == 2) {
                        // 思享荟
                        if ($val[17] == '无' || $val[17] == '') {
                            $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '思享荟等级到期时间不正确。'];
                            $error_count++;
                            continue;
                        }
                        $time = $val[17];
                        if (strlen((string)$val[17]) !== 10) {
                            $excelDate = ((int)$val[17] - 25569) * 86400; // 转换为 Excel 格式日期
                            $time = $excelDate;
                        }
                        $gradeDate[] = ['id' => $id, 'grade' => $v, 'vip_start_time' => $time, 'vip_end_time' => $time];
                    }elseif ($v == 4) {
                        // 了了派
                        if ($val[18] == '无' || $val[18] == '') {
                            $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '了了派等级到期时间不正确。'];
                            $error_count++;
                            continue;
                        }
                        $time = $val[18];
                        if (strlen((string)$val[18]) !== 10) {
                            $excelDate = ((int)$val[18] - 25569) * 86400; // 转换为 Excel 格式日期
                            $time = $excelDate;
                        }
                        $gradeDate[] = ['id' => $id, 'grade' => $v, 'vip_start_time' => $time, 'vip_end_time' => $time];
                    }elseif ($v == 7 || $v == 8) {
                        // 悦读荟
                        if ($val[16] == '无' || $val[16] == '') {
                            $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '悦读荟等级到期时间不正确。'];
                            $error_count++;
                            continue;
                        }
                        $time = $val[16];
                        if (strlen((string)$val[16]) !== 10) {
                            $excelDate = ((int)$val[16] - 25569) * 86400; // 转换为 Excel 格式日期
                            $time = $excelDate;
                        }
                        $gradeDate[] = ['id' => $id, 'grade' => $v, 'vip_start_time' => $time, 'vip_end_time' => $time];
                    }elseif ($v == 3 || $v == 5 || $v == 6 || $v == 9 || $v == 12) {
                        $gradeDate[] = ['id' => $id, 'grade' => $v, 'vip_start_time' => null, 'vip_end_time' => null];
                    }else{
                        $error_data[] = ['name' => $val[0], 'phone' => $val[1], 'desc' => '未知的级别身份：'.$v];
                        $error_count++;
                        continue;
                    }
                }
                Db::name('member_grade')->insertAll($gradeDate);
                $count++;
            }
            if ($error_count !== 0) $this->exportExcelMin($error_data, $count, $error_count);
            // 提交事务
            Db::commit();
			return json(['code' => 200, 'msg' => '导入Excel完成'.$count.'条，失败'.$error_count.'条。']);
		} catch (\Throwable $e){
            // 回滚事务
			Db::rollback();
			return json(['code' => 203, 'msg' => $e->getMessage()]);
		}
	}
	
	/**
	 * Created by PhpStorm.
	 * @purpose 读取文件
	 * @Author: Linxy
	 * @Time: 2023/11/30 11:26
	 */
	private function readExcel($filePath)
	{
		// 读取 Excel 文件的逻辑
		$spreadsheet = IOFactory::load($filePath);
        $worksheet = $spreadsheet->getActiveSheet();

		$data = [];
		foreach ($worksheet->getRowIterator() as $row) {
			$rowData = [];
			foreach ($row->getCellIterator() as $cell) {
				$rowData[] = $cell->getValue();
			}
			$data[] = $rowData;
		}
		return $data;
	}

    /**
     * @Description: 导出excel表格
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 15:43
     */
    public function exportExcelMin($arr, $count, $error_count)
    {
        // 创建一个新的Excel对象
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // 在表格中填充数据
        foreach ($arr as $key => $v) {
            $sheet->setCellValue('A'.((int)$key+1), $v['name']);
            $sheet->setCellValue('B'.((int)$key+1), $v['phone']);
            $sheet->setCellValue('C'.((int)$key+1), $v['desc']);
        }

        // 导出Excel文件
        $filename = 'example.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        // 设置响应头，告诉浏览器输出Excel文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // 将Excel文件输出到浏览器
        $writer->save('php://output');

        // 结束请求，确保不会输出其他内容
        return json(['code' => 200, 'msg' => '导入Excel完成'.$count.'条，失败'.$error_count.'条。']);
    }

    /**
     * @Description: 导出库存excel表格
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 15:43
     */
    public function exportExcel()
    {
        $param = input();
        dd($param);
        // 创建一个新的Excel对象
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $data = Db::name('member')
            ->alias('t1')
            ->join('member_grade t2', 't1.id = t2.id')
            ->where([
                ['t2.grade', '=', $param['grade']]
            ])
//            ->whereBetween('t1.time',[$param['start_time'], $param['end_time']])
            ->select()->toArray();
        dd($data);

        // 在表格中填充数据
        $sheet->setCellValue('A1', '姓名'); // 姓名
        $sheet->setCellValue('B1', '电话'); // 电话
        $sheet->setCellValue('C1', '等级'); // 电话
        $sheet->setCellValue('D1', '推荐人电话'); // 电话
        $sheet->setCellValue('E1', '编号'); // 编号
        $sheet->setCellValue('F1', '密码'); // 编号
        $sheet->setCellValue('G1', '惠乐福预购'); // 编号
        $sheet->setCellValue('H1', '999预购'); // 编号
        $sheet->setCellValue('I1', '了了派库存'); // 编号
        $sheet->setCellValue('J1', '悦读会预购'); // 编号
        $sheet->setCellValue('K1', '积分'); // 编号
        $sheet->setCellValue('L1', '省'); // 编号
        $sheet->setCellValue('M1', '市'); // 编号
        $sheet->setCellValue('N1', '区'); // 编号
        $sheet->setCellValue('O1', '详细地址'); // 编号
        $sheet->setCellValue('P1', '访问后台权限'); // 编号
        $sheet->setCellValue('Q1', '悦读会到期时间'); // 编号
        $sheet->setCellValue('R1', '思想汇到期时间'); // 编号
        $sheet->setCellValue('S1', '了了派到期时间'); // 编号
        $sheet->setCellValue('U1', '注册时间'); // 注册时间
        foreach ($data as $key => $v) {
            // 推荐人电话
            $tuijianren = Db::name('member_parent')->where('id', $v['id'])->value('p_id');
            $tuijianren_phone = Db::name('member')->where('id', $tuijianren)->value('phone');
            // 惠乐福预购
            $huilefuyugou = Db::name('member_pay')->where(['id' => $v['id'], 'type' => 1])->value('num');
            $jiujiuyugou = Db::name('member_pay')->where(['id' => $v['id'], 'type' => 4])->value('num');
            $lelepai = Db::name('member_pay')->where(['id' => $v['id'], 'type' => 2])->value('num');
            $yueduhui = Db::name('member_pay')->where(['id' => $v['id'], 'type' => 3])->value('num');
            // 会员到期时间
            $daoqishijian1 = Db::name('member_grade')->where(['id' => $v['id'], 'grade' => 1])->value('vip_end_time');
            $daoqishijian2 = Db::name('member_grade')->where(['id' => $v['id'], 'grade' => 7])->value('vip_end_time');
            $daoqishijian3 = Db::name('member_grade')->where(['id' => $v['id'], 'grade' => 4])->value('vip_end_time');
            $sheet->setCellValue('A'.((int)$key+2), $v['name']); // 姓名
            $sheet->setCellValue('B'.((int)$key+2), $v['phone']); // 电话
            $sheet->setCellValue('D'.((int)$key+2), $tuijianren_phone); // 电话
            $sheet->setCellValue('E'.((int)$key+2), $v['number']); // 编号
            $sheet->setCellValue('F'.((int)$key+2), $v['pwd']); // 电话
            $sheet->setCellValue('G'.((int)$key+2), $huilefuyugou); // 电话
            $sheet->setCellValue('H'.((int)$key+2), $jiujiuyugou); // 电话
            $sheet->setCellValue('I'.((int)$key+2), $lelepai); // 电话
            $sheet->setCellValue('J'.((int)$key+2), $yueduhui); // 电话
            $sheet->setCellValue('K'.((int)$key+2), 0); // 电话
            $sheet->setCellValue('Q'.((int)$key+2), $daoqishijian1); // 电话
            $sheet->setCellValue('R'.((int)$key+2), $daoqishijian2); // 电话
            $sheet->setCellValue('S'.((int)$key+2), $daoqishijian3); // 电话
            $sheet->setCellValue('U'.((int)$key+2), $v['time']); // 注册时间
        }

        // 导出Excel文件
        $filename = '生产数据.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        // 设置响应头，告诉浏览器输出Excel文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        // 将Excel文件输出到浏览器
        $writer->save('php://output');

        // 结束请求，确保不会输出其他内容
        exit;
    }

    /**
     * @Description: 导出excel表格
     * @Author: 林怼怼
     * @Date: 2023/12/15/015
     * @Time: 15:43
     */
    public function exportExcel2()
    {
        // 创建一个新的Excel对象
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $data = Db::name('member')
            ->where([
                ['state', '=', 1],
                ['id', '>', 9],
                ['name', '<>', '测试']
            ])
            ->field('phone, quota')
            ->select()->toArray();
        // 在表格中填充数据
        foreach ($data as $key => $v) {
            $sheet->setCellValue('A'.((int)$key+1), $v['phone']); // 姓名
            $sheet->setCellValue('B'.((int)$key+1), $v['quota']); // 姓名
        }

        // 导出Excel文件
        $filename = 'example.xlsx';
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        // 设置响应头，告诉浏览器输出Excel文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        // 将Excel文件输出到浏览器
        $writer->save('php://output');

        // 结束请求，确保不会输出其他内容
        exit;
    }
}
