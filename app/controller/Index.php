<?php
namespace app\controller;

use app\BaseController;

class Index extends BaseController
{
    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V' . \think\facade\App::version() . '<br/><span style="font-size:30px;">16载初心不改 - 你值得信赖的PHP框架</span></p><span style="font-size:25px;">[ V6.0 版本由 <a href="https://www.yisu.com/" target="yisu">亿速云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="ee9b1aa918103c4fc"></think>';
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
	
	public function read()
	{
		return '写字楼里写字间，写字间里程序员；
                      程序人员写程序，又拿程序换酒钱。
                      酒醒只在网上坐，酒醉还来网下眠；
                      酒醉酒醒日复日，网上网下年复年。
                      但愿老死电脑间，不愿鞠躬老板前；
                      奔驰宝马贵者趣，公交自行程序员。
                      别人笑我忒疯癫，我笑自己命太贱；
                      不见满街漂亮妹，哪个归得程序员？';
	}
	
	
	/**
	 *                             _ooOoo_
	 *                            o8888888o
	 *                            88" . "88
	 *                            (| -_- |)
	 *                            O\  =  /O
	 *                         ____/`---'\____
	 *                       .'  \\|     |//  `.
	 *                      /  \\|||  :  |||//  \
	 *                     /  _||||| -:- |||||-  \
	 *                     |   | \\\  -  /// |   |
	 *                     | \_|  ''\---/''  |   |
	 *                     \  .-\__  `-`  ___/-. /
	 *                   ___`. .'  /--.--\  `. . __
	 *                ."" '<  `.___\_<|>_/___.'  >'"".
	 *               | | :  `- \`.;`\ _ /`;.`/ - ` : | |
	 *               \  \ `-.   \_ __\ /__ _/   .-` /  /
	 *          ======`-.____`-.___\_____/___.-`____.-'======
	 *                             `=---='
	 *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 *                     佛祖保佑        永无BUG
	 *
	 *
	 */
}
