<?php
declare (strict_types = 1);

namespace app\middleware;

use think\facade\Db;

class CheckMidApi
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
		$header = request()->header();
		// 验证token是否存在
		if (!isset($header['authorization']) || $header['authorization'] == null) return json(['code'=>401, 'msg'=>'请确认token是否输入。']);
		// 验证是否有效
		$result = checkToken($header['authorization'], true);
		if (!isset($result['code']) || $result['code'] != 200) return json(['code'=>401, 'msg'=>'请确认token是否有效。']);
		// 除了总部其他账号都不可以重复登录
		$data = Db::name('member')->find($result['data']['id']);
        $pinyin = new \Overtrue\Pinyin\Pinyin();
        $name_pinyin = $pinyin->convert($data['name']);
        // 将拼音数组转换为字符串并使用自定义连接符
        $pinyinStr = implode(' ', $name_pinyin);
        // 将拼音字符串转换为大写
        $pinyinStrUpper = strtoupper($pinyinStr);

        Db::name('member')->save(['id' => $data['id'], 'pinyin' => $pinyinStrUpper]);
        if ($data['onlyCode'] != $result['data']['onlyCode'] && $data['id'] != 1 && $data['is_teacher'] != 1) return json(['code'=>401,'msg'=>'您已在其他设备登录，请检查账号密码是否泄露!!!']);
        return $next($request);
    }
}
