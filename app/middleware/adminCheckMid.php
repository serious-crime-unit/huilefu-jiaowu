<?php
declare (strict_types = 1);

namespace app\middleware;

class adminCheckMid
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
		$header = request()->header();
		// 验证token是否存在
		if (!isset($header['authorization']) || $header['authorization'] == null) return json(['code'=>401, 'msg'=>'请确认token是否输入。']);
		// 验证是否有效
		$result = checkToken($header['authorization']);
		if (!isset($result['code']) || $result['code'] != 200) return json(['code'=>401, 'msg'=>'请确认token是否有效。']);
		return $next($request);
    }
}
