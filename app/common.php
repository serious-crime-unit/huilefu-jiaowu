<?php
// 应用公共文件
use dh2y\qrcode\QRcode;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use OSS\Core\OssException;
use OSS\OssClient;
use think\facade\Db;
use think\facade\Env;

/**
 * @Description:
 * @Author: 林怼怼
 * @Date: 2025/1/16
 * @Time: 10:58
 */
function getUserID($data)
{
    $data = Db::name('evaluate')->where('id', $data['id'])->find();
    if ($data['parent_comment_id'] != 0){
        $data = Db::name('evaluate')->where('id', $data['p_id'])->find();
        getUserID($data);
    }
    return $data;
}

/**
 * @Description: 查询课程id
 * @Author: 林怼怼
 * @Date: 2025/1/16
 * @Time: 10:58
 */
function getActivityID($data)
{
    if ($data['parent_comment_id'] === 0) return $data;
    $data = Db::name('evaluate')->where('id', $data['parent_comment_id'])->find();
    return $data;
}

/**
 * @Description: 获取全部课程评论id
 * @Author: 林怼怼
 * @Date: 2024/12/9
 * @Time: 14:17
 */
function getAllCommentMeeting($ids)
{
    // 查询所有阶段
    $id1 = Db::name('meeting')
        ->whereIn('p_id', $ids)
        ->column('id');
    // 查询场次id
    $id2 = Db::name('meeting_city')
        ->whereIn('p_id', $id1)
        ->column('id');
    $ids = array_merge(explode(',', $ids), $id2);
    return $ids;
}


/**
 * @Description: 获取评论id
 * @Author: 林怼怼
 * @Date: 2024/12/9
 * @Time: 14:17
 */
function getAllCommentIDs($ids)
{
    $ids = Db::name('evaluate')
        ->whereIn('p_id', $ids)
        ->where(['type' => 3])
        ->column('id');
    if (!empty($ids)) {
        $ids = array_merge($ids, getAllCommentIDs($ids));
    }
    return $ids;
}

/**
 * @Description: 讲师获取评论id
 * @Author: 林怼怼
 * @Date: 2024/12/9
 * @Time: 14:18
 */
function getAllCommentIDsjs($ids)
{
    $ids = Db::name('evaluate')
        ->whereIn('p_id', $ids)
        ->where(['type' => 6])
        ->column('id');
    if (!empty($ids)) {
        $ids = array_merge($ids, getAllCommentIDsjs($ids));
    }
    return $ids;
}

/**
 * @Description: 查询活动排名
 * @Author: 林怼怼
 * @Date: 2024/12/5
 * @Time: 09:41
 */
function getMeetingRank($id)
{
    // 查询所有活动的参与人数并计算排名
    $rank_data = Db::name('meeting')
        ->alias('m')
        ->join('meeting m2', 'm2.p_id = m.id', 'LEFT')  // 连接活动子级表
        ->join('meeting_city mc', 'mc.p_id = m2.id', 'LEFT')  // 连接活动子级表
        ->join('member_meeting mm', 'mc.id = mm.m_id', 'LEFT')  // 连接预约人数表
        ->where('m.p_id', 0)
        ->field('m.id, COUNT(mm.u_id) as participant_count')
        ->group('m.id')  // 按活动ID分组
        ->order('participant_count', 'desc')  // 按参与人数降序排序
        ->select();
    // 获取活动ID为1的排名
    $rank_position = 0;
    foreach ($rank_data as $index => $item) {
        if ($item['id'] == $id) {
            $rank_position = $index + 1;  // 排名从1开始
            break;
        }
    }
    return $rank_position;
}

/**
 * @Description: 活动添加订单
 * @Author: 林怼怼
 * @Date: 2024/4/10/010
 * @Time: 10:55
 */
function setOrderSave($id, $p_id, $m_id, $url, $mobile = 0)
{
    $count = Db::name('order')
        ->where([
            ['u_id', '=', $id],
            ['p_id', '=', $p_id],
            ['c_id', '=', 10],
            ['mobile', '=', $mobile],
            ['year', '=', $m_id],
            ['type', '=', 6],
            ['state', '=', 0]
        ])
        ->count();
    if ($count > 0) return false;
    Db::name('order')->insert([
        'u_id' => $id,
        'p_id' => $p_id,
        'c_id' => 10,
        'mobile' => $mobile,
        'image' => $url,
        'num' => 1,
        'year' => $m_id,
        'type' => 6,
        'state' => 0,
        'vip_start_time' => time(),
        'vip_end_time' => time(),
        'time' => time()
    ]);
    return true;
}

/**
 * @Description: 冒泡排序
 * @Author: 林怼怼
 * @Date: 2024/3/12/012
 * @Time: 16:29
 */
function bubbleSortDesc($arr, $sortByIndex) {
    $n = count($arr);
    // 外层循环控制需要比较的轮数
    for ($i = 0; $i < $n - 1; $i++) {
        // 内层循环控制每一轮比较的次数
        for ($j = 0; $j < $n - $i - 1; $j++) {
            // 如果当前元素比下一个元素小，则交换它们
            if ($arr[$j][$sortByIndex] < $arr[$j + 1][$sortByIndex]) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}


/**
 * @Description: 检测是否又默认地址如果有取消
 * @Author: 林怼怼
 * @Date: 2024/3/5/005
 * @Time: 17:58
 */
function cityCorrect($id)
{
    $is_master = Db::name('member_address')
        ->where([
            'id' => $id,
            'is_master' => 1
        ])->find();
    if ($is_master) {
        // 地址全部取消默认
        $res = Db::name('member_address')->where(['id' => $id, 'is_master' => 1])->update([
            'is_master' => 2
        ]);
    }
    return true;
}

/**
 * @Description: 购买课程时寻找的推荐人 微笑长或者笑长
 * @Author: 林怼怼
 * @Date: 2024/3/5/005
 * @Time: 12:57
 */
function getParentCourse($id)
{
    // 查看上级是否开通对应产品
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
    if (in_array(0, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParentCourse($userInfo['id']);
    }
}

/**
 * @Description: 续费查找推荐人是否续费
 * @Author: 林怼怼
 * @Date: 2024/3/5/005
 * @Time: 11:06
 */
function getParentMaster($id, $cate = 1)
{
    // 查看上级是否开通对应产品
    $parent = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $parent['level'] = array_level($parent['id']);
    if ($cate == 1) {
        // 查看是否开通365慧乐福
        if (in_array(0, $parent['level']) || in_array(1, $parent['level']) || in_array(3, $parent['level'])) {
            return $parent;
        }
    }elseif ($cate == 2) {
        // 查看是否开通521了了派
        if (in_array(0, $parent['level']) || in_array(4, $parent['level']) || in_array(5, $parent['level']) || in_array(6, $parent['level'])) {
            return $parent;
        }
    }elseif ($cate == 3) {
        // 查看是否开通365悦读慧
        if (in_array(0, $parent['level']) || in_array(7, $parent['level']) || in_array(9, $parent['level'])) {
            return $parent;
        }
    }elseif ($cate == 8) {
        // 学员证什么身份都可以
        return $parent;
    }elseif ($cate == 11) {
        // 查看是否开通好妈妈
        if (in_array(0, $parent['level']) || in_array(10, $parent['level']) || in_array(11, $parent['level'])) {
            return $parent;
        }
    }
    return getParentMaster($parent['id'], $cate);
}

/**
 * @Description: 获取归属代理
 * @Author: 林怼怼
 * @Date: 2024/3/13/013
 * @Time: 17:21
 */
function getParent_365_521($id)
{
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
    if (in_array(0, $userInfo['level']) || in_array(3, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level']) || in_array(9, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParent365($userInfo['id']);
    }
}

/**
 * @Description: 查询521笑长 (微笑长专用)
 * @Author: 林怼怼
 * @Date: 2023/12/29/029
 * @Time: 15:30
 */
function getParent521($id)
{
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
//    if (in_array(0, $userInfo['level']) || in_array(3, $userInfo['level']) || in_array(6, $userInfo['level']) || in_array(9, $userInfo['level'])) {
    if (in_array(0, $userInfo['level']) || in_array(6, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParent521($userInfo['id']);
    }
}

/**
 * @Description: 查询思享慧主任身份
 * @Author: 林怼怼
 * @Date: 2023/12/29/029
 * @Time: 15:30
 */
function getParent365($id)
{
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
//    if (in_array(0, $userInfo['level']) || in_array(3, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level']) || in_array(9, $userInfo['level']) || in_array(11, $userInfo['level'])) {
    if (in_array(0, $userInfo['level']) || in_array(3, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParent365($userInfo['id']);
    }
}

/**
 * @Description: 查询阅读荟主任身份
 * @Author: 林怼怼
 * @Date: 2023/12/29/029
 * @Time: 15:30
 */
function getParent365s($id)
{
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
    if (in_array(0, $userInfo['level']) || in_array(9, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParent365s($userInfo['id']);
    }
}

/**
 * @Description: 查询站长身份
 * @Author: 林怼怼
 * @Date: 2023/12/29/029
 * @Time: 15:30
 */
function getParentMama($id)
{
    $userInfo = Db::name('member')
        ->alias('t1')
        ->join('member_parent t2', 't1.id = t2.p_id')
        ->where(['t2.id' => $id])
        ->field('t1.id, t1.name, t1.avatar, t1.phone')
        ->find();
    $userInfo['level'] = array_level($userInfo['id']);
    if (in_array(0, $userInfo['level']) || in_array(5, $userInfo['level']) || in_array(6, $userInfo['level'])) {
        return $userInfo;
    }else{
        return getParentMama($userInfo['id']);
    }
}

/**
 * @Description: 用户审核发送模板消息
 * @Author: 林怼怼
 * @Date: 2024/1/4/004
 * @Time: 17:09
 */
function sendTemplateMessage2($openid, $data)
{
    $token = vxAccessToken();
    $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$token;
    $params = [
        'touser' => $openid,
        'template_id' => env("WECHAT.wx_template_id2"),
        'url' => $data['url'],
        'data' => [
            'first' => ['value' => $data['userName']],
            'keyword1' => ['value' => $data['userName']],
            'keyword2' => ['value' => $data['desc']],
            'keyword3' => ['value' => $data['time']],
            'remark' => ['value' => $data['userName']],
        ],
    ];
    // 因为参数问题，直接使用curl原生来请求token接口
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $response;
}

/**
 * @Description: 定时发布公告
 * @Author: 林怼怼
 * @Date: 2023/12/23/023
 * @Time: 15:56
 */
function saveNotice()
{
    // 启动事务
    Db::startTrans();
    try {
        $data = Db::name('notice_time')
            ->where([
                ['message_time', '<=', time()],
                ['state', '=', 2]
            ])
            ->select()->toArray();
        foreach ($data as $key => $val) {
            $id = Db::name('notice')->insertGetId([
                'title' => $val['title'],
                'u_id' => $val['u_id'],
                'img' => $val['img'],
                'desc' => $val['desc'],
                'time' => time()
            ]);
            $arr = [];
            $ids = explode(',', $val['ids']);
            $count = count($ids);
            for ($i = 0; $i < $count; $i++) {
                $arr[] = ['id' => $id, 'u_id' => $ids[$i], 'state' => 2, 'time' => time()];
            }
            Db::name('notice_user')->insertAll($arr);
            Db::name('notice_time')->save(['id' => $val['id'], 'state' => 1]);
        }
        // 提交事务
        Db::commit();
        return json(['code' => 200, 'msg' => '成功。']);
    } catch (\Throwable $e){
        // 回滚事务
        Db::rollback();
        // 这是进行异常捕获
        $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'定时发布活动异常，错误：'.$e->getMessage();
        BackstageLog(1, '定时任务', $desc);
        return json(['code' => 201, 'msg' => $e->getMessage()]);
    }
}

/**
 *
 * @Description: 生成二维码
 * @Author: Linxy
 * @Date: 2023/12/4/004
 * @Time: 15:47
 *
 */
function generateQrCode($url)
{
    // 启动事务
    Db::startTrans();
    try {
        // 创建 QrCode 实例
        $qrCode = new QrCode();
        //获取二维码生成的地址
        $code_path =  $qrCode->png($url)->getPath();
        $fileName = md5(date('Y-m-d H:i:s:u')) . '.' . 'png';
        $destName = $_SERVER['DOCUMENT_ROOT'].$code_path;
        // 上传到oss
        $result = uploadFile($fileName, $destName);
        // 删除临时文件
        unlink($destName);
        // 提交事务
        Db::commit();
        return $result['info']['url'];
    } catch (\Throwable $e){
        // 回滚事务
        Db::rollback();
        // 这是进行异常捕获
        return json(['code' => 203, 'msg' => $e->getMessage()]);
    }
}

/**
 *
 * @Description: 会员增加积分
 * @Author: Linxy
 * @Date: 2023/12/4/004
 * @Time: 14:55
 *
 */
function integralInc($id)
{
    $data = Db::name('member_grade')->where('id', $id)->select()->toArray();
    foreach ($data as &$datum) {
        if ($datum['grade'] === 1 || $datum['grade'] === 7) {
            // 365慧乐福 悦读慧 100积分
            Db::name('member_pay')->where([
                'id' => $datum['id'],
                'type' => 6,
            ])->inc('num', 100)->update();
        }elseif ($datum['grade'] === 4) {
            // 521了了会员 200积分
            Db::name('member_pay')->where([
                'id' => $datum['id'],
                'type' => 6,
            ])->inc('num', 200)->update();
        }
    }
    return true;
}

/**
 * Created by PhpStorm.
 * @purpose 预约定时活动提醒
 * @Author: Linxy
 * @Time: 2023/11/28 9:53
 */
function getUserEventsAppoint()
{
    // 启动事务
    Db::startTrans();
    try {
        // 查询出需要提醒的内容
        $data = Db::name('member_meeting')
            ->alias('m1')
            ->leftJoin('meeting m2', 'm1.m_id = m2.id')
            ->leftJoin('member m3', 'm1.u_id = m3.id')
            ->whereTime('m1.time', '<=', time())
            ->field('m2.*, m3.name as userName, m3.openid')
            ->select()->toArray();
        if ($data) {
            $token = vxAccessToken();
            foreach ($data as &$datum) {
                // 给openid发送
                $res = sendTemplateMessage($datum['openid'], $token, $datum);
                $result = Db::name('member_meeting')->where('id', $datum['id'])->update(['state' => 2]);
            }
        }
        // 提交事务
        Db::commit();
        return true;
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'预约定时活动提醒异常，错误：'.$e->getMessage();
        BackstageLog(1, '定时任务', $desc);
        return true;
    }
}

/**
 * Created by PhpStorm.
 * @purpose 慧乐福预约活动发送模版消息
 * @Author: Linxy
 * @Time: 2023/11/28 11:10
 */
function sendTemplateMessage($openid, $token, $data)
{
    $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$token;
    $params = [
        'touser' => $openid,
        'template_id' => env("WECHAT.wx_template_id"),
        'url' => \env('WECHAT.wx_url'),
        'data' => [
            'first' => ['value' => $data['userName'].'您的活动提醒来了'],
            'keyword1' => ['value' => $data['name']],
            'keyword2' => ['value' => date('Y-m-d H:i', $data['start_time'])],
            'remark' => ['value' => $data['userName'].'您预约的活动《'.$data['name'].'》提醒时间到了快点去看看吧~'],
        ],
    ];
    // 因为参数问题，直接使用curl原生来请求token接口
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $response;
}

/**
 * Created by PhpStorm.
 * @purpose 获取微信js_sdk
 * @Author: Linxy
 * @Time: 2023/11/30 9:22
 */
function vx_js_SDK()
{
    // 获取token的接口
    $url = "https://i-wx.aimanrenjian.net/wxTokenCenter/token/jsapi";
    $data = [
        'appId' => env('WECHAT.appId'),
        'apiSecret' => env('WECHAT.apiSecret'),
        'reqSource' => env('WECHAT.reqSource'),
    ];
    // 因为参数问题，直接使用curl原生来请求token接口
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $response;
}

/**
 * Created by PhpStorm.
 * @purpose 获取慧乐福公众号token
 * @Author: Linxy
 * @Time: 2023/11/28 11:07
 */
function vxAccessToken()
{
    // 获取token的接口
    $url = "https://i-wx.aimanrenjian.net/wxTokenCenter/token/cgiAccess";
    $data = [
        'appId' => env('WECHAT.appId'),
        'apiSecret' => env('WECHAT.apiSecret'),
        'reqSource' => env('WECHAT.reqSource'),
    ];
    // 因为参数问题，直接使用curl原生来请求token接口
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    return $response;
}

/**
 * Created by PhpStorm.
 * @purpose 检测会员如果没有到期但是账号被冻结了自动恢复解冻(定时任务)
 * @Author: Linxy
 * @Time: 2023/11/24 15:36
 */
function editUserVIP2($id)
{
    // 启动事务
    Db::startTrans();
    try {
        // 查询被冻结的用户有没有没到期的身份
        $user_data = Db::name('member_grade')->where('id', $id)->select()->toArray();
        if (count($user_data) != 0) {
            // 不等于0说明有没到期的身份
            foreach ($user_data as &$datum) {
                if ($datum['vip_end_time'] > time() || $datum['vip_end_time'] == null) {
                    // 没办法续身份直接修改账号状态为正常
                    Db::name('member')->save(['id' => $datum['id'], 'state' => 1]);
                }else{
                    Db::name('member_grade')->where(['id' => $datum['id'], 'grade' => $datum['grade']])->delete();
                    Db::name('member_grade')->save(['id' => $datum['id']]);
                }
            }
        }
        // 提交事务
        Db::commit();
        return json(['code' => 200, 'msg' => '成功。']);
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检测用户没到期状态异常，错误：'.$e->getMessage();
        BackstageLog(1, '定时任务', $desc);
        return json(['code' => 201, 'msg' => '失败。']);
    }
}

/**
 * Created by PhpStorm.
 * @purpose 检测会员到期后一个月是否续费(定时任务)  需要测试
 * @Author: Linxy
 * @Time: 2023/11/24 15:36
 */
function editUserVIP()
{
    // 启动事务
    Db::startTrans();
    try {
        // 查询出已经到期的
        $data = Db::name('member_grade')
            ->where([
                ['vip_end_time', '<', time()],
                ['grade', 'in', [1,4,7,10]]
            ])
            ->select()->toArray();
        // 查询到期以后除了账号
        $arr = [];
        foreach ($data as $key => $val) {
            // 到期后去掉他的等级身份
            if ($val['vip_end_time'] < time()) {
                if ($val['grade'] === 1) {
                    // 去除365慧乐福身份
                    Db::name('member_grade')->where([
                        ['id', '=', $val['id']],
                        ['grade', 'in', [1,2]],
                    ])->delete();
                }elseif ($val['grade'] === 4) {
                    // 去除521了了派身份
                    Db::name('member_grade')->where([
                        ['id', '=', $val['id']],
                        ['grade', 'in', [4]],
                    ])->update(['vip_start_time' => null, 'vip_end_time' => null]);
                }elseif ($val['grade'] === 7) {
                    // 去除365悦读慧身份
                    Db::name('member_grade')->where([
                        ['id', '=', $val['id']],
                        ['grade', 'in', [7,8]],
                    ])->delete();
                }elseif ($val['grade'] === 10) {
                    // 去除好妈妈身份
                    Db::name('member_grade')->where([
                        ['id', '=', $val['id']],
                        ['grade', 'in', [10]],
                    ])->update(['vip_start_time' => null, 'vip_end_time' => null]);
                }
            }
            // 查看是否还有身份
            $count = Db::name('member_grade')->where('id', $val['id'])->count();
            if ($count === 0) {
                // 如果没身份账号冻结
                Db::name('member_grade')->save(['id' => $val['id']]);
//                Db::name('member')->where('id', $val['id'])->update(['state' => 2]);
            }
        }
        // 提交事务
        Db::commit();
        return json(['code' => 200, 'msg' => '成功。']);
    } catch (\Exception $e) {
        // 回滚事务
        Db::rollback();
        $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检测用户是否到期异常，错误：'.$e->getMessage();
        BackstageLog(1, '定时任务', $desc);
        return json(['code' => 201, 'msg' => '失败，异常。', 'data' => $e->getMessage()]);
    }
}

/**
 * Created by PhpStorm.
 * @purpose 获取全部子级用户手机号
 * @Author: Linxy
 * @Time: 2023/11/24 9:28
 */
function getChildUserPhoneS($idS)
{
    // 查询推荐用户电话
    $idS = Db::name('member_parent')
        ->alias('t1')
//        ->join('member_grade t3', 't2.id = t3.id AND t3.grade <> 3 AND t3.grade <> 6 AND t3.grade <> 9')
        ->where([
            ['t1.p_id', 'in', $idS]
        ])
        ->column('t1.id');
    $idS = array_unique($idS);
    if (!empty($idS)) {
        $idS = array_merge($idS, getChildUserPhoneS($idS));
    }
    return $idS;
}

// 计算两个日期相差 年 月 日
function DiffDate($date1,$date2)
{
    $s_date = date("Y-m-d H:i:s", $date1);
    $e_date = date("Y-m-d H:i:s", $date2);

    $cha = strtotime($e_date)-strtotime($s_date);

    //年
    $year = floor((strtotime($e_date)-strtotime($s_date)) / (31536000));
    return $year;
}

/**
 * Created by PhpStorm.
 * @purpose 检查活动更新状态
 * @Author: Linxy
 * @Time: 2023/10/25 17:53
 */
function activityExamine()
{
    // 启动事务
    Db::startTrans();
    try {
        $data = Db::name('meeting')->where(['p_id' => 0])->select()->toArray();
        foreach ($data as &$item){
            $id =  Db::name('meeting')->where(['p_id' => $item['id']])->column('id');
            // 活动批量修改
            $dataMax = Db::name('meeting_city')
                ->whereIn('p_id', $id)
                ->max('end_time');
            $dataMin = Db::name('meeting_city')
                ->whereIn('p_id', $id)
                ->min('start_time');
            if ($dataMax > 100 && $dataMin > 100) {
                if ($dataMin <= time() && $dataMax >= time()) {
                    // 批量修改(进行中)
                    $state = 2;
                }elseif ($dataMin >= time()) {
                    // 批量修改(未开始)
                    $state = 1;
                }elseif ($dataMax+86400 <= time()) {
                    // 批量修改(已结束)
                    $state = 3;
                    $ids = Db::name('meeting_city')->whereIn('p_id', $id)->column('id');
                    Db::name('member_meeting')->where([
                        ['m_id', 'in', $ids],
                        ['complete', '=', 1]
                    ])->delete();
                }else{
                    $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'活动时间异常修改失败';
                    BackstageLog(1, '定时任务', $desc);
                }
                Db::name('meeting')->where('id', $item['id'])->update(['state' => $state]);
            }
        }
        // 提交事务
        Db::commit();
        return json(['code' => 200, 'msg' => '活动状态已更新。']);
    } catch (\Throwable $e){
        // 回滚事务
        Db::rollback();
        $desc = '公众号定时任务['. date('Y年m月d日 H时i分s秒') .']'.'检查活动是否开始结束异常，错误：'.$e->getMessage();
        BackstageLog(1, '定时任务', $desc);
        // 这是进行异常捕获
        return json(['code' => 201, 'msg' => $e->getMessage()]);
    }
}

/**
 * Created by PhpStorm.
 * @purpose 删除oss图片（暂时没有使用）
 * @Author: Linxy
 * @Time: 2023/10/17 18:01
 */
function deleteFile($path_file) {
    if (env('ALY_OSS.OSS_OPEN')) {
        try {
            // 删除的时候记住不要带域名，也不要带'/'这个路径符号，如全路径是：'https://hxty.oss-cn-beijing.aliyuncs.com/data/pdf/contract/2021-06-16/HXSHB21061611594482340c.pdf'，则这里的path_file路径为：'data/pdf/contract/2021-06-16/HXSHB21061611594482340c.pdf'。
            $path_file=trim(parse_url($path_file)['path'],'/');//去掉域名,
            $ossClient = new \OSS\OssClient(Env::get('ALY_OSS.ALY_ACCESSKEY_ID'), Env::get('ALY_OSS.ALY_ACCESSKEY_SECRET'), Env::get('ALY_OSS.ALY_ENDPOINT'));
            $ossClient->deleteObject(Env::get('ALY_OSS.ALY_BUCKET'), $path_file);
        } catch (OssException $e) {
            return __FUNCTION__ . ": FAILED". $e->getMessage();
        }
    } else {
        $val2 = "." . $path_file;
        if (@file_exists($val2)) {
            $unlinkfilename = trim($path_file, "/");
            unlink($unlinkfilename);
        }
        return true;
    }
}

/**
 * Created by PhpStorm.
 * @purpose 上傳文件
 * @Author: Linxy
 * @Time: 2023/10/17 15:38
 */
function uploadFile(string $newFilePath, string $saveName)
{
    $accessKeyId = env('ALY_OSS.ALY_ACCESSKEY_ID');
    $accessKeySecret = env('ALY_OSS.ALY_ACCESSKEY_SECRET');
    $endpoint = env('ALY_OSS.ALY_ENDPOINT');
    $bucket = env('ALY_OSS.ALY_BUCKET');
    $rootDir = Env::get('ALY_OSS.ALY_ROOTDIR');
    try {
        $object = $rootDir.'/'.$newFilePath;
        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
        $result = $ossClient->uploadFile($bucket, $object, $saveName);
        return $result;
    } catch (OssException $e) {
        printf(__FUNCTION__.": FAILED\n");
        printf($e->getMessage()."\n");
        return;
    }
}

/**
 * Created by PhpStorm.
 * @purpose 加入消息提醒
 * @Author: Linxy
 * @Time: 2023/10/12 16:06
 */
function remind($arr)
{
    $data = Db::name('remind')->save([
        'u_id' => $arr['u_id'],
        'c_id' => $arr['c_id'],
        'r_id' => $arr['r_id'],
        'desc' => $arr['desc'],
        'type' => $arr['type'],
        'time' => time(),
    ]);
    return true;
}

/**
 * Created by PhpStorm.
 * @purpose 递归
 * @Author: Linxy
 * @Time: 2023/4/16 10:41
 */
function tree($arr,$p_id=0){
    $data = array();
    foreach($arr as $key=>$val){
        if($val['pid'] == $p_id) {
            $length = count($data); //0
            $data[$length] = $val;
            if (!empty(tree($arr, $val['id']))){
                $data[$length]['children'] = tree($arr, $val['id']);
            }
        }
    }
    return $data;
}

/**
 * Created by PhpStorm.
 * @purpose 获取用户信息
 * @Author: Linxy
 * @Time: 2023/10/9 15:52
 */
function getUserInfo($type = false)
{
    $header = request()->header();
    $result = checkToken($header['authorization'], $type);
    if (!isset($result['code']) || $result['code'] != 200) return ['code'=>401, 'msg'=>$result['msg']];
    $id = $result['data']['id'];
    $data = Db::name('member')
        ->alias('t1')
        ->leftJoin('member_address t2', 't1.id = t2.id AND t2.is_master = 1 AND t2.state = 1')
        ->leftJoin('city t3', 't2.p_id = t3.id')
        ->leftJoin('city t4', 't2.c_id = t4.id')
        ->leftJoin('city t8', 't2.c_id = t8.id')
        ->leftJoin('city t5', 't2.d_id = t5.id')
        ->leftJoin('user_role t6', 't1.id = t6.id')
        ->leftJoin('role t7', 't6.r_id = t7.id')
        ->leftJoin('member_parent t9', 't1.id = t9.id')
        ->fieldRaw('t1.id, t1.name, t1.pinyin, t1.openid, t1.avatar, t1.phone, t1.number, t1.sex, t1.desc, t9.p_id, IFNULL(t7.code, "") as roles, t2.a_id, IFNULL(t3.cityName, "") province_name, t3.id as province_id, IFNULL(t4.cityName, "") city_name, t4.id as city_id, IFNULL(t5.cityName, "") area_name, t5.id as d_id, t2.address, t1.is_teacher, t1.state, t1.is_admin, t1.time')
        ->find($id);
    $data['num'] = Db::name('member_pay')->where(['id' => $data['id'], 'type' => 6])->value('num');
    $data['child'] = Db::name('member_parent')->where(['p_id' => $data['id']])->count();
    // 获取等级
    $data['level'] = array_level($data['id']);
    // 推荐人
    $data['parentName'] = '最高级';
    $data['parentPhone'] = '最高级';
    if ($data['id'] != 1) {
        $parent = Db::name('member')->find($data['p_id']);
        $data['parentName'] = $parent['name'];
        $data['parentPhone'] = $parent['phone'];
    }
    $data['code'] = 200;
    editUserVIP(); // 检查会员是否到期一个月
    editUserVIP2($id); // 检测会员是否异常冻结 （如果会员没到期自动解冻）
    return $data;
}

/**
 * @Description: 级别转为整型数组
 * @Author: 林怼怼
 * @Date: 2024/1/10/010
 * @Time: 11:36
 */
function array_level($id)
{
    $arr = Db::name('member_grade')->where(['id' => $id])->column('grade');
    return $arr;
}

/**
 * Created by PhpStorm.
 * @purpose JWT解密Token
 * @Author: Linxy
 * @Time: 2023/10/9 15:18
 */
function checkToken($jwt = '', $type = false)
{
    $date['code']  = true;
    try {
        $key         = env('jwt.key');//这里是你的秘钥，需要和解密是保持一致！
        if ($type) $key = env('jwt.api_key'); // api
        JWT::$leeway    = 60;//当前时间减去60，把时间留点余地
        $decoded        = JWT::decode($jwt, new Key($key, 'HS256')); //HS256方式，这里要和签发的时候对应
        $arr            = (array)$decoded;
        $date['code']  = 200;
        $date['data']    =(array)$arr['data'];
        return $date;
    } catch (\Exception $e) {
        return ['code'=>401, 'msg'=>$e->getMessage()];
    }
}

/**
 * Created by PhpStorm.
 * @purpose JWT加密Token
 * @Author: Linxy
 * @Time: 2023/10/9 14:57
 */
function encryptToken($id, $type = false)
{
    $key = env('jwt.key');//这里是你的秘钥，需要和解密是保持一致！
    if ($type) $key = env('jwt.api_key');
    $onlyCode = uuid();
    $res = Db::name('member')->save(['id' => $id, 'onlyCode' => $onlyCode]);
    $token = array(
        "iss" => 'weixin',         //签发者 可以为空
        "aud" => 'spospone',       //面象的用户，可以为空
        "iat" => time(),           //签发时间
        "nbf" => time(),          //立马生效
        "exp" => time() + 86400,   //token 过期时间 两小时
        "data" => [               //这里是的数据参数等，需要保存和取出的
            'id' => $id,
            'onlyCode' => $onlyCode
        ]
    );
    return JWT::encode($token, $key, "HS256");
}

/**
 * Created by PhpStorm.
 * @purpose 后台日志
 * @Author: Linxy
 * @Time: 2023/10/9 11:21
 */
function BackstageLog($id, $type, $content)
{
//    if (env('ALY_SMS.ALY_STATE') == 0) {
//        return true;
//    }
    $result = Db::name('admin_log')
        ->save([
            'u_id' => $id,
            'type' => $type,
            'content' => $content,
            'ip' => $_SERVER["REMOTE_ADDR"],
            'time' => time(),
        ]);
    return true;
}

if (!function_exists('encrypt_password')) {
    /**
     * Created by PhpStorm.
     * @purpose 密码加密
     * @Author: Linxy
     * @Time: 2023/10/9 11:16
     */
    function encrypt_password($password)
    {
        //加盐处理
        $salt = '!@#$%^&|@jiaYan';
        //使用MD5加密
        return md5($salt . $password . $salt);
    }
}

/**
 * Created by PhpStorm.
 * @purpose 打印函数使用p可以打印任意类型
 * @Author: Linxy
 * @Time: 2022/12/24 10:09
 */
function p($data)
{
    // 定义样式
    $str='';
    // 如果是boolean或者null直接显示文字；否则print
    if (is_bool($data)) {
        $show_data=$data ? 'true' : 'false';
    }elseif (is_null($data)) {
        $show_data='null';
    }else{
        $show_data=print_r($data,true);
    }
    $str.=$show_data;
    $str.='';
    echo $str;exit();
}

/**
 * 发送HTTP请求方法
 * @param string $url  请求URL
 * @param array $params 请求参数
 * @param string $method 请求方法GET/POST
 * @return array|bool|string
 */
function http($url, $params, $method = 'GET', $header = array(), $multi = false)
{
    $opts = array(
        CURLOPT_TIMEOUT    => 30,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_HTTPHEADER   => $header
    );
    /* 根据请求类型设置特定参数 */
    switch(strtoupper($method)){
        case 'GET':
            $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
            break;
        case 'POST':
            //判断是否传输文件
            $params = $multi ? $params : http_build_query($params);
            $opts[CURLOPT_URL] = $url;
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $params;
            break;
        default:
            throw new Exception('不支持的请求方式！');
    }
    /* 初始化并执行curl请求 */
    $ch = curl_init();
    curl_setopt_array($ch, $opts);
    $data = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    if($error) throw new Exception('请求发生错误：' . $error);
    return $data;
}

/**
 * Created by PhpStorm.
 * @purpose 生成唯一字符串
 * @Author: Linxy
 * @Time: 2023/10/9 17:26
 */
function uuid($length=11)
{
    // fix for compatibility with 32bit architecture; each mt_rand call is restricted to 32bit
    // two such calls will cause 64bits of randomness regardless of architecture
    $seed = mt_rand(0, 2147483647) . '#' . mt_rand(0, 2147483647);

    // Hash the seed and convert to a byte array
    $val = md5($seed, true);
    $byte = array_values(unpack('C16', $val));

    // extract fields from byte array
    $tLo = ($byte[0] << 24) | ($byte[1] << 16) | ($byte[2] << 8) | $byte[3];
    $tMi = ($byte[4] << 8) | $byte[5];
    $tHi = ($byte[6] << 8) | $byte[7];
    $csLo = $byte[9];
    $csHi = $byte[8] & 0x3f | (1 << 7);

    // correct byte order for big edian architecture
    if (pack('L', 0x6162797A) == pack('N', 0x6162797A)) {
        $tLo = (($tLo & 0x000000ff) << 24) | (($tLo & 0x0000ff00) << 8)
            | (($tLo & 0x00ff0000) >> 8) | (($tLo & 0xff000000) >> 24);
        $tMi = (($tMi & 0x00ff) << 8) | (($tMi & 0xff00) >> 8);
        $tHi = (($tHi & 0x00ff) << 8) | (($tHi & 0xff00) >> 8);
    }

    // apply version number
    $tHi &= 0x0fff;
    $tHi |= (3 << 12);

    // cast to string
    $uuid = sprintf(
        '%08x%04x%04x%02x%02x%02x%02x%02x%02x%02x%02x',
        $tLo,
        $tMi,
        $tHi,
        $csHi,
        $csLo,
        $byte[10],
        $byte[11],
        $byte[12],
        $byte[13],
        $byte[14],
        $byte[15]
    );

    $str = strtoupper($uuid);
    while (1){
        $uuid = substr(str_shuffle(str_repeat($str, $length)), 0, $length);
        if(!is_numeric($uuid)){
            break;
        }
    }
    return $uuid;
}

/**
* @Description: 返回内容
* @Author: 林怼怼
* @Date: 2024/1/3/003
* @Time: 10:32
*/
function success($data = '', $code = 200)
{
    if ($code == 200) {
        $week = array("日","一","二","三","四","五","六"); // 星期
        $month = array('一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'); // 月份
        $result = [
            "createDate"=> "星期".$week[date("w")]." ".$month[date('n')-1]." ". date("d")." ".date("H:i:s")." CST ".date("Y"),
            "flag"=> $code,
            "data"=>$data,
            "message"=>"操作成功",
            "failed"=>"true",
        ];
    }else{
        $result = [
            "flag"=> $code,
            "message"=>"操作失败",
        ];
    }
    return json($result);
}