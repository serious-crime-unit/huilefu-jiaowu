<?php
/**
 * @Description: 描述
 * @Author: 林怼怼
 * @Date: 2025/1/22
 * @Time: 10:15
 */
namespace app\utils;

class WeChatPay
{
    /**
     * 生成HMAC-SHA256签名
     *
     * @param array $data 请求参数
     * @param string $apiV3Key APIv3密钥
     * @return string 签名
     */
    public static function generateSign($data, $apiV3Key)
    {
        // 排序请求参数
        ksort($data);

        // 拼接成字符串
        $string = urldecode(http_build_query($data)) . '&key=' . $apiV3Key;

        // 使用HMAC-SHA256生成签名
        return strtoupper(hash_hmac('sha256', $string, $apiV3Key));
    }
}
